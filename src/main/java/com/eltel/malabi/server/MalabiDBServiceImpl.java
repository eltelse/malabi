package com.eltel.malabi.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import com.eltel.malabi.client.LightArea;
import com.eltel.malabi.client.LightAreasStatus;
import com.eltel.malabi.client.MalabiDBService;
import com.eltel.malabi.client.MalabiInfo;
import com.eltel.malabi.client.Quarter;
import com.eltel.malabi.client.SoundInfo;
import com.eltel.malabi.client.SoundPole;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server-side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class MalabiDBServiceImpl extends RemoteServiceServlet implements MalabiDBService {
	
	private final static UserOnlineControl mUserOnlineControl = new UserOnlineControl();
	private static String CONFIGURATION_FILE = "C:/MALA-B/config.cfg";
	public static String MALABI_IP ="2.55.96.114";
	private static String MALABI_START_COMMAND = "echo y | C:\\MALA-B\\plink.exe -ssh pi@2.55.96.114 -P ";
	private static String STOP_ADAM_COMMAND = "sudo killall -s 9 omxplayer.bin";
	private static String ADD_PLAY_ADAM_COMMAND = " | omxplayer --vol 500 /home/pi/Music/\"'";
	public static int MALABI_PORT_BASE_LIGHT = 8100;
	public static int MALABI_PORT_BASE_SOUND = 8110;
	private static int POLE_TIMEOUT = 6;
	public static boolean MALABI_PROD_MODE = true;
	
	private static TimerTask mTimerTask;
	private static Timer mTimer;
	
	static {
		initInfoAndTimer();
	}
	
	/*******************   Login *********************/
	@Override
	public MalabiInfo loginServer(String userName, String password) {	    
		MalaLogging.log("Login user *"+userName+"* Password *"+password+"*");
		
		MalabiInfo malabiInfo = SQLUtils.LOGIN(userName, password, mUserOnlineControl);
		
		if (!malabiInfo.mMalabiUser.isShouldGetMoreUserInfo())
			return malabiInfo;

		mUserOnlineControl.addUser(malabiInfo.mMalabiUser.mID, malabiInfo);
		return malabiInfo;
	}

	@Override
	public Boolean logoffServer(int userID) {
		MalabiInfo malabiInfo = mUserOnlineControl.getUserInfo(userID);
		if (malabiInfo != null){
			MalaLogging.log("User "+ malabiInfo.mMalabiUser.mName + malabiInfo.mMalabiUser.mID +" is logging off;");
			SQLUtils.LOGOFF(malabiInfo.mQuarters, malabiInfo.mSoundPoles, userID);
			mUserOnlineControl.logoffUser(userID, false);
			mUserOnlineControl.mUserActiveStatus.keySet().remove(userID);
		}
		else {
			MalaLogging.log("User "+ userID + " is logging off; Not such user on the system;");
		}

		return false;
	}

	private static void initInfoAndTimer() {
		if (mTimerTask != null || mTimer != null)
			return;
		int userTime = 300000;

		Properties configFile = new Properties();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(new File(CONFIGURATION_FILE));
			configFile.load(fis);
			for (String name : configFile.stringPropertyNames()) {
				MalaLogging.log("Configoration values: Name: "+ name+". Value: /"+configFile.getProperty(name)+"/");
			}
			MALABI_IP = configFile.getProperty("malabiIp");
			MALABI_START_COMMAND = new StringBuilder("echo y | C:\\MALA-B\\plink.exe -ssh pi@").append(MALABI_IP).append(" -P ").toString();
			
			MALABI_PORT_BASE_LIGHT = Integer.valueOf(configFile.getProperty("malabiPort"));
//			MALABI_PROD_MODE = configFile.getProperty("malabiMode").equals("prod");
//			
			MALABI_PORT_BASE_SOUND = MALABI_PORT_BASE_LIGHT+10;
			
			userTime= Integer.valueOf(configFile.getProperty("malabiUserTimer"));
			MalaLogging.log("Finish initInfoAndTimer. Got values : MALABI_IP" +MALABI_IP  + "; MALABI_PORT_BASE_LIGHT : " +
					MALABI_PORT_BASE_LIGHT 	+ "; MALABI_PROD_MODE : " +MALABI_PROD_MODE  +";  MALABI_PORT_BASE_SOUND : " +MALABI_PORT_BASE_SOUND);
		}catch(Exception eta){
			MalaLogging.log("Error on initInfoAndTimer. Got values : MALABI_IP" +MALABI_IP  + "; MALABI_PORT_BASE_LIGHT : " +
					MALABI_PORT_BASE_LIGHT 	+ "; MALABI_PROD_MODE : " +MALABI_PROD_MODE  +";  MALABI_PORT_BASE_SOUND : " +MALABI_PORT_BASE_SOUND);
			MalaLogging.logError(eta);
		} finally {
			try {
				if (fis != null)
					fis.close();
			} catch (Exception ex) {
				MalaLogging.log("Error on initInfoAndTimer. Got values: MALABI_IP" +MALABI_IP  + "; MALABI_PORT_BASE_LIGHT : " +
						MALABI_PORT_BASE_LIGHT 	+ "; MALABI_PROD_MODE : " +MALABI_PROD_MODE  +";  MALABI_PORT_BASE_SOUND : " +MALABI_PORT_BASE_SOUND);
				MalaLogging.logError(ex);
		}
		}
		
		configFile.clear();
		
		AdamUtils.MALABI_IP = MALABI_IP;
		AdamUtils.MALABI_PORT_BASE_LIGHT = MALABI_PORT_BASE_LIGHT;
		AdamUtils.MALABI_PROD_MODE = MALABI_PROD_MODE;
		
		mTimerTask = new TimerTask() {
			
			@Override
			public void run() {
				mUserOnlineControl.checkStatus();

			}
		};
		mTimer = new Timer();
		mTimer.scheduleAtFixedRate(mTimerTask, userTime, userTime);
		
		SQLUtils.InitAllUsedData();
	}
	/*******************   Light *********************/
	@Override
	public Integer changeQuarterServer(int userID,Quarter quarter, Boolean state) throws IllegalArgumentException {
		if (!mUserOnlineControl.isUserActiveStatus(userID))
			return 2;
		if (!mUserOnlineControl.refreshUserStatus(userID))
			return 1;
		
		if (!SQLUtils.isControlerValidForUser(userID, quarter.mID))
				return 3;
		
		MalaLogging.log("Change Pole value to "+state+"; Quarter is"+quarter);
		ArrayList<Integer> poles = new ArrayList<Integer>();
		for (LightArea lightPole: quarter.mLightAreas ) {
			poles.add(lightPole.mAreaIndex);
		}
		return AdamUtils.CHANGE_LIGHT_AREAS_STATE(quarter, poles, state);
	}

	@Override
	public Integer changeAreaServer(int userID,Quarter quarter, LightArea lightPole, Boolean state) throws IllegalArgumentException {
		if (!mUserOnlineControl.isUserActiveStatus(userID))
			return 2;
		if (!mUserOnlineControl.refreshUserStatus(userID))
			return 1;
		if (!SQLUtils.isControlerValidForUser(userID, quarter.mID))
			return 3;
		MalaLogging.log("Change Pole value to "+state+"; Pole is"+lightPole+ "; Quarter is "+lightPole.getQuarter());
		ArrayList<Integer> lightAreasIndexsOfSameQuarter = new ArrayList<Integer>();
		lightAreasIndexsOfSameQuarter.add(lightPole.mAreaIndex);
		
		return  AdamUtils.CHANGE_LIGHT_AREAS_STATE(quarter, lightAreasIndexsOfSameQuarter, state);
	}
	

	/**
	 * 0 = good change
	 * -1 = problem on changing
	 * -2 = no communication with adam
	 * 1 = user is logged off
	 * 2 = user is not active.
	 * **/
	@Override
	public Integer playAudioServer(int userID, SoundInfo soundInfo, ArrayList<SoundPole> polesListToPlay) throws IllegalArgumentException {
		int res;
		String soundPolesIds = SQLUtils.GET_CONTROLLES__ID_LIST(new ArrayList<Quarter>(), polesListToPlay);
		res = SQLUtils.isUserActiveAndCanUsePole(userID, soundPolesIds, polesListToPlay.size());
		if (res != 1)
			return res;
		if (!mUserOnlineControl.refreshUserStatus(userID))
			return 1;

		MalaLogging.log("Playing audio file: "+ soundInfo.mDisplayName+" in: "+polesListToPlay);
		mUserOnlineControl.refreshUserStatus(userID);
		ArrayList<StringBuilder> playCommands= new ArrayList<StringBuilder>();
		for  (SoundPole pole : polesListToPlay) {
			playCommands.add(buildPlayCommand(pole.mQuarterId, soundInfo.mFileName));
		}
		for (StringBuilder playCommand : playCommands) {
			doCommand(playCommand.toString(), false);			
		}
		return 0;
	}
	
	private static boolean doCommand(String command, boolean waitForRes) {

		try {
			ProcessBuilder processBuilder =buildProcessBuilder(command);
			MalaLogging.log("Command: "+command);
			processBuilder.redirectErrorStream(true);
			final Process process = processBuilder.start();

			if (!waitForRes)
				return true;


			if(!process.waitFor(POLE_TIMEOUT, TimeUnit.SECONDS)) {
				MalaLogging.log("Process took more then "+POLE_TIMEOUT+" seconds.");
				process.destroy(); // consider using destroyForcibly instead
				return false;
			}
			
			InputStream stderr = process.getInputStream();
			InputStreamReader isr = new InputStreamReader(stderr);
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			while ((line = br.readLine()) != null) {
				MalaLogging.log("Adan Info: "+line);
			}
			//process.waitFor();
			
			int exitVaue = process.exitValue();
			MalaLogging.log("Returned Value :" + exitVaue);
			if (exitVaue == 0)
				return true;
			else
				return false;
		} catch (Exception e) {
			MalaLogging.log("Error doing Adam command: "+command);
			MalaLogging.logError(e);
			return false;
		}
	}
	
	private static ProcessBuilder buildProcessBuilder(String command) {
		ArrayList<String> commands = new ArrayList<String>();
		commands.add("cmd");
		commands.add("/c");
		commands.add(command);
		return new ProcessBuilder(commands);
	}

	private static StringBuilder buildCommand(int quarterNum) {
		return new StringBuilder(MALABI_START_COMMAND).append(MALABI_PORT_BASE_SOUND+quarterNum).append(" -pw #eltel10 ");
	}
	
	private static String buildAdamCommand(String val) {
		return new StringBuilder(" \"").append(val).append("\"").toString();
	}
	
	private static StringBuilder buildStartConnectionCommand(int quarterNum) {
		return buildCommand(quarterNum).append(buildAdamCommand("exit"));
	}
	
	private StringBuilder buildPlayCommand(int quarterNum, String audioname) {
		return buildCommand(quarterNum).append("\"").append(STOP_ADAM_COMMAND).append(ADD_PLAY_ADAM_COMMAND)
			.append(audioname).append("'\"\"");
	}
	
	private StringBuilder buildStopCommand(int quarterNum) {
		return buildCommand(quarterNum).append(buildAdamCommand(STOP_ADAM_COMMAND));
	}
	
	/*******************   Sound *********************/
	@Override
	public Integer stopAudioServer(int userID, ArrayList<SoundPole> polesListToStop) throws IllegalArgumentException {
		if (!mUserOnlineControl.isUserActiveStatus(userID))
			return 2;
		if (!mUserOnlineControl.refreshUserStatus(userID))
			return 1;
		for (SoundPole soundPole : polesListToStop) {
			if (!SQLUtils.isControlerValidForUser(userID, soundPole.mID))
				return 3;
		}
		mUserOnlineControl.refreshUserStatus(userID);
		MalaLogging.log("Stoping audio file");
		ArrayList<StringBuilder> stopCommands= new ArrayList<StringBuilder>();
		for  (SoundPole pole : polesListToStop) {
			stopCommands.add(buildStopCommand(pole.mQuarterId));
		}
		for (StringBuilder stopCommand : stopCommands) {
			doCommand(stopCommand.toString(),true);
		}
		return 0;
	}

	@Override
	public LightAreasStatus getAreasInQuarterState(int numOfAreasInQuarter, int quarterNum) throws IllegalArgumentException {
		return AdamUtils.GET_LIGHT_AREAS_STATE(numOfAreasInQuarter, quarterNum);
	}

	@Override
	public Boolean isUserLoggedOffServer(int userID) throws IllegalArgumentException {
		return mUserOnlineControl.isUserLoggedOff(userID);
	}

	/** 
	 * Getting the list of audio. This is done when loading the sound view because new
	 * tracks may be added when the user is online. 
	 */
	@Override
	public ArrayList<SoundInfo> getSoundInfoList() throws IllegalArgumentException {
		return SQLUtils.GET_AUDIO_LIST();
	}

	@Override
	public Boolean getPoleState(int poleNum) throws IllegalArgumentException {
		return doCommand(buildStartConnectionCommand(poleNum).toString(),true);
	}

}
