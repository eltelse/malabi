package com.eltel.malabi.server;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class MalaLogging {
    private static Logger Mala_Logger;

    private MalaLogging() {
    	try {
    		//instance the logger
    		Mala_Logger = Logger.getLogger(MalaLogging.class.getName());
    		//instance the filehandler

    		SimpleDateFormat dateFormatter= new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
    		Date date = new Date(System.currentTimeMillis());
    		StringBuilder logName = new StringBuilder().append("C:/MALA-B/logs/Malabi-")
    				.append(dateFormatter.format(date)).append(".log");
    		FileHandler fh= new FileHandler(logName.toString().toString());
			fh.setFormatter(new SimpleFormatter());  
    		Mala_Logger.addHandler(fh);
    		System.setProperty("hsqldb.reconfig_logging", "false");
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
    }
    
    private static Logger getLogger() {
        if(Mala_Logger == null) {
                new MalaLogging();
        }
        return Mala_Logger;
    }
    
    public static void log( String msg) {
        getLogger().info(msg);
    }
    
    public static void logError(Throwable e) {
        getLogger().log(Level.INFO, e.getMessage(), e);
    }
}