package com.eltel.malabi.server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import com.eltel.malabi.client.LightArea;
import com.eltel.malabi.client.MalabiInfo;
import com.eltel.malabi.client.MalabiUser;
import com.eltel.malabi.client.Quarter;
import com.eltel.malabi.client.SoundInfo;
import com.eltel.malabi.client.SoundPole;

public class SQLUtils {

	private static String USERS_TABLE_NAME = "Users";
	private static String SOUND_TABLE_NAME = "sound_Files";
	private static String CONTROLLERS_TABLE_NAME = "Controllers";
	private static String MDB_FILE_NAME = "jdbc:ucanaccess://C:/MALA-B/audioFiles/users.mdb";
	private static String LIGHT_VALUE = "�����";
	private static String SOUND_VALUE = "�����";


	private static String USER_NAME_COLUMN = "User_name";
	private static String USER_PASSWORD_COLUMN = "Password";
	private static String USER_FIRST_NAME_COLUMN = "First_Name";
	private static String USER_ID_COLUMN = "User_ID";
	private static String USER_LOGGED_IN = "LOGGED_IN";
	private static String USER_LAST_NAME = "LAST_NAME";
	private static String USER_EXPIRE_DATE_COLUMN ="expire_Date";
	private static String USER_IS_ACTIVE_COLUMN = "Is_Active";
	
	
	private static String SOUND_ID_COLUMN = "sound_File_ID";
	private static String SOUND_FILE_NAME_COLUMN = "sound_File_Name";
	private static String SOUND_DURANCE_COLUMN = "durance";
	private static String SOUND_DISPLAY_NAME_COLUMN = "display_Name";
	
	private static String POLE_QUARTER_COLUMN ="quarter";
	private static String POLE_ID_COLUMN = "Controller_ID";
	private static String POLE_NAME_COLUMN = "Controller_Name";
	private static String POLE_LOCATION_COLUMN = "Controller_Location";
	private static String POLE_IS_ACTIVE_COLUMN = "is_Active";
	private static String POLE_IS_USED_COLUMN = "is_In_Use";
	private static String POLE_CONTROLLER_TYPE = "Controller_type";
	
	private static String PERMISSIONS_TABLE_NAME ="Permissions";
	private static String PERMISSIONS_USER_ID_COLUMN ="User_ID";
	private static String PERMISSIONS_CONTROLLER_COLUMN ="Controller_ID";
	private static String PERMISSIONS_IS_ACTIVE_COLUMN ="Is_Active";

//	private static String PERMISSIONS_EXPIRE_DATE_COLUMN ="ExpireDate";
	
	private static String QUARTER_NUM_OF_AREA = "num_Of_Areas";
	
	private static boolean mIsFirstSqlUse = true;
	
	private static  DateTimeFormatter DURATION_FORMAT =DateTimeFormatter.ofPattern("HH:mm:ss");

    

	/**
	 * Loading SQL driver on the first login to the system.
	 */
	private static void initSqlDriver() {
		if (mIsFirstSqlUse) {
			mIsFirstSqlUse = false;
			try {

				Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
			}
			catch(ClassNotFoundException cnfex) {

				MalaLogging.log("Problem in loading or registering MS Access JDBC driver");
				MalaLogging.logError(cnfex);
			}
		}
		else {
			return;
		}

	}
	
	public static MalabiInfo LOGIN(String userName, String password, UserOnlineControl userOnlineControl){

		initSqlDriver();

		Connection connection = null;
		Statement statement = null;
		MalabiUser malabiUser = null;
//		ArrayList<SoundInfo> soundFliesList =null;
		ArrayList<Quarter> quartersList = null;
		ArrayList<SoundPole> soundPoleList = null;
		try {
			connection = DriverManager.getConnection(MDB_FILE_NAME); 
			statement = connection.createStatement();
			malabiUser = GET_MALABI_USER(userName, password, statement);

			if (!malabiUser.isShouldGetMoreUserInfo()) {
				// Issue with the user details. No reason to check any other info
				return GetResAndCloseConnection(connection, statement, malabiUser, null, null);//,"");
			}
			
			
			CHANGE_USER_LOGGED_IN_STATE(malabiUser.mID, true);

//			soundFliesList =  GET_SOUND_FILES_LIST(statement);
			
			quartersList = GET_LIGHT_AREA_LIST(malabiUser.mID, statement);
			soundPoleList =  GET_SOUND_POLE_LIST(malabiUser.mID, statement);
			
			if (!quartersList.isEmpty() || !soundPoleList.isEmpty())
			{
				CHANGE_CONTROLLER_USED_STATE(quartersList, soundPoleList, true);
			}
		}
		catch(Exception sqlex){
			MalaLogging.log("Error on LOGIN with user: "+userName+"; password: "+password);
			MalaLogging.logError(sqlex);
			return GetResAndCloseConnection(connection, statement, null, null, null);//,""");
		}

		if (malabiUser== null || quartersList== null || soundPoleList== null)
			return GetResAndCloseConnection(connection, statement, null, null, null);//,"");
		else
			return GetResAndCloseConnection(connection, statement, malabiUser, quartersList, soundPoleList);//, badQuarters);
	}

	public static boolean isUserActive(int userID) {
		Connection connection = null;
		Statement statement = null;
		boolean activeState = false;

		try {
			connection = DriverManager.getConnection(MDB_FILE_NAME); 
			statement = connection.createStatement();
			activeState = GET_USER_ACTIVE_STATE(userID, statement);
			
		}
		catch(Exception sqlex){
			MalaLogging.log("Error on isUserActive with userID: "+userID);
			MalaLogging.logError(sqlex);
		}
		closeConnection(connection, statement);
		return activeState;

	}
	public static int isUserActiveAndCanUsePole(int userID, String soundPolesIds, int numOfPoles ){
		Connection connection = null;
		Statement statement = null;
		boolean validState = false;

		try {
			connection = DriverManager.getConnection(MDB_FILE_NAME); 
			statement = connection.createStatement();
			MalaLogging.log("Checking User "+userID+"  active state.");
			boolean activeState = GET_USER_ACTIVE_STATE(userID, statement);
			
			if (!activeState)
				return 2;
			MalaLogging.log("User "+userID+"  is active.");
			
			validState = GET_USER_CONTROLERS_VALID_STATE(userID, soundPolesIds, numOfPoles, statement);
		}
		catch(Exception sqlex){
			MalaLogging.log("Error on isUserActiveAndCanUsePole with userID: "+userID);
			MalaLogging.logError(sqlex);
		}
		closeConnection(connection, statement);
		
		return validState ? 1 : 3;
	}
	/**
	 * Closing the connection and Getting all the log-in info. 
	 * @param connection
	 * @param statement
	 * @param malabiUser
	 * @param soundFliesList
	 * @param quartersList
	 * @param soundPoleList
	 * @return
	 */
	private static MalabiInfo GetResAndCloseConnection(Connection connection,	Statement statement, 
			MalabiUser malabiUser, ArrayList<Quarter> quartersList, ArrayList<SoundPole> soundPoleList) {
		closeConnection(connection,	statement);

		return new MalabiInfo(malabiUser, quartersList, soundPoleList);
	}
	
	private static void closeConnection(Connection connection,	Statement statement) {
		try {
			if (connection != null) {
				statement.close();
				connection.close();
			}
		}
		catch (Exception sqlex) {
			MalaLogging.log("Error on CloseConnection");
			MalaLogging.logError(sqlex);
		}
		
	}


	public static void LOGOFF(ArrayList<Quarter> quartersList, ArrayList<SoundPole> soundPoleList, int userID) {
		CHANGE_USER_LOGGED_IN_STATE(userID, false);
		if (!quartersList.isEmpty() || !soundPoleList.isEmpty())
		{
			CHANGE_CONTROLLER_USED_STATE(quartersList, soundPoleList, false);
		}
	}

	private static ArrayList<Quarter> GET_LIGHT_AREA_LIST(int userID,Statement statement) throws Exception{
		ResultSet resultSet = statement.executeQuery(GET_QUERY(userID, LIGHT_VALUE));

		int quarterNum;
		Quarter quarter;
		ArrayList<Quarter> res = new ArrayList<Quarter>();
		while (resultSet.next())
		{
			quarter = createQuarter(resultSet);
			MalaLogging.log("Get info of quarter on DB: "+quarter);
			quarterNum = quarter.mQuarterNum;
			for (int areaIndex=0 ; areaIndex <quarter.mNumOfAreasInQuarter; areaIndex++) {
				quarter.addLightArea(new LightArea(quarterNum+"-"+areaIndex,quarterNum,areaIndex ));
			}
			res.add(quarter);
		}
		resultSet.close();
		return res;
	}
	
	private static String GET_QUERY(int userID, String poleType) {
		StringBuilder quaryBuilder = new  StringBuilder().append("Select "+"poles.*").append(
				" from ").append(
				CONTROLLERS_TABLE_NAME).append(" poles ").append(
				" inner join ").append(PERMISSIONS_TABLE_NAME ).append(" perm").append(
				" on poles.").append(POLE_ID_COLUMN).append(" = perm.").append(PERMISSIONS_CONTROLLER_COLUMN).append(
				" where ");
		quaryBuilder.append(
				" perm.").append(PERMISSIONS_USER_ID_COLUMN).append(" = ").append(userID).append(
				" and poles.").append(POLE_CONTROLLER_TYPE).append(" = '").append(poleType).append(
				"' and  perm.").append(PERMISSIONS_IS_ACTIVE_COLUMN).append(" = true").append(
				" and  poles.").append(POLE_IS_ACTIVE_COLUMN).append(" = true").append(
				" and  poles.").append(POLE_IS_USED_COLUMN).append(" = false").append(
				" order by poles.").append(POLE_QUARTER_COLUMN).append(" asc");
		MalaLogging.log("Get "+poleType+" list query: "+quaryBuilder);
		return quaryBuilder.toString();
	}
	
	public static boolean isControlerValidForUser(int userID, int controlerID) {
		Connection connection = null;
		Statement statement = null;
		boolean validState = false;

		try {
			connection = DriverManager.getConnection(MDB_FILE_NAME); 
			statement = connection.createStatement();
			validState = GET_USER_CONTROLER_VALID_STATE(userID, controlerID,statement);
			
		}
		catch(Exception sqlex){
			MalaLogging.log("Error on isControlerValidForUser with userID: "+userID);
			MalaLogging.logError(sqlex);
		}
		closeConnection(connection, statement);
		
		return validState;
	}
	
	public static boolean GET_USER_CONTROLER_VALID_STATE(int userID, int controlerID,Statement statement) throws Exception
	{
		StringBuilder query = new StringBuilder().append("SELECT * FROM ").append(PERMISSIONS_TABLE_NAME).append(
				" where ").append(PERMISSIONS_USER_ID_COLUMN).append(" = ").append(userID).append(
						" and ").append(PERMISSIONS_CONTROLLER_COLUMN).append(" = ").append(controlerID).append("");
		MalaLogging.log("Getting controler valid for user from DB by IDs: "+query.toString());
		boolean res = false;
		try {
			ResultSet resultSet = statement.executeQuery(query.toString());
			if (resultSet.next() ) {
				res = resultSet.getBoolean(PERMISSIONS_IS_ACTIVE_COLUMN);
				MalaLogging.log("Controler "+res+" active for user;");
			} else {
				MalaLogging.log("Controler not exit for user;");
			}
			resultSet.close();
			return res;
		} catch (Exception e) {
			MalaLogging.log("Error on GET_USER_CONTROLER_ACTIVE_STATE");
			MalaLogging.logError(e);
			return false;
		}
	}
	
	public static boolean GET_USER_CONTROLERS_VALID_STATE(int userID, String controlersIds , int size, Statement statement) throws Exception
	{
		StringBuilder query = new StringBuilder().append("SELECT * FROM ").append(PERMISSIONS_TABLE_NAME).append(
				" where ").append(PERMISSIONS_USER_ID_COLUMN).append(" = ").append(userID).append(
						" and ").append(PERMISSIONS_CONTROLLER_COLUMN).append(" in ").append(controlersIds).append("");
		MalaLogging.log("Getting controler valid for user from DB by IDs: "+query.toString());
		boolean res = false;
		try {
			ResultSet resultSet = statement.executeQuery(query.toString());
			int counter = 0;
			while (resultSet.next() ) {
				counter ++;
				res = resultSet.getBoolean(PERMISSIONS_IS_ACTIVE_COLUMN);
				if (!res) {
					MalaLogging.log("Controler not exit for user;");
					resultSet.close();
					return false;
				}
			}
			if (counter <  size) {
				MalaLogging.log("Controler not exit for user;");
				resultSet.close();
				return false;
			}
				
			resultSet.close();
			return res;
		} catch (Exception e) {
			MalaLogging.log("Error on GET_USER_CONTROLER_ACTIVE_STATE");
			MalaLogging.logError(e);
			return false;
		}
	}
	
	private static ArrayList<SoundPole> GET_SOUND_POLE_LIST(int userID, Statement statement) throws Exception{
		MalaLogging.log("GET_SOUND_POLE_LIST");
		ArrayList<SoundPole> res  = new ArrayList<SoundPole>();		
		ResultSet resultSet = statement.executeQuery(GET_QUERY(userID, SOUND_VALUE));
		
		SoundPole soundPole;
		while (resultSet.next())
		{
			soundPole = createSoundPole(resultSet);
			MalaLogging.log("Get info of Sound pole on DB: "+soundPole);
				res.add(soundPole);
		}

		resultSet.close();
		return res;
	}

	public static ArrayList<SoundInfo> GET_SOUND_FILES_LIST(Statement statement) throws Exception
	{
		SoundInfo soundInfo;
		ArrayList<SoundInfo> res = new ArrayList<SoundInfo>();
		StringBuilder query = new StringBuilder().append("SELECT * FROM ").append(SOUND_TABLE_NAME);
		ResultSet resultSet = statement.executeQuery(query.toString());
		while (resultSet.next())
		{
			soundInfo = createSoundInfo(resultSet);
			MalaLogging.log("Get info of Audio on DB: "+soundInfo);
			res.add(soundInfo);
		}
		
		resultSet.close();
		return res;
	}

	public static MalabiUser GET_MALABI_USER(String userName, String password, Statement statement) throws Exception
	{
		MalaLogging.log(new StringBuilder().append("Try Login user: *").append(userName).append("*, password: *").append(password).append("*").toString());
		StringBuilder query = new StringBuilder().append("SELECT * FROM ").append(USERS_TABLE_NAME).append(
					" where ").append(
								     USER_NAME_COLUMN).append(" = '").append(userName).append(
					"' and ").append(USER_PASSWORD_COLUMN).append(" = '").append(password).append("'");
//					"' and ").append(USER_EXPIRE_DATE_COLUMN).append(" >= '").append(sqlDate).append("'");
							//"' and  ").append(USER_IS_ACTIVE_COLUMN).append(" = true");
		MalaLogging.log("Getting user from DB: "+query.toString());
		ResultSet resultSet = statement.executeQuery(query.toString());
		if (resultSet.next() ) {
			MalabiUser res = getMalabiUser(resultSet);
			MalaLogging.log("Login user result: "+res);
			resultSet.close();
			return res;
		} else {
			MalaLogging.log("Fail login. No user, or wrong Password");
			resultSet.close();
			return new MalabiUser();
		}
	}
	
	public static boolean GET_USER_ACTIVE_STATE(int userID, Statement statement) throws Exception
	{
		StringBuilder query = new StringBuilder().append("SELECT * FROM ").append(USERS_TABLE_NAME).append(
				" where ").append(USER_ID_COLUMN).append(" = ").append(userID);
		MalaLogging.log("Getting user from DB by ID: "+query.toString());
		boolean res = false;
		try {
			ResultSet resultSet = statement.executeQuery(query.toString());
			if (resultSet.next() ) {
				res = resultSet.getBoolean(USER_IS_ACTIVE_COLUMN);
				MalaLogging.log("Login Active result: "+res);
			} else {
				MalaLogging.log("Fail get user active state. No user with this ID");
			}
			resultSet.close();
			return res;
		} catch (Exception e) {
			MalaLogging.log("Error on GET_MALABI_USER_BY_ID");
			MalaLogging.logError(e);
			return false;
		}
	}
	
	private static MalabiUser getMalabiUser(ResultSet resultSet) {
		try {
			return new MalabiUser(
					resultSet.getInt(USER_ID_COLUMN),
					new StringBuilder().append(
					resultSet.getString(USER_FIRST_NAME_COLUMN)).append(" ").append(
					resultSet.getString(USER_LAST_NAME)).toString(),
					resultSet.getBoolean(USER_IS_ACTIVE_COLUMN),
					resultSet.getDate(USER_EXPIRE_DATE_COLUMN).getTime() >= getCurrentTime(),
					resultSet.getBoolean(USER_LOGGED_IN)); 
		} catch (Exception e) {
			MalaLogging.log("Error on getMalabiUser");
			MalaLogging.logError(e);
		}
		return new MalabiUser();
	}
	
	private static long getCurrentTime() {
		return new java.util.Date().getTime();
	}
//	private static Pole createPole(ResultSet resultSet) {
//		try {
//			return new Pole(
//					resultSet.getInt(POLE_ID_COLUMN),
//					resultSet.getString(POLE_NAME_COLUMN),
//					resultSet.getBoolean(POLE_IS_USED_COLUMN),
//					resultSet.getInt(POLE_QUARTER_COLUMN),
//					resultSet.getInt(POLE_ADAM_CODE_COLUMN));// ADAM CODE FOR EXAMLEl
//					//resultSet.getString(POLE_ADAM_CODE_COLUMN));
//
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return new Pole();
//	}
	
	private static SoundPole createSoundPole(ResultSet resultSet) {
		try {
			return new SoundPole(
					resultSet.getInt(POLE_ID_COLUMN),
					resultSet.getString(POLE_NAME_COLUMN),
					resultSet.getInt(POLE_QUARTER_COLUMN),
					resultSet.getString(POLE_LOCATION_COLUMN));// ADAM CODE FOR EXAMLEl
					//resultSet.getString(POLE_ADAM_CODE_COLUMN));

		} catch (Exception e) {
			MalaLogging.log("Error on createSoundPole");
			MalaLogging.logError(e);
		}
		return new SoundPole();
	}
	
	
	private static Quarter createQuarter(ResultSet resultSet) {
		try {
			return new Quarter(
					resultSet.getInt(POLE_ID_COLUMN),
					resultSet.getInt(QUARTER_NUM_OF_AREA),
					resultSet.getInt(POLE_QUARTER_COLUMN));
		} catch (Exception e) {
			MalaLogging.log("Error on createQuarter");
			MalaLogging.logError(e);
		}
		return new Quarter();
	}
	
	/**
	 * Convert string of type "01:02:03" to int of the seconds value.
	 * @param duration The vale to convert
	 * @return The converted seconds int value.
	 */
	public static int  Get_Seconds_OF_Durance(String duration) {
		return LocalTime.parse(duration, DURATION_FORMAT).toSecondOfDay() + 3;
	}
	
	private static SoundInfo createSoundInfo(ResultSet resultSet) {
		//SOUND_FILE_ID, SOUND_FILE_NAME, DISPLAY_NAME, PATH, DATE_CREATED, DATE_MODIFIED, DURANCE]
		try {
			String durance = resultSet.getString(SOUND_DURANCE_COLUMN);
			return new SoundInfo(

					resultSet.getInt(SOUND_ID_COLUMN),
					resultSet.getString(SOUND_FILE_NAME_COLUMN),
					durance,
					resultSet.getString(SOUND_DISPLAY_NAME_COLUMN),
					Get_Seconds_OF_Durance(durance));
		} catch (Exception e) {
			MalaLogging.log("Error on createSoundInfo");
			MalaLogging.logError(e);
		}
		return new SoundInfo();
	}
	
	static String GET_CONTROLLES__ID_LIST(ArrayList<Quarter> quarters, ArrayList<SoundPole> soundPoles) {
		StringBuilder res = new StringBuilder();
		StringBuilder listId = new StringBuilder();
		res.append(" ( ");
		for (Quarter quarter : quarters) {
			listId.append(quarter.mID).append(", ");
		}
		for (SoundPole soundPole : soundPoles) {
			listId.append(soundPole.mID).append(", ");
		}
		if (!listId.toString().isEmpty())
			res.append(listId.substring(0,listId.length() -2));
		res.append(" )");
		return res.toString();
	}

	public static void CHANGE_USER_LOGGED_IN_STATE(int userID, Boolean state) throws IllegalArgumentException {
		Connection connection = null;
		PreparedStatement preparedStmt = null;
		StringBuilder query = new StringBuilder();
		try {
			query.append("update ").append(USERS_TABLE_NAME)
				.append(" set ").append(USER_LOGGED_IN).append(" = ").append(state)  //new State  - true / false
				.append(" where ").append(USER_ID_COLUMN).append(" = ").append(userID);  //which user- user ID
			MalaLogging.log("Change USER LOGGED state query: "+query.toString());
			connection = DriverManager.getConnection(MDB_FILE_NAME); 
			preparedStmt = connection.prepareStatement(query.toString());
			preparedStmt.executeUpdate();
		}
		catch(Exception sqlex){
			MalaLogging.log("Error on CHANGE_USER_LOGGED_IN_STATE. Query is: "+query.toString());
			MalaLogging.logError(sqlex);
		}
		
		if(null != connection) {
			try {
				preparedStmt.close();
				connection.close();
			}
			catch (Exception e) {
				MalaLogging.log("Error on CHANGE_USER_LOGGED_IN_STATE. Query is: "+query.toString());
				MalaLogging.logError(e);
			}
		}
	}
	
	private static void CHANGE_CONTROLLER_USED_STATE(ArrayList<Quarter> quarters, ArrayList<SoundPole> soundPoles, Boolean state) throws IllegalArgumentException {
		String idsList = GET_CONTROLLES__ID_LIST(quarters, soundPoles);
		Connection connection = null;
		PreparedStatement preparedStmt = null;
		StringBuilder query= new StringBuilder();
		try {
			connection = DriverManager.getConnection(MDB_FILE_NAME); 
			query.append("update ").append(CONTROLLERS_TABLE_NAME)
					.append(" set ").append(POLE_IS_USED_COLUMN).append(" = ").append(state)  //new State  - true / false
					.append(" where ").append(POLE_ID_COLUMN).append(" in ").append(idsList);  //which pole - pole ID
			MalaLogging.log("Change controller state query: "+query.toString());
			preparedStmt = connection.prepareStatement(query.toString());
			preparedStmt.executeUpdate();
		}
		catch(Exception sqlex){
			MalaLogging.log("Error on CHANGE_CONTROLLER_USED_STATE. Query is: "+query.toString());
			MalaLogging.logError(sqlex);
		}
		
		if(null != connection) {
			try {
				preparedStmt.close();
				connection.close();
			}
			catch (Exception e) {
				MalaLogging.log("Error on CHANGE_CONTROLLER_USED_STATE. Query is: "+query.toString());
				MalaLogging.logError(e);
			}
		}
	}
	
	public static ArrayList<SoundInfo> GET_AUDIO_LIST(){

		initSqlDriver();

		Connection connection = null;
		Statement statement = null;
		try {
			connection = DriverManager.getConnection(MDB_FILE_NAME); 
			statement = connection.createStatement();

			return (GET_SOUND_FILES_LIST(statement));
			
		}
		catch(Exception sqlex){
			MalaLogging.log("Error on GET_AUDIO_LIST");
			MalaLogging.logError(sqlex);
		}

			return new ArrayList<SoundInfo>();
	}
	
	public static void InitAllUsedData() {
		Connection connection = null;
		PreparedStatement preparedStmt = null;
		try {
			connection = DriverManager.getConnection(MDB_FILE_NAME); 

			String query = "update "+CONTROLLERS_TABLE_NAME+" set "+POLE_IS_USED_COLUMN+" = false;";
			MalaLogging.log(query.toString());
			preparedStmt = connection.prepareStatement(query);
			preparedStmt.executeUpdate();
			preparedStmt.close();
			query = "update "+USERS_TABLE_NAME+" set "+USER_LOGGED_IN+" = false;";
			MalaLogging.log(query.toString());
			preparedStmt = connection.prepareStatement(query);
			preparedStmt.executeUpdate();
			connection.close();
		}
		catch(Exception sqlex){
			sqlex.printStackTrace();
		}
		try {
			if (connection != null) {
				preparedStmt.close();
				connection.close();
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

//	public static void initLogger(Logger malabiLogger) {
//		MALABI_LOGGER = malabiLogger;
//		MALABI_LOGGER.info("in sql");
//	}
	
}
