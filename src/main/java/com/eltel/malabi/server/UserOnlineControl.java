package com.eltel.malabi.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import com.eltel.malabi.client.MalabiInfo;

public class UserOnlineControl {

	public HashMap<Integer, Boolean> mUserActiveStatus =  new HashMap<Integer, Boolean>();
	public HashMap<Integer, MalabiInfo> mUserData =  new HashMap<Integer, MalabiInfo>();
	
	public static Logger MALABI_LOGGER; 
	
	
	public UserOnlineControl(){
	}
	
	/**
	 * Add new user to control
	 * @param index the user index
	 */
	public void addUser(int userID, MalabiInfo malabiInfo) {
		if (mUserData.get(userID) != null) {
			MalaLogging.log("User "+userID+" is re-logging to the system. Skipping adding info to the Control; Only refresh status.");
		}
		else {
			MalaLogging.log("Adding user "+userID+" to user control;");
			mUserActiveStatus.put(userID, true);
			mUserData.put(userID, malabiInfo);
		}
	}

	public MalabiInfo getUserInfo(int userID) {
		return mUserData.get(userID);
	}
	
	
	public boolean isUserActiveStatus(int userID) {
		MalaLogging.log("Checking User "+userID+"  active state.");
		if (!SQLUtils.isUserActive(userID)) {
			MalaLogging.log("User "+userID+" is NOT ACTIVE. Can't use the syste.");
			return false;
		}
		MalaLogging.log("User "+userID+"  is active.");
		return true;
	}
	
	/**
	 * Refresh user status when user is active
	 * @param index User status.
	 */
	public boolean refreshUserStatus(int userID) {
		MalaLogging.log("User "+userID+" is using the system. Setting to active state.");
		if (mUserActiveStatus.get(userID) == null) {
			MalaLogging.log("User "+userID+" is offline. Can't do action.");
			return false;
		}
		mUserActiveStatus.replace(userID, true);
		return true;
		
	}
	
	/**
	 * Init all user status to FALSE when loop is start.
	 * If on the end of loop user is still FALSE, then the user should be logged off.
	 */
	public void startLoop() {
//		MalaLogging.log("Init users status to offline.");
		for (Integer userID : mUserActiveStatus.keySet()) {
			mUserActiveStatus.replace(userID, false);
		}
	}
	
	/**
	 * Check status for all users in the end of loop. Logging off users not active;
	 */
	public void checkStatus() {
//		MalaLogging.log("Check status of all users, as it is end of timer countdown.");
		ArrayList<Integer> usersToRemove = new ArrayList<Integer>();
		for (Integer userID : mUserActiveStatus.keySet()) {
			if (!mUserActiveStatus.get(userID))
			{
				MalaLogging.log("User "+userID+" is not using the system for long time, logging him off and removing the user off the list.");
				logoffUser(userID, true);
				usersToRemove.add(userID);
			}
			else if (!isUserActiveStatus(userID))
			{
				MalaLogging.log("User "+userID+" is not ACTIVE in DB, logging him off and removing the user off the list.");
				logoffUser(userID, true);
				usersToRemove.add(userID);
			}
			
			else {
				MalaLogging.log("User "+userID+" is online.");
			}
		}
		mUserActiveStatus.keySet().removeAll(usersToRemove);
		startLoop();
	}

	/**
	 * clear user data.
	 * @param userID
	 * @param clearDataBase
	 */
	public void logoffUser(int userID, boolean clearDataBase) {
		MalaLogging.log("Logging off user "+userID+ " by ");
		MalaLogging.log(clearDataBase ? "user control system." : "app action.");
		if (clearDataBase)
		{
			MalabiInfo malabiInfo= mUserData.get(userID);
			SQLUtils.LOGOFF(malabiInfo.mQuarters, malabiInfo.mSoundPoles, userID);
		}
		
		mUserData.get(userID).InitAll();
		mUserData.remove(userID);
	}
	
	public static void initLogger(Logger malabiLogger) {
		MALABI_LOGGER = malabiLogger;
	}
	
	public boolean isUserLoggedOff(int userId) {
		return mUserData.get(userId) == null;
	}
}
