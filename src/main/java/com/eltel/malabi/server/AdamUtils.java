package com.eltel.malabi.server;

import java.io.File;
import java.io.FileInputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Properties;

import com.eltel.malabi.client.LightAreasStatus;
import com.eltel.malabi.client.Quarter;

import net.wimpi.modbus.facade.ModbusTCPMaster;
import net.wimpi.modbus.util.BitVector;

public class AdamUtils {
	private static byte[] bytes = new byte[1];
	public static String MALABI_IP; //  "2.55.96.114" 8100
	public static int MALABI_PORT_BASE_LIGHT;
	public static boolean MALABI_PROD_MODE;
	private static String CONFIGURATION_FILE = "C:/MALA-B/config.cfg";
	private static String MALABI_SWITCH = "malabiSwitch";

	/**
	 * 0 - active good;
	 * 1- no Communication
	 * -1 - adam error;
	 * @param quarter
	 * @param poles
	 * @param state
	 * @return
	 */
	public static int CHANGE_LIGHT_AREAS_STATE(Quarter quarter, ArrayList<Integer> lightAreasIndexsOfSameQuarter, boolean state) {
		int quarterPort= MALABI_PORT_BASE_LIGHT + quarter.mQuarterNum;
		String ip = MALABI_IP+":"+quarterPort;
		if (!pingCheckToQuarter(quarter.mQuarterNum)) {
			return -2;
		}
		
		ModbusTCPMaster modbusTCPMaster = new ModbusTCPMaster(MALABI_IP, quarterPort);
		System.out.print("Writing state quarter "+quarter+" poles "+ lightAreasIndexsOfSameQuarter+"\t ip "+ip+"\t to "+state);
		int switchIndex;
		ArrayList<Integer> tagsIndexs= new ArrayList<Integer>();
		Properties configFile = new Properties();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(new File(CONFIGURATION_FILE));
			configFile.load(fis);
			modbusTCPMaster.connect();
			bytes[0] = 1;
			MalaLogging.log("State of quarter BEFORE change:"+ modbusTCPMaster.readCoils(17, 8).toString());
			for (int areaIndex : lightAreasIndexsOfSameQuarter) {
					switchIndex = (quarter.mQuarterNum) * 10 +areaIndex+1;	
					tagsIndexs = getTagsValues(configFile, switchIndex);
					for (Integer tagIndex : tagsIndexs)
						if (MALABI_PROD_MODE) {
							modbusTCPMaster.writeCoil(-1, 17+ tagIndex, state);
						}
						else {
							MalaLogging.log("Test - Changing adan state on port "+quarterPort +
									" in tag "+ tagIndex);
						}
			}
			MalaLogging.log("State of quarter AFTER change:"+ modbusTCPMaster.readCoils(17, 8).toString());

			modbusTCPMaster.disconnect();
			try {
				if (fis != null)
					fis.close();
			} catch (Exception ex) {
				MalaLogging.log("Error doing CHANGE_LIGHT_AREAS_STATE quarter: "+ quarter+" to state: "+ state);
				MalaLogging.logError(ex);
				configFile.clear();
			}
			return 0;
		} catch (Exception e) {
			modbusTCPMaster.disconnect();
			MalaLogging.log("ADAM FAIL Writing state quarter "+quarter+" poles "+ lightAreasIndexsOfSameQuarter+"\t ip "+ip+"\t to "+state);
			MalaLogging.logError(e);
			return -1;
		}
	}
		
	public static ArrayList<Integer> getTagsValues(Properties configFile, int switchIndex){
		ArrayList<Integer> res = new ArrayList<Integer>();
		String tagsList = configFile.getProperty(MALABI_SWITCH+switchIndex);
		try {

			for (String num : tagsList.split(","))
				res.add(Integer.valueOf(num));
		} catch (Exception e) {
		}
		return res;
	}
	
	public static boolean GET_SOUND_POLE_IS_VALID(int poleNum) {
		if (!pingCheckToQuarter(10+poleNum)) {
			return false;
		}
		return true;
	}

	public static LightAreasStatus GET_LIGHT_AREAS_STATE(int numOfAreasInQuarter, int quarterNum) {
		LightAreasStatus lightAreaStatus = new LightAreasStatus(numOfAreasInQuarter, quarterNum);
		if (!pingCheckToQuarter(quarterNum)) {
			lightAreaStatus.mReturnState =-1;
			return lightAreaStatus;
		}
		
		String qIP= MALABI_IP;
		int port = 8100+quarterNum;
		String ip = qIP+":"+port;
		MalaLogging.log("Checking quarter poles state of quarter "+quarterNum+" IP- "+ip);
		
		int switchIndex;
		Properties configFile = new Properties();
		FileInputStream fis = null;
		
		ModbusTCPMaster modbusTCPMaster = new ModbusTCPMaster(qIP, port);
		try {
			fis = new FileInputStream(new File(CONFIGURATION_FILE));
			configFile.load(fis);
			
			modbusTCPMaster.connect();
			BitVector quarterState = modbusTCPMaster.readCoils(17, 8);
	
			MalaLogging.log("read quarter \t ip "+ip+"\t"+numOfAreasInQuarter+" "+quarterState);
			boolean state;
			for (int areaIndex =0 ; areaIndex < numOfAreasInQuarter; areaIndex++) {
			 	switchIndex = (quarterNum) * 10 +areaIndex+1;	
			 	state = quarterState.getBit(getTagsValues(configFile, switchIndex).get(0));
				lightAreaStatus.mAreasState.add(state);
			}

			modbusTCPMaster.disconnect();
			
			try {
				if (fis != null)
					fis.close();
			} catch (Exception ex) {
				MalaLogging.log("Error doing GET_LIGHT_AREAS_STATE quarter: "+ quarterNum+" numOfAreasInQuarter: "+ numOfAreasInQuarter);
				MalaLogging.logError(ex);
				configFile.clear();
			}
			
			lightAreaStatus.mReturnState = 0;
			return lightAreaStatus;
		} catch (Exception e) {
			modbusTCPMaster.disconnect();
			
			try {
				if (fis != null)
					fis.close();
			} catch (Exception ex) {
				MalaLogging.log("Error doing GET_LIGHT_AREAS_STATE quarter: "+ quarterNum+" numOfAreasInQuarter: "+ numOfAreasInQuarter);
				MalaLogging.logError(ex);
				configFile.clear();
			}
			
			MalaLogging.log("ADAM Fail connecting and checking quarter "+quarterNum+" IP- "+ip);
			MalaLogging.logError(e);
			lightAreaStatus.mReturnState = -2;
			return lightAreaStatus;
		}
	}
	
	private static boolean pingCheckToQuarter(int quarterIndex) {
		try {
 
			try (Socket crunchifySocket = new Socket()) {
				// Connects this socket to the server with a specified timeout value.
				crunchifySocket.connect(new InetSocketAddress("2.55.96.114", 8100+ quarterIndex), 2000);

			} catch (Exception exception) {
				MalaLogging.log("Error pinging to quarter: "+ quarterIndex + " - 2.55.96.114 : "+ 8100+ quarterIndex);
				MalaLogging.logError(exception);
				return false;
			}
			// Return false if connection fails
			// Return true if connection successful
			return true;
		} catch (Exception exception) {
			MalaLogging.log("Error pinging to quarter: "+ quarterIndex + " - 2.55.96.114 : "+ 8100+ quarterIndex);
			MalaLogging.logError(exception);
 
			// Return false if connection fails
			return false;
		}
	}


	//	    public static void main(String[] args) throws Exception {
	//	        ModbusTCPMaster modbusTCPMaster = new ModbusTCPMaster("192.168.112.105");
	//	        modbusTCPMaster.connect();
	//	        userIt(modbusTCPMaster ,  (byte) 1 , (byte)1);
	//	    };
}
