package com.eltel.malabi.client;

import com.eltel.malabi.client.messages.MalaMessages;
import com.google.gwt.core.client.GWT;

public class StaticInfo {
	
	public static int USER_ID;
	public static MalabiInfo MALABI_INFO;;
	
	private static MalabiDBServiceAsync MALABI_DB_SERVICE_ASYNC;
	
	private static MalaMessages MALABI_MESSAGES;
	
	public static MalabiDBServiceAsync GET_MalabiDBServiceAsync() {
		if (MALABI_DB_SERVICE_ASYNC != null) {
			return MALABI_DB_SERVICE_ASYNC;
		}
		else
		{
			MALABI_DB_SERVICE_ASYNC = GWT.create(MalabiDBService.class);
			return MALABI_DB_SERVICE_ASYNC;
		}
			
	}
	
	public static MalaMessages GET_MalaMessages() {
		if (MALABI_MESSAGES != null) {
			return MALABI_MESSAGES;
		}
		else
		{
			MALABI_MESSAGES = GWT.create(MalaMessages.class);
			return MALABI_MESSAGES;
		}
			
	}

}