package com.eltel.malabi.client.mappers;

import com.eltel.malabi.client.ClientFactory;
import com.eltel.malabi.client.activities.AboutActivity;
import com.eltel.malabi.client.activities.AboutPlace;
import com.eltel.malabi.client.light.LightActivity;
import com.eltel.malabi.client.light.LightPlace;
import com.eltel.malabi.client.login.LoginActivity;
import com.eltel.malabi.client.login.LoginPlace;
import com.eltel.malabi.client.mainScreen.MainScreenActivity;
import com.eltel.malabi.client.mainScreen.MainScreenPlace;
import com.eltel.malabi.client.map.MapActivity;
import com.eltel.malabi.client.map.MapPlace;
import com.eltel.malabi.client.sound.SoundActivity;
import com.eltel.malabi.client.sound.SoundPlace;
import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;

public class PhoneActivityMapper implements ActivityMapper {

	private final ClientFactory clientFactory;

	public PhoneActivityMapper(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}
	private AboutActivity aboutActivity;
	private MainScreenActivity mainScreenActivity;
	private LoginActivity loginActivity;
	private SoundActivity soundActivity;
	private LightActivity lightActivity;
	private MapActivity mapActivity;

	private AboutActivity getAboutActivity() {
		if (aboutActivity == null) {
			aboutActivity = new AboutActivity(clientFactory);
		}

		return aboutActivity;
	}
	private MainScreenActivity getMainScreenActivity() {
		if (mainScreenActivity == null) {
			mainScreenActivity = new MainScreenActivity(clientFactory);
		}

		return mainScreenActivity;
	}
	
	private LoginActivity getLoginActivity() {
		if (loginActivity == null) {
			loginActivity = new LoginActivity(clientFactory);
		}

		return loginActivity;
	}
	
	private Activity getSoundActivity() {
		if (soundActivity == null) {
			soundActivity = new SoundActivity(clientFactory);
		}
		return soundActivity;
	}
	
	private Activity getLightActivity() {
		if (lightActivity == null) {
			lightActivity = new LightActivity(clientFactory);
		}
		return lightActivity;
	}
	

	private Activity getMapActivity() {
		if (mapActivity == null) {
			mapActivity = new MapActivity(clientFactory);
		}
		return mapActivity;
	}

	@Override
	public Activity getActivity(Place place) {
		if (place instanceof MainScreenPlace) {
			return getMainScreenActivity();
		}

		if (place instanceof LoginPlace) {
			return getLoginActivity();
		}

		if (place instanceof AboutPlace) {
			return getAboutActivity();
		}
		
		if (place instanceof SoundPlace) {
			return getSoundActivity();
		}
		
		if (place instanceof LightPlace) {
			return getLightActivity();
		}
		
		if (place instanceof MapPlace) {
			return getMapActivity();
		}
		

		return getLoginActivity() ;
	}
}
