package com.eltel.malabi.client.mappers;

import com.eltel.malabi.client.ClientFactory;
import com.eltel.malabi.client.activities.AboutPlace;
import com.eltel.malabi.client.light.LightActivity;
import com.eltel.malabi.client.light.LightPlace;
import com.eltel.malabi.client.login.LoginActivity;
import com.eltel.malabi.client.login.LoginPlace;
import com.eltel.malabi.client.mainScreen.MainScreenActivity;
import com.eltel.malabi.client.mainScreen.MainScreenPlace;
import com.eltel.malabi.client.map.MapActivity;
import com.eltel.malabi.client.map.MapPlace;
import com.eltel.malabi.client.places.HomePlace;
import com.eltel.malabi.client.sound.SoundActivity;
import com.eltel.malabi.client.sound.SoundPlace;
import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;

public class TabletMiddleActivityMapper implements ActivityMapper {

	private final ClientFactory clientFactory;

	public TabletMiddleActivityMapper(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	private MainScreenActivity mainScreenActivity;
	private LoginActivity loginActivity;
	private SoundActivity soundActivity;
	private LightActivity lightActivity;
	private MapActivity mapActivity;

	private Activity getMainScreenActivity() {
		if (mainScreenActivity == null) {
			mainScreenActivity = new MainScreenActivity(clientFactory);
		}
		return mainScreenActivity;
	}

	private Activity getSoundActivity() {
		if (soundActivity == null) {
			soundActivity = new SoundActivity(clientFactory);
		}
		return soundActivity;
	}
	

	private Activity getLightActivity() {
		if (lightActivity == null) {
			lightActivity = new LightActivity(clientFactory);
		}
		return lightActivity;
	}
	

	private Activity getMapActivity() {
		if (mapActivity == null) {
			mapActivity = new MapActivity(clientFactory);
		}
		return mapActivity;
	}
	
	private Activity getLoginActicity() {
		if (loginActivity == null) {
			loginActivity = new LoginActivity(clientFactory);
		}
		return loginActivity;
	}

	@Override
	public Activity getActivity(Place place) {
		if (place instanceof HomePlace || place instanceof AboutPlace || place instanceof LoginPlace) {
//			return getLightActivity();
			//return getSoundActivity();
			//return getLoginActicity();
			return getLoginActicity();
		}
		
		if (place instanceof MainScreenPlace) {
			return getMainScreenActivity();
		}

		if (place instanceof SoundPlace) {
			return getSoundActivity();
		}
		
		if (place instanceof LightPlace) {
			return getLightActivity();
		}
		
		if (place instanceof MapPlace) {
			return getMapActivity();
		}
		
		return getLoginActicity();
	}
}
