package com.eltel.malabi.client.mappers;

import com.eltel.malabi.client.ClientFactory;
import com.eltel.malabi.client.activities.AboutActivity;
import com.eltel.malabi.client.light.LightPlace;
import com.eltel.malabi.client.login.LoginAboutActivity;
import com.eltel.malabi.client.login.LoginPlace;
import com.eltel.malabi.client.mainScreen.MainScreenPlace;
import com.eltel.malabi.client.map.MapPlace;
import com.eltel.malabi.client.places.HomePlace;
import com.eltel.malabi.client.sound.SoundPlace;
import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;

public class TabletButtomActivityMapper implements ActivityMapper {

	private final ClientFactory clientFactory;

	private Place lastPlace;

	public TabletButtomActivityMapper(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;

	}

	@Override
	public Activity getActivity(Place place) {
		Activity activity = getActivity(lastPlace, place);
		lastPlace = place;
		return activity;

	}
	
	private LoginAboutActivity loginAboutActivity;
	private AboutActivity aboutActivity;

	private AboutActivity getAboutActivity() {
		if (aboutActivity == null) {
			aboutActivity = new AboutActivity(clientFactory);
		}

		return aboutActivity;
	}

	
	private LoginAboutActivity getLoginAboutActivity() {
		if (loginAboutActivity == null) {
			loginAboutActivity = new LoginAboutActivity(clientFactory);
		}

		return loginAboutActivity;
	}

	private Activity getActivity(Place lastPlace, Place newPlace) {
		if (newPlace instanceof HomePlace || newPlace instanceof LoginPlace) {
			return getLoginAboutActivity();
		}
		
		if (newPlace instanceof MainScreenPlace) {
			return getAboutActivity();
		}

		
		if (newPlace instanceof MapPlace) {
			return getAboutActivity();
		}
		
		if (newPlace instanceof LightPlace) {
			return getAboutActivity();
		}
		
		if (newPlace instanceof SoundPlace) {
			return getAboutActivity();
		}
		

		return getLoginAboutActivity();
	}

}
