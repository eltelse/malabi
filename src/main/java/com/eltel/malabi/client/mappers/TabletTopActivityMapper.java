package com.eltel.malabi.client.mappers;

import com.eltel.malabi.client.ClientFactory;
import com.eltel.malabi.client.activities.AboutPlace;
import com.eltel.malabi.client.light.LightPlace;
import com.eltel.malabi.client.login.LoginPlace;
import com.eltel.malabi.client.mainScreen.MainScreenPlace;
import com.eltel.malabi.client.map.MapPlace;
import com.eltel.malabi.client.places.HomePlace;
import com.eltel.malabi.client.sound.SoundPlace;
import com.eltel.malabi.client.topPanel.EmptyTopPanelActivity;
import com.eltel.malabi.client.topPanel.TopLightPanelActivity;
import com.eltel.malabi.client.topPanel.TopMapPanelActivity;
import com.eltel.malabi.client.topPanel.TopPanelActivity;
import com.eltel.malabi.client.topPanel.TopSoundPanelActivity;
import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;

public class TabletTopActivityMapper implements ActivityMapper {

	private final ClientFactory clientFactory;

	public TabletTopActivityMapper(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	private TopPanelActivity topPanelActivity;
	private EmptyTopPanelActivity emptyTopPanelActivity;
	private TopSoundPanelActivity soundActivity;
	private TopLightPanelActivity lightActivity;
	private TopMapPanelActivity mapActivity;

	private Activity getTopPanelActivity() {
		if (topPanelActivity == null) {
			topPanelActivity = new TopPanelActivity(clientFactory);
		}
		return topPanelActivity;
	}
	

	private Activity getEmptyTopPanelActivity() {
		if (emptyTopPanelActivity == null) {
			emptyTopPanelActivity = new EmptyTopPanelActivity(clientFactory);
		}
		return emptyTopPanelActivity;
	}

	private Activity getTopSoundActivity() {
		if (soundActivity == null) {
			soundActivity = new TopSoundPanelActivity(clientFactory);
		}
		return soundActivity;
	}
	

	private Activity getTopLightActivity() {
		if (lightActivity == null) {
			lightActivity = new TopLightPanelActivity(clientFactory);
		}
		return lightActivity;
	}
	

	private Activity getTopMapActivity() {
		if (mapActivity == null) {
			mapActivity = new TopMapPanelActivity(clientFactory);
		}
		return mapActivity;
	}
	
	@Override
	public Activity getActivity(Place place) {
		if (place instanceof HomePlace || place instanceof AboutPlace || place instanceof LoginPlace) {
			return getEmptyTopPanelActivity();//get login
		}
		
		if (place instanceof MainScreenPlace) {
			return getTopPanelActivity();
		}
		
		if (place instanceof  SoundPlace) {
			return getTopSoundActivity();
		}
		
		if (place instanceof  LightPlace) {
			return getTopLightActivity();
		}
		
		if (place instanceof  MapPlace) {
			return getTopMapActivity();
		}
		return getEmptyTopPanelActivity();
	}
}
