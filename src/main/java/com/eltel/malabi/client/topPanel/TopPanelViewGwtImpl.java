package com.eltel.malabi.client.topPanel;

import com.eltel.malabi.client.StaticInfo;
import com.eltel.malabi.client.login.LoginPlace;
import com.eltel.malabi.client.mainScreen.MainScreenPlace;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.ui.client.widget.button.Button;
import com.googlecode.mgwt.ui.client.widget.header.HeaderPanel;
import com.googlecode.mgwt.ui.client.widget.header.HeaderTitle;

public class TopPanelViewGwtImpl implements TopPanelView {

	private HeaderPanel headerPanel;
	private Button mBackButton;
	private Button mLogOffButton;
	private Button mHamburgerButton;
	private HeaderTitle headerPanelTitle = new HeaderTitle();
	private PlaceController mPlaceController;
	private HamburgerDialogOverlay mBarsPopUp;
	
	private static String ICON_A= "<i class=\"material-icons\">";
	private static String ICON_B= "</i>";

	

	public TopPanelViewGwtImpl() {

		headerPanel = new HeaderPanel();
		headerPanel.addStyleName("malabi-top-panel");

		headerPanel.add(headerPanelTitle);

		
		mHamburgerButton= createButtonSqure("bars", "view_headline");
		mBackButton=  createButtonSqure("arrow-right","arrow_right_alt");
		mLogOffButton=  createButtonSqure("arrow-right","settings_power");
		 
		
		mHamburgerButton.addTapHandler(new TapHandler() {
			
			@Override
			public void onTap(TapEvent event) {
				StaticInfo.GET_MalabiDBServiceAsync().isUserLoggedOffServer(StaticInfo.USER_ID, new AsyncCallback<Boolean>() {
					@Override public void onFailure(Throwable res) {
						mPlaceController.goTo(new LoginPlace());
					}
					@Override public void onSuccess(Boolean res) {
						if (res) {
							mPlaceController.goTo(new LoginPlace());
						}
						else {
							if (mBarsPopUp == null)
								mBarsPopUp = new HamburgerDialogOverlay( mPlaceController);
							mBarsPopUp.show();
						}
					}
				});
			}
		});
		 
		 mLogOffButton.addTapHandler(new TapHandler() {
			 @Override
			 public void onTap(TapEvent event) {

				StaticInfo.GET_MalabiDBServiceAsync().logoffServer(StaticInfo.USER_ID, new AsyncCallback<Boolean>() {
					 @Override public void onFailure(Throwable arg0) {}
					 @Override public void onSuccess(Boolean arg0) {}
				 });
				 mPlaceController.goTo(new LoginPlace());
			 }
		 });
		 mBackButton.addTapHandler(new TapHandler() {
			 @Override
			 public void onTap(TapEvent event) {
				 mPlaceController.goTo(new MainScreenPlace());
			 }
		 });

	}
	private Button createButtonSqure(String styleName, String iconName)
	{
		Button button = new Button();
		StringBuilder stringBuilderIcon = new StringBuilder().append(ICON_A).append(iconName).append(ICON_B);
		button.getElement().setInnerHTML(stringBuilderIcon.toString());
		button.addStyleName("malabi-top-panel-button");
		button.addStyleName("malabi-top-panel-button-"+styleName);
		headerPanel.add(button);
		return button;
	}

	@Override
	public Widget asWidget() {
		return headerPanel;
	}

	@Override
	public void setTitle(String text) {
		headerPanelTitle.setText(text);
		headerPanelTitle.addStyleName("malabi-top-panel-title");
	}

	@Override
	public HasTapHandlers getBackButton() {
		return mBackButton ;
	}
	
	@Override
	public HasTapHandlers getLogOffButton() {
		return mLogOffButton;
	}
	
	@Override
	public HasTapHandlers getHamburgerButton() {
		return mHamburgerButton;
	}

	@Override
	public void setPlaceController(PlaceController placeController) {
		this.mPlaceController = placeController;		
	}
	
	@Override
	public void setBackHidden(boolean hidden) {
		mBackButton.setVisible(!hidden);;
		mLogOffButton.setVisible(hidden);
	}

}
