package com.eltel.malabi.client.topPanel.popup;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.mgwt.ui.client.util.MGWTCssResource;

public interface HamburgerDialogPanelAppearance {
   

  interface HamburgerDialogCss extends MGWTCssResource {
    @ClassName("mgwt-HamburgerDialogPanel")
    String hamburgerDialogPanel();

    @ClassName("mgwt-HamburgerDialogPanel-container")
    String container();

    @ClassName("mgwt-HamburgerDialogPanel-title")
    String title();

    @ClassName("mgwt-HamburgerDialogPanel-content")
    String content();

    @ClassName("mgwt-HamburgerDialogOverlay")
	String hamburgerDialogOverlay();

    @ClassName("mgwt-HamburgerDialogOverlay-shadow")
    String animationContainerShadow();
  }

  HamburgerDialogCss hamburgerDialogCss();
  
  UiBinder<Widget, HamburgerDialogPanel> hamburgerDialogBinder();
}
