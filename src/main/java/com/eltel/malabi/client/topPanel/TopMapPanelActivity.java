package com.eltel.malabi.client.topPanel;

import com.eltel.malabi.client.ClientFactory;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.web.bindery.event.shared.EventBus;public class TopMapPanelActivity extends TopPanelActivity {

	public TopMapPanelActivity(ClientFactory clientFactory) {
		super(clientFactory);
	}

	@Override
	public void start(AcceptsOneWidget panel, final EventBus eventBus) {
		startEx(panel, eventBus, MESSAGES.map(), false);
	}

}
