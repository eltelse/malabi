package com.eltel.malabi.client.topPanel;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;


public class TopPanelPlace extends Place {
	public static class TopPanelPlaceTokenizer implements PlaceTokenizer<TopPanelPlace> {

		@Override
		public TopPanelPlace getPlace(String token) {
			return new TopPanelPlace();
		}

		@Override
		public String getToken(TopPanelPlace place) {
			return "";
		}

	}
}
