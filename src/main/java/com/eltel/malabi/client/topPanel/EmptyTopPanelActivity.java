package com.eltel.malabi.client.topPanel;

import com.eltel.malabi.client.ClientFactory;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.web.bindery.event.shared.EventBus;
import com.googlecode.mgwt.mvp.client.MGWTAbstractActivity;public class EmptyTopPanelActivity extends MGWTAbstractActivity {

  private final ClientFactory clientFactory;

  public EmptyTopPanelActivity(ClientFactory clientFactory) {
    this.clientFactory = clientFactory;

  }

  @Override
  public void start(AcceptsOneWidget panel, final EventBus eventBus) {
    EmptyTopPanelView view = clientFactory.getEmptyTopPanelView();

    panel.setWidget(view);
  }

}
