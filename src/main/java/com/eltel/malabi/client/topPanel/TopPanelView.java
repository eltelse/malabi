package com.eltel.malabi.client.topPanel;

import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.IsWidget;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
public interface TopPanelView extends IsWidget {

	public void setTitle(String text);

	public HasTapHandlers getBackButton();
	
	public HasTapHandlers getLogOffButton();
	
	public HasTapHandlers getHamburgerButton();

	public void setPlaceController(PlaceController placeController);
	
	public void setBackHidden(boolean hidden);
}
