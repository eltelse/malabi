package com.eltel.malabi.client.topPanel;

import com.google.gwt.user.client.ui.Widget;
import com.googlecode.mgwt.ui.client.MGWT;
import com.googlecode.mgwt.ui.client.widget.header.HeaderPanel;
import com.googlecode.mgwt.ui.client.widget.header.HeaderTitle;
import com.googlecode.mgwt.ui.client.widget.panel.flex.FixedSpacer;
import com.googlecode.mgwt.ui.client.widget.panel.flex.FlexSpacer;

public class EmptyTopPanelViewGwtImp implements EmptyTopPanelView {

	private HeaderPanel headerPanel;
	private HeaderTitle headerPanelTitle = new HeaderTitle();
	

	public EmptyTopPanelViewGwtImp() {

		headerPanel = new HeaderPanel();

		headerPanel.add(new FixedSpacer());
		headerPanel.add(new FlexSpacer());
		headerPanel.add(headerPanelTitle);
		headerPanel.add(new FlexSpacer());
		headerPanel.addStyleName("malabi-empty-top-panel");
    if (MGWT.getFormFactor().isPhone()) {
    } else {
      headerPanel.add(new FixedSpacer());
    }	}	
	
	@Override
	public Widget asWidget() {
		return headerPanel;
	}
}
