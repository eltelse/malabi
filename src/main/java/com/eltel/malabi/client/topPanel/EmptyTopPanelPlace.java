package com.eltel.malabi.client.topPanel;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;


public class EmptyTopPanelPlace extends Place {
	public static class EmptyTopPanelPlaceTokenizer implements PlaceTokenizer<EmptyTopPanelPlace> {

		@Override
		public EmptyTopPanelPlace getPlace(String token) {
			return new EmptyTopPanelPlace();
		}

		@Override
		public String getToken(EmptyTopPanelPlace place) {
			return "";
		}

	}
}
