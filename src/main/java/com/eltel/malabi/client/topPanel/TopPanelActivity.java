package com.eltel.malabi.client.topPanel;

import com.eltel.malabi.client.ClientFactory;
import com.eltel.malabi.client.StaticInfo;
import com.eltel.malabi.client.messages.MalaMessages;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.web.bindery.event.shared.EventBus;
import com.googlecode.mgwt.mvp.client.MGWTAbstractActivity;public class TopPanelActivity extends MGWTAbstractActivity {

	private final ClientFactory clientFactory;
	private final TopPanelView view;
	protected MalaMessages MESSAGES = StaticInfo.GET_MalaMessages();

	public TopPanelActivity(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
		this.view = clientFactory.getTopPanelView();
	}

	@Override
	public void start(AcceptsOneWidget panel, final EventBus eventBus) {
		startEx(panel, eventBus, MESSAGES.shloam() + " "+ StaticInfo.MALABI_INFO.mMalabiUser.mName, true);
	}

	protected void startEx(AcceptsOneWidget panel, final EventBus eventBus, String header, boolean hideBackButton) {
		view.setTitle(header);
		view.setPlaceController(clientFactory.getPlaceController());
		panel.setWidget(view);
		view.setBackHidden(hideBackButton);
	}

}
