package com.eltel.malabi.client.topPanel.popup;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiFactory;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;

public class HamburgerDialogPanel extends Composite {

	public static final HamburgerDialogPanelDefaultAppearance DEFAULT_APPEARANCE = GWT
			.create(HamburgerDialogPanelDefaultAppearance.class);

	private static String BARS_DIALOG = "malabi-bars-dialog";
	private static String BARS_DIALOG_CONTENT = "malabi-bars-dialog-content";
	private static String BARS_DIALOG_TITLE = "malabi-bars-dialog-title";

	@UiField
	public Label title;
	@UiField
	public FlowPanel content;

	private HamburgerDialogPanelAppearance appearance;

	/**
	 * Construct panel with a special css
	 *
	 * @param css the css to use
	 */
	public HamburgerDialogPanel(HamburgerDialogPanelAppearance appearance) {
		this.appearance = appearance;
		initWidget(this.appearance.hamburgerDialogBinder().createAndBindUi(this));
		this.addStyleName(BARS_DIALOG);
		content.addStyleName(BARS_DIALOG_CONTENT);
		title.addStyleName(BARS_DIALOG_TITLE);
	}


	/**
	 * get the container of the panel
	 *
	 * @return the container of the dialog panel
	 */
	public FlowPanel getContent() {
		return content;
	}

	/**
	 * Get the title of the dialog
	 *
	 * @return the title of the dialog
	 */
	public Label getDialogTitle() {
		return title;
	}

	@UiFactory
	public HamburgerDialogPanelAppearance getAppearance() {
		return appearance;
	}
}
