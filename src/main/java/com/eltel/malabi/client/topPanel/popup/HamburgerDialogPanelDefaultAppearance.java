package com.eltel.malabi.client.topPanel.popup;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Widget;

public class HamburgerDialogPanelDefaultAppearance  implements HamburgerDialogPanelAppearance
{

	  static {
	    Resources.INSTANCE.cssHamburgerPanel().ensureInjected();
	  }

	  interface Resources extends ClientBundle {

	  Resources INSTANCE = GWT.create(Resources.class);

	  @Source({"hamburgerdialog.css"})
		HamburgerDialogCss cssHamburgerPanel();
	  }


	@UiTemplate("HamburgerDialogPanelAbstractAppearance.ui.xml")
	interface DialogBinder extends UiBinder<Widget, HamburgerDialogPanel> {
	}

	private static final DialogBinder PUP_UP_BINDER = GWT.create(DialogBinder.class);

	@Override
	public UiBinder<Widget, HamburgerDialogPanel> hamburgerDialogBinder() {
		return PUP_UP_BINDER;
	}


	@Override
	public HamburgerDialogCss hamburgerDialogCss() {
		return Resources.INSTANCE.cssHamburgerPanel();
	}
}
