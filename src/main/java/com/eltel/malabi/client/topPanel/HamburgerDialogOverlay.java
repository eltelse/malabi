package com.eltel.malabi.client.topPanel;

import java.util.ArrayList;
import java.util.Iterator;

import com.eltel.malabi.client.StaticInfo;
import com.eltel.malabi.client.light.LightPlace;
import com.eltel.malabi.client.login.LoginPlace;
import com.eltel.malabi.client.map.MapPlace;
import com.eltel.malabi.client.messages.MalaMessages;
import com.eltel.malabi.client.sound.SoundPlace;
import com.eltel.malabi.client.topPanel.popup.HamburgerDialogPanel;
import com.eltel.malabi.client.topPanel.popup.HamburgerDialogPanelAppearance;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.EventTarget;
import com.google.gwt.dom.client.Node;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.TouchCancelEvent;
import com.google.gwt.event.dom.client.TouchCancelHandler;
import com.google.gwt.event.dom.client.TouchEndEvent;
import com.google.gwt.event.dom.client.TouchEndHandler;
import com.google.gwt.event.dom.client.TouchMoveEvent;
import com.google.gwt.event.dom.client.TouchMoveHandler;
import com.google.gwt.event.dom.client.TouchStartEvent;
import com.google.gwt.event.dom.client.TouchStartHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.mgwt.dom.client.event.mouse.HandlerRegistrationCollection;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.dom.client.event.touch.HasTouchHandlers;
import com.googlecode.mgwt.dom.client.event.touch.TouchHandler;
import com.googlecode.mgwt.ui.client.util.MGWTUtil;
import com.googlecode.mgwt.ui.client.widget.animation.Animation;
import com.googlecode.mgwt.ui.client.widget.animation.AnimationEndCallback;
import com.googlecode.mgwt.ui.client.widget.animation.AnimationWidget;
import com.googlecode.mgwt.ui.client.widget.animation.Animations;
import com.googlecode.mgwt.ui.client.widget.button.Button;
import com.googlecode.mgwt.ui.client.widget.dialog.Dialog;
import com.googlecode.mgwt.ui.client.widget.dialog.HasTitleText;
import com.googlecode.mgwt.ui.client.widget.panel.flex.FlexPropertyHelper.Alignment;
import com.googlecode.mgwt.ui.client.widget.panel.flex.FlexPropertyHelper.Justification;
import com.googlecode.mgwt.ui.client.widget.panel.flex.RootFlexPanel;
import com.googlecode.mgwt.ui.client.widget.touch.TouchDelegate;


public class HamburgerDialogOverlay implements HasWidgets, HasTouchHandlers, HasTapHandlers,HasTitleText, Dialog {

	private HamburgerDialogPanel mBarDialogPanel;
	ArrayList<Button> mButtonList = new ArrayList<Button>();
	private  MalaMessages  MESSAGES = StaticInfo.GET_MalaMessages();
	private final PlaceController mPlaceController;

	private static String BARS_DIALOG_FLOW_PANEL = "malabi-bars-dialog-flow-panel";
	private static String BARS_DIALOG_ICON_BUTTON = "malabi-bars-dialog-icon-button";
	private static String BARS_DIALOG_LABEL_BUTTON = "malabi-bars-dialog-label-button";

	private static String ICON_A= "<i class=\"material-icons\">";
	private static String ICON_B= "</i>";

	/**
	 * @param callback - the callback used when a button of the dialog is taped
	 */
	public HamburgerDialogOverlay(PlaceController placeController) {
		this.mPlaceController = placeController;
		HamburgerDialogPanelAppearance appearance =HamburgerDialogPanel.DEFAULT_APPEARANCE;
		this.appearance = appearance;
		display = new AnimationWidget();
		display.addStyleName(appearance.hamburgerDialogCss().hamburgerDialogOverlay());
		touchDelegateForDisplay = new TouchDelegate(display);
		display.addStyleName(appearance.hamburgerDialogCss().animationContainerShadow());

		container = new RootFlexPanel();    addTouchHandler(new InternalTouchHandler(container.getElement()));
		mBarDialogPanel = new HamburgerDialogPanel(appearance);
		addTapHandler(new TapHandler() {
			@Override
			public void onTap(final TapEvent event) {
				Element targetObject = event.getTargetElement();
				if ( targetObject.equals(mBarDialogPanel.getDialogTitle().getElement())
						) {
					return;
				}
				for (FlowPanel flowPanel : flowPanels) {				
					if  (targetObject.equals(flowPanel.getElement())) {
						return;
					}
				}
				hide();
			}
		});
		add(mBarDialogPanel);

		//    for (String audio : mHamburgerList)
		//    	addRadio(audio);
		addButton(MESSAGES.sound(), new SoundPlace(), false, "volume_up");
		addButton(MESSAGES.light(), new LightPlace(), false, "highlight");
		addButton(MESSAGES.map(), new MapPlace(), false,"map");
		// addButton(MESSAGES.help(), null, false,"question");
		//  addButton(MESSAGES.about(), null, false, "info");
		addButton(MESSAGES.logoff(), new LoginPlace(), true, "settings_power");//"📴");
		setTitleText("maLabi");

	}

	ArrayList<FlowPanel> flowPanels = new ArrayList<FlowPanel>();
	private void addButton(String text,Place place, boolean logoff, String iconName)
	{

		FlowPanel flowPanel = new FlowPanel();
		flowPanels.add(flowPanel);
		Button button = new Button();
		Label label = new Label(text);


		flowPanel.addStyleName(BARS_DIALOG_FLOW_PANEL);
		flowPanel.addStyleName(BARS_DIALOG_FLOW_PANEL+"-"+iconName);
		button.addStyleName(BARS_DIALOG_ICON_BUTTON);
		label.addStyleName(BARS_DIALOG_LABEL_BUTTON);
		label.addClickHandler(createLogOffClickHandler(place, logoff));


		StringBuilder stringBuilderIcon = new StringBuilder().append(ICON_A).append(iconName).append(ICON_B);
		button.getElement().setInnerHTML(stringBuilderIcon.toString());
		if (place != null)
			button.addTapHandler(createLogOffTapHandler(place, logoff));
		// button.addTapHandler(handler);

		flowPanel.add(button);
		flowPanel.add(label);

		mBarDialogPanel.getContent().add(flowPanel);
		mButtonList.add(button);
	}

	@Override
	public void setTitleText(String title) {
		mBarDialogPanel.getDialogTitle().setText(title);
	}

	private ClickHandler createLogOffClickHandler(final Place place,final boolean logoff)
	{
		return new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				doLogOff(place, logoff);
			}
		};
	}
	
	private TapHandler createLogOffTapHandler(final Place place,final boolean logoff)
	{
		return new TapHandler() {
			@Override
			public void onTap(TapEvent event) {
				doLogOff(place, logoff);
			}
		};
	}

	private void doLogOff(final Place place,final boolean logoff){
		if (logoff) {
			//	StaticInfo.MALABI_INFO.InitAll();
			StaticInfo.GET_MalabiDBServiceAsync().logoffServer(StaticInfo.USER_ID, new AsyncCallback<Boolean>() {
				@Override	public void onSuccess(Boolean arg0) {}
				@Override	public void onFailure(Throwable arg0) {}
			});
		}
		mPlaceController.goTo(place);
		hide();
	}

	@Override
	public String getTitleText() {
		return mBarDialogPanel.getDialogTitle().getText();
	}


	public void show() {
		//	for (String audio : mHamburgerList)
		//		addRadio(audio);
		center();
	}


	private class HideAnimationEndCallback implements AnimationEndCallback {
		public void onAnimationEnd() {
			HasWidgets panel = getPanelToOverlay();
			panel.remove(display.asWidget());
			// see issue 247 => http://code.google.com/p/mgwt/issues/detail?id=247
			MGWTUtil.forceFullRepaint();

			transitionState = TransitionState.NOTVISIBLE;
			if (requestShow) {
				requestShow = false;
				Scheduler.get().scheduleDeferred(new ScheduledCommand() {
					@Override
					public void execute() {
						show();
					}
				});
			}
		}
	}

	private class ShowAnimationEndCallback implements AnimationEndCallback {
		public void onAnimationEnd() {
			transitionState = TransitionState.VISIBLE;
			if (requestHide) {
				requestHide = false;
				Scheduler.get().scheduleDeferred(new ScheduledCommand() {
					@Override
					public void execute() {
						hide();
					}
				});
			}
		}
	}

	private class InternalTouchHandler implements TouchHandler {
		private final Element shadow;
		private Element startTarget;

		private InternalTouchHandler(Element shadow) {
			this.shadow = shadow;
		}

		@Override
		public void onTouchCancel(TouchCancelEvent event) {
			startTarget = null;
		}

		@Override
		public void onTouchEnd(TouchEndEvent event) {
			EventTarget eventTarget = event.getNativeEvent().getEventTarget();
			if (eventTarget != null) {
				// no textnode or element node
				if (Node.is(eventTarget)) {
					if (Element.is(eventTarget)) {
						Element endTarget = eventTarget.cast();

						if (endTarget == shadow && startTarget == shadow) {
							maybeHide();
						}

					}
				}
			}
			startTarget = null;
		}

		@Override
		public void onTouchMove(TouchMoveEvent event) {
		}

		@Override
		public void onTouchStart(TouchStartEvent event) {
			EventTarget eventTarget = event.getNativeEvent().getEventTarget();
			if (eventTarget != null) {
				// no textnode or element node
				if (Node.is(eventTarget)) {
					if (Element.is(eventTarget)) {
						startTarget = eventTarget.cast();
					}
				}
			}
		}
	}

	private final AnimationEndCallback SHOW_ANIMATION_CALLBACK = new ShowAnimationEndCallback();
	private final AnimationEndCallback HIDE_ANIMATION_CALLBACK = new HideAnimationEndCallback();

	protected final HamburgerDialogPanelAppearance appearance;

	private RootFlexPanel container;
	private HasWidgets panelToOverlay;
	private AnimationWidget display;

	private boolean centerChildren;
	private boolean autoHide;
	private TouchDelegate touchDelegateForDisplay;
	private enum TransitionState {
		HIDING, SHOWING, VISIBLE, NOTVISIBLE;
	}
	private TransitionState transitionState = TransitionState.NOTVISIBLE;
	private boolean requestHide = false;
	private boolean requestShow = false;

	//	  public HamburgerDialogOverlay(HamburgerDialogPanelAppearance appearance) {
	//	    this.appearance = appearance;
	//	    display = new AnimationWidget();
	//	    display.addStyleName(appearance.overlayCss().hamburgerDialogOverlay());
	//	    touchDelegateForDisplay = new TouchDelegate(display);
	//	    display.addStyleName(appearance.overlayCss().animationContainerShadow());
	//
	//	    container = new RootFlexPanel();    addTouchHandler(new InternalTouchHandler(container.getElement()));
	//
	//	  }

	@Override
	public void add(Widget w) {
		container.add(w);
	}

	@Override
	public HandlerRegistration addTouchStartHandler(TouchStartHandler handler) {
		return touchDelegateForDisplay.addTouchStartHandler(handler);
	}

	@Override
	public HandlerRegistration addTouchMoveHandler(TouchMoveHandler handler) {
		return touchDelegateForDisplay.addTouchMoveHandler(handler);
	}

	@Override
	public HandlerRegistration addTouchCancelHandler(TouchCancelHandler handler) {
		return touchDelegateForDisplay.addTouchCancelHandler(handler);
	}

	@Override
	public HandlerRegistration addTouchEndHandler(TouchEndHandler handler) {
		return touchDelegateForDisplay.addTouchEndHandler(handler);
	}

	@Override
	public HandlerRegistration addTouchHandler(TouchHandler handler) {
		HandlerRegistrationCollection handlerRegistrationCollection =
				new HandlerRegistrationCollection();
		handlerRegistrationCollection.addHandlerRegistration(addTouchCancelHandler(handler));
		handlerRegistrationCollection.addHandlerRegistration(addTouchStartHandler(handler));
		handlerRegistrationCollection.addHandlerRegistration(addTouchEndHandler(handler));
		handlerRegistrationCollection.addHandlerRegistration(addTouchMoveHandler(handler));
		return handlerRegistrationCollection;
	}

	public HandlerRegistration addTapHandler(TapHandler handler) {
		return touchDelegateForDisplay.addTapHandler(handler);
	}

	/**
	 * Show the dialog centered
	 */
	public void center() {
		centerChildren = true;
		showEx();
	}

	@Override
	public void clear() {
		container.clear();
	}

	/**
	 * get the panel that the dialog overlays
	 *
	 * @return the panel that is overlayed by this dialog
	 */
	public HasWidgets getPanelToOverlay() {
		if (panelToOverlay == null) {
			panelToOverlay = RootPanel.get();
		}
		return panelToOverlay;
	}

	/**
	 * hide the dialog if it is visible
	 */
	public void hide() {
		if (transitionState == TransitionState.SHOWING) {
			// not finished showing yet so request to hide once show complete
			requestHide = true;
		}
		else if (transitionState == TransitionState.VISIBLE) {
			requestHide = false;
			transitionState = TransitionState.HIDING;
			display.animate(getHideAnimation(), false, HIDE_ANIMATION_CALLBACK);
		}
		else if (transitionState == TransitionState.HIDING) {
			// not finished hiding yet so remove requestShow if set
			requestShow = false;
		}
	}

	/**
	 * Should the dialog hide itself if there is a tap outside the dialog
	 *
	 * @return true if the dialog automatically hides, otherwise false
	 */
	public boolean isHideOnBackgroundClick() {
		return autoHide;
	}

	@Override
	public Iterator<Widget> iterator() {
		return container.iterator();
	}

	@Override
	public boolean remove(Widget w) {
		return container.remove(w);
	}

	/**
	 * Should the content of the dialog be centered
	 *
	 * @param centerContent true to center content
	 */
	public void setCenterContent(boolean centerContent) {
		this.centerChildren = centerContent;
	}

	/**
	 * Should the dialog hide itself if there is a tap outside the dialog
	 *
	 * @param hideOnBackgroundClick true if the dialog automatically hides, otherwise false
	 */
	public void setHideOnBackgroundClick(boolean hideOnBackgroundClick) {
		this.autoHide = hideOnBackgroundClick;
	}

	/**
	 * set the panel that should be overlayed by the dialog
	 *
	 * @param panel the area to be overlayed
	 */
	public void setPanelToOverlay(HasWidgets panel) {
		this.panelToOverlay = panel;
	}

	/**
	 * should the dialog add a shadow over the area that it covers
	 *
	 * @param shadow true to add a shadow
	 */
	public void setShadow(boolean shadow) {
		if (shadow) {
			display.asWidget().addStyleName(appearance.hamburgerDialogCss().animationContainerShadow());
		} else {
			display.asWidget().removeStyleName(appearance.hamburgerDialogCss().animationContainerShadow());
		}
	}

	public void showEx() {
		if (transitionState == TransitionState.HIDING) {
			// not finished hiding yet so request to show once hide complete
			requestShow = true;
		}
		else if (transitionState == TransitionState.NOTVISIBLE) {
			requestShow = false;
			transitionState = TransitionState.SHOWING;
			// add overlay to DOM
			HasWidgets panel = getPanelToOverlay();
			panel.add(display.asWidget());

			if (centerChildren) {
				container.setAlignment(Alignment.CENTER);
				container.setJustification(Justification.CENTER);
			} else {
				container.clearAlignment();
				container.clearJustification();
			}

			display.setFirstWidget(container);

			// and animiate
			display.animate(getShowAnimation(), true, SHOW_ANIMATION_CALLBACK);
		}
		else if (transitionState == TransitionState.SHOWING) {
			// not finished showing yet so remove requestHide if set
			requestHide = false;
		}
	}


	protected Animation getShowAnimation() {
		return Animations.POP;
	}


	protected Animation getHideAnimation() {
		return Animations.POP_REVERSE;
	}

	protected void maybeHide() {
		if (autoHide) {
			hide();
		}
	}

	@Override
	public void fireEvent(GwtEvent<?> event) {
		display.fireEvent(event);
	}
}
