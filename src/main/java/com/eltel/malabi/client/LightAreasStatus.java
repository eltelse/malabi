package com.eltel.malabi.client;

import java.io.Serializable;
import java.util.ArrayList;

public class LightAreasStatus implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1542425687896743644L;
	
	public ArrayList<Boolean> mAreasState;
	
	public int mReturnState = -1;
	
	public int mQuarterIndex;
	
	public LightAreasStatus() {
		mAreasState = new ArrayList<Boolean>();
		mReturnState = -1;
		mQuarterIndex = -1;
	}
	
	public LightAreasStatus(int numOfPoles, int quarterIndex) {
		mAreasState = new ArrayList<Boolean>(numOfPoles);
		mQuarterIndex = quarterIndex;
	}
	

}
