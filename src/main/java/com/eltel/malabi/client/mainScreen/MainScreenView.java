package com.eltel.malabi.client.mainScreen;

import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.IsWidget;
public interface MainScreenView extends IsWidget {

	void setPlaceController(PlaceController placeController);
	
//	public void setInfoMessage(String msg);
	
}
