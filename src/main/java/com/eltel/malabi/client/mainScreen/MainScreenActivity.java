package com.eltel.malabi.client.mainScreen;

import com.eltel.malabi.client.ClientFactory;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.web.bindery.event.shared.EventBus;
import com.googlecode.mgwt.mvp.client.MGWTAbstractActivity;
public class MainScreenActivity extends MGWTAbstractActivity {

  private final ClientFactory clientFactory;
  private final MainScreenView view;
  
  public MainScreenActivity(ClientFactory clientFactory) {
    this.clientFactory = clientFactory;
    this.view = clientFactory.getMainScreenView();
  }

  @Override
  public void start(AcceptsOneWidget panel, final EventBus eventBus) {
    view.setPlaceController(clientFactory.getPlaceController());
    panel.setWidget(view);
  }

}
