package com.eltel.malabi.client.mainScreen;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;


public class MainScreenPlace extends Place {
	public static class MainScreenPlaceTokenizer implements PlaceTokenizer<MainScreenPlace> {

		@Override
		public MainScreenPlace getPlace(String token) {
			return new MainScreenPlace();
		}

		@Override
		public String getToken(MainScreenPlace place) {
			return "";
		}

	}
}
