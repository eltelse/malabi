package com.eltel.malabi.client.mainScreen;

import com.eltel.malabi.client.StaticInfo;
import com.eltel.malabi.client.light.LightPlace;
import com.eltel.malabi.client.login.LoginPlace;
import com.eltel.malabi.client.map.MapPlace;
import com.eltel.malabi.client.messages.MalaMessages;
import com.eltel.malabi.client.sound.SoundPlace;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.ui.client.widget.button.Button;
import com.googlecode.mgwt.ui.client.widget.panel.Panel;
import com.googlecode.mgwt.ui.client.widget.panel.flex.RootFlexPanel;


public class MainScreenViewGwtImpl implements MainScreenView {

	private static String MAIN_SCREEN_INFO_PANEL = "malabi-main-screen-info-panel";
	private static String MAIN_SCREEN_MSG = "malabi-main-screen-info-msg";
	private static String HIDDEN = "hidden";

	private RootFlexPanel main;
	private MalaMessages messages = StaticInfo.GET_MalaMessages();
	private static String ICON_A= "<i class=\"material-icons\">";
	private static String ICON_B= "</i>";
	private PlaceController mPlaceController;

	private HTML mInfoMessage;
	private Panel mInfoMessagePanel;

	private boolean mIsLoginLocked = false;

	public MainScreenViewGwtImpl() {
		main = new RootFlexPanel();
		main.addStyleName("malabi-main");

		//Button
		//	scrollPanel.getElement().getStyle().setProperty("background-size", "cover");

		createButtonRound("volume-up", "volume_up" , messages.sound(), main, new SoundPlace());
		createButtonRound("lightbulb", "highlight", messages.light(), main, new LightPlace());
		createButtonRound("map-marked-alt", "map", messages.map(), main, new MapPlace());
		mInfoMessage=  new HTML("info");
		mInfoMessage.addStyleName(MAIN_SCREEN_MSG);
		//
		mInfoMessagePanel = new Panel();
		mInfoMessagePanel.addStyleName(MAIN_SCREEN_INFO_PANEL);
		//		
		mInfoMessagePanel.add(mInfoMessage);
		mInfoMessagePanel.addStyleName(HIDDEN);
		main.add(mInfoMessagePanel);
		//		String errorMsg = StaticInfo.MALABI_INFO.mErrorMsg.isEmpty() ? "" : messages.nonActiveQuarters(StaticInfo.MALABI_INFO.mErrorMsg);
		//		setInfoMessage(errorMsg);

	}


	private Label createLabel(String className, String labelName, RootFlexPanel flowPanel)
	{
		Label label = new Label(labelName);
		label.addStyleName("malabi-main-label");
		label.addStyleName("malabi-main-label-"+className);
		flowPanel.add(label);

		return label;
	}



	private Button createButtonRound( String name,  String iconName, String label, RootFlexPanel flowPanel, final Place place)
	{
		final Button button = new Button("");
		StringBuilder stringBuilderIcon = new StringBuilder().append(ICON_A).append(iconName).append(ICON_B);
		createButton(button, name, stringBuilderIcon.toString(), flowPanel);
		createLabel(name, label,flowPanel);
		button.addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
				if (getLockLoginState())
					return;
				setLockLoginState(true);
				setInfoMessage(messages.pleasewait());
				StaticInfo.GET_MalabiDBServiceAsync().isUserLoggedOffServer(StaticInfo.USER_ID, new AsyncCallback<Boolean>() {
					@Override public void onFailure(Throwable res) {
					//	mPlaceController.goTo(new LoginPlace());
						setInfoMessage(messages.actionFail());
						setLockLoginState(false);
					}
					@Override public void onSuccess(Boolean userIsoffline) {
						if (userIsoffline) {
							mPlaceController.goTo(new LoginPlace());
						}
						else {
							mPlaceController.goTo(place);
						}
						setInfoMessage("");
						setLockLoginState(false);
					}
				});
				//	 
			}

		});
		return button;
	}

	private Button createButton(Button button, String name, String iconName, RootFlexPanel flowPanel)
	{
		button.setImportant(true);

		button.addStyleName("malabi-main-button");
		button.addStyleName("malabi-main-button-"+name);
		// if (name.contains("#"))
		button.getElement().setInnerHTML(iconName);
		// else
		// 	button.getElement().setInnerHTML("<i class=\"fas fa-"+iconName+"\";></i>");

		flowPanel.add(button);
		return button;
	}

	@Override
	public Widget asWidget() {
		return main;
	}

	@Override
	public void setPlaceController(PlaceController placeController) {
		this.mPlaceController = placeController;		
	}

	//	public void setInfoMessagePanelHidden(final boolean isHide) {
	//		mInfoMessagePanel.setStyleName(HIDDEN, isHide);
	//	}

	public void setInfoMessage(String msg) {
		mInfoMessagePanel.setStyleName(HIDDEN, msg.isEmpty());
		mInfoMessage.setText(msg);
	}

	private boolean getLockLoginState() {
		return mIsLoginLocked;
	}

	private void setLockLoginState(boolean state) {
		mIsLoginLocked = state;
	}


}
