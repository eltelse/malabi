package com.eltel.malabi.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * 
 */
public interface MalabiDBServiceAsync {
  void loginServer(String userName, String password, AsyncCallback<MalabiInfo> callback)
      throws IllegalArgumentException;
  void logoffServer(int userID, AsyncCallback<Boolean> callback)
    	      throws IllegalArgumentException;
  void changeAreaServer(int userID, Quarter quarter, LightArea lightPole, Boolean state, AsyncCallback<Integer> callback)
	      throws IllegalArgumentException;
  void changeQuarterServer(int userID, Quarter quarter, Boolean state, AsyncCallback<Integer> callback)
	      throws IllegalArgumentException;
  void playAudioServer(int userID,SoundInfo soundInfo, ArrayList<SoundPole> polesListToPlay, AsyncCallback<Integer> callback)
	      throws IllegalArgumentException;
  void stopAudioServer(int userID, ArrayList<SoundPole> polesListToStop ,AsyncCallback<Integer> callback)
	      throws IllegalArgumentException;
  void getAreasInQuarterState(int numOfPoles, int quarterNum, AsyncCallback<LightAreasStatus> callback)
		  throws IllegalArgumentException;
  void isUserLoggedOffServer(int userID, AsyncCallback<Boolean> callback)
	      throws IllegalArgumentException;
  void getSoundInfoList (AsyncCallback<ArrayList<SoundInfo>> classback)
		  throws IllegalArgumentException;
  void getPoleState(int poleNum, AsyncCallback<Boolean> callback)
		  throws IllegalArgumentException;
}

