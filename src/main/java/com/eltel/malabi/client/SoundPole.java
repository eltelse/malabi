package com.eltel.malabi.client;

import java.io.Serializable;

public class SoundPole implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -339799642697673266L;

	public int mID;
	
	public String mName;
	
	public int mQuarterId;
	
	public String mLocation;
	
	public SoundPole ()
	{
		mID = -1;
	}

	/**
	 * 
	 * @param id
	 * @param name
	 * @param isInUse
	 * @param quarterID
	 * @param adamCode
	 * @param location
	 */
	public SoundPole (int id, String name, int quarterID, String location) {
		mID = id;
		mName =name;
		mQuarterId = quarterID;
		mLocation = location;
	}
	
	@Override
	public String toString() {
		return "Pole ID: "+mID+"; mName: "+ mName+";  Quarter: "+mQuarterId;
	}
	
	public int getQuarter() {
		return mQuarterId;
	}
}
