package com.eltel.malabi.client;
import java.io.Serializable;


public class MalabiUser implements Serializable    {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8339128572568906863L;

	public int mID;
	
	public String mName;
	
	public boolean mIsActive;
	
	public boolean mIsValidDate;
	
	public boolean mIsValidUser;
	
	public boolean mIsFirstUser;
	
	/**
	 * Create default Malabi User
	 */
	public MalabiUser ()
	{
		mID = -1;
		mName = "";
		mIsActive = false;
		mIsValidDate = false;
		mIsValidUser = false;
		mIsFirstUser = false;
	}
	

	/**
	 * Create User with ID and Name
	 * @param id
	 * @param name
	 */
	public MalabiUser (int id, String name, boolean isActive, boolean isValidDate, boolean isAlreadyLogged) {
		mID = isActive && isValidDate ? id : -1;
		mName =name;
		mIsActive = isActive;
		mIsValidDate = isValidDate;
		mIsValidUser = true;
		mIsFirstUser = !isAlreadyLogged;
	}
	
	public boolean isShouldGetMoreUserInfo() {
		return mIsValidUser && mIsActive && mIsValidDate && mIsFirstUser;
	}
	
	@Override
	public String toString() {
		return " mID: "+mID+";  name: "+mName+";  isActive: "+mIsActive+";  isValidDate: "+mIsValidDate
				+";  mIsFirstUser: "+mIsFirstUser;
	}
	

}
