package com.eltel.malabi.client;
import java.io.Serializable;


public class LightArea implements Serializable    {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8339128572568906863L;

	public String mName;
	
	public boolean mState;
	
	public int mQuarterId;
	
	public int mAreaIndex;
	
	public String mLocation;
	
	public LightArea ()
	{
		mAreaIndex = -1;
	}

	/**
	 * 
	 * @param id
	 * @param name
	 * @param isInUse
	 * @param quarterID
	 * @param adamCode
	 * @param location
	 */
	public LightArea (String name, int quarterID, int areaIndex) {
//		mID = id;
		mName =name;
		mQuarterId = quarterID;
		mAreaIndex = areaIndex;
	//	mLocation = location;
	}
	
	@Override
	public String toString() {
		return " mAdamCode: "+mAreaIndex+";  name: "+mName+";  mIsInUse: "+mState;
	}
	
	public int getQuarter() {
		return mQuarterId;
	}
	
	public void setState(boolean state) {
		mState = state;
	}
}
