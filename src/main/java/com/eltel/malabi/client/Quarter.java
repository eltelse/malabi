package com.eltel.malabi.client;

import java.io.Serializable;
import java.util.ArrayList;


public class Quarter implements Serializable    {


	/**
	 * "2.55.96.114" 8100
	 */
	private static final long serialVersionUID = -8714046668709509009L;
	
	public int mID;
	
	public int mNumOfAreasInQuarter;
	
	public ArrayList<LightArea> mLightAreas;
	
	public int mQuarterNum;
	
	public Quarter ()
	{
		mID = -1;
	
	}
	public Quarter (int focal)
	{
		mID = focal;
	
	}
	public Quarter (int id, int numOfAreasInQuarter, int quarterNum)
	{
		mID = id;
		mNumOfAreasInQuarter = numOfAreasInQuarter;
		mLightAreas = new ArrayList<LightArea>();
		mQuarterNum = quarterNum;
	}
	
	public ArrayList<String> getSubAreas()
	{
		
		ArrayList<String> list = new ArrayList<String>(mLightAreas.size()) ;
		for (LightArea lightPole : mLightAreas) { 
			list.add(String.valueOf(lightPole)); 
		}
		return list;
	}
	
	public void addLightArea(LightArea lightArea) {
		
		mLightAreas.add(lightArea);
	}
	
	@Override
	public String toString() {
		return getInfo()+ getSubAreas();
	}
	
	
	public String getInfo() {
		return "Quarter "+mID+"; NumOfPoles: "+mNumOfAreasInQuarter+";";
	}
}
