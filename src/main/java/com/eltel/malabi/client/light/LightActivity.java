package com.eltel.malabi.client.light;

import com.eltel.malabi.client.ClientFactory;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.web.bindery.event.shared.EventBus;
import com.googlecode.mgwt.mvp.client.MGWTAbstractActivity;

public class LightActivity extends MGWTAbstractActivity {

  private final LightView view;
  public LightActivity(ClientFactory clientFactory) {
	  view = clientFactory.getLightView();
  }

  @Override
  public void start(AcceptsOneWidget panel,  final EventBus eventBus) {
	  view.setControler(this);
	  view.UpdateLightControlList();
	  panel.setWidget(view);
  }

  @Override
  public void onStop() {
	  super.onStop();
	  view.clearViewObjects();
  }

  @Override
  public void addHandlerRegistration(HandlerRegistration handlerRegistration) {
	  super.addHandlerRegistration(handlerRegistration);
  }

  @Override
  public void addHandlerRegistration(com.google.web.bindery.event.shared.HandlerRegistration handlerRegistration) {
	  super.addHandlerRegistration(handlerRegistration);
  }

}
