package com.eltel.malabi.client.light;

import com.google.gwt.user.client.ui.IsWidget;
public interface LightView extends IsWidget {

	void UpdateLightControlList();

	void clearViewObjects();
	
	void setControler(LightActivity lightActivity);

}
