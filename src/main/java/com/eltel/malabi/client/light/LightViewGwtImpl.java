package com.eltel.malabi.client.light;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.eltel.malabi.client.FlowPanelEx;
import com.eltel.malabi.client.LightArea;
import com.eltel.malabi.client.LightAreasStatus;
import com.eltel.malabi.client.Quarter;
import com.eltel.malabi.client.StaticInfo;
import com.eltel.malabi.client.messages.MalaMessages;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.ui.client.widget.button.Button;
import com.googlecode.mgwt.ui.client.widget.input.checkbox.MCheckBox;
import com.googlecode.mgwt.ui.client.widget.panel.Panel;
import com.googlecode.mgwt.ui.client.widget.panel.flex.FlexSpacer;
import com.googlecode.mgwt.ui.client.widget.panel.flex.RootFlexPanel;
import com.googlecode.mgwt.ui.client.widget.panel.scroll.ScrollPanel;
public class LightViewGwtImpl implements LightView {

	private RootFlexPanel main;
	private ScrollPanel mainScrollPanel;
	private Panel mainPanel;
	private ArrayList<MCheckBox> mQuarterCheckBoxList = new ArrayList<MCheckBox>();
	
	private static String MAIN_SCREEN_INFO_PANEL = "malabi-main-screen-info-panel";
	private static String MAIN_SCREEN_MSG = "malabi-main-screen-info-msg";

	private HashMap<Integer,HTML> mInfoMessageList = new HashMap<Integer,HTML>();
	ArrayList<FlowPanel> mQuarterFlexPanelList = new ArrayList<FlowPanel>();
//	private ArrayList<Panel> mInfoMessagePanelList = new ArrayList<Panel>();
	
	private HashMap<Integer,ArrayList<MCheckBox>> mSubAreas = new HashMap<Integer, ArrayList<MCheckBox>>();
	private MalaMessages MESSAGES = StaticInfo.GET_MalaMessages();
	
//	private ValueChangeHandler<Boolean> mAreaStatusChanged;
	
	private static String QUARTER_PANEL= "malabi-light-quarter-panel";
	private static String LIGHT_SCROLL_PANEL = "malabi-light-scroll-panel";
	private static String QUARTER_TITLE_PANEL = "malabi-light-quarter-title-panel";
	private static String QUARTER_EXPAND_ICON = "malabi-light-quarter-expand-icon";
	private static String HIDDEN_TAG = "hidden";
	private static String QUARTER_TITLE = "malabi-light-quarter-title";
	private static String QUARTER_ALL_PANEL = "malabi-light-quarter-all-panel";
	private static String QUARTER_INDEX_AREA_PANEL = "malabi-light-quarter-index-area-panel";
	private static String INDEX = "index";
	private static String QUARTER_PANEL_AREA_CHECKBOX = "malabi-light-quarter-panel-area-checkbox";
	private static String QUARTER_AREA_PANEL = "malabi-light-quarter-area-panel";
	private static String QUARTER_PANEL_ALL_CHECKBOX = "malabi-light-quarter-panel-all-checkbox";
	private static String QUARTER_PANEL_ALL_LABEL ="malabi-light-quarter-panel-all-label";
	private static String QUARTER_PANEL_SUBAREA_PANEL = "malabi-light-quarter-panel-subarea-panel";
	private static String QUARTER_PANEL_SUBAREA_LABEL = "malabi-light-quarter-panel-subarea-label";
	
//	private static String QUARTER_PANEL_SUBAREA_INFO_PANEL = "malabi-light-quarter-panel-info-panel";
	private static String HIDDEN = "hidden";
	private static String QUARTER_PANEL_SUBAREA_INFO_MSG = "malabi-light-quarter-panel-info-msg";
	
	private static final String MINUS = "-";
	private static final String PLUS = "+";
	
	private LightActivity mLightActivity;
	
	private final HTML mInfoMessage;
	private  final Panel mInfoMessagePanel;
	
//	private ArrayList<HTML> mQuarterInfoMessageList = new ArrayList<HTML>();
//	private ArrayList<Panel> mQuarterInfoMessagePanelList = new ArrayList<Panel>();

	public LightViewGwtImpl() {
	//	mAreaStatusChanged = getAreaValueChangeHandler();
		main = new RootFlexPanel();
		main.getElement().getStyle().setBackgroundColor("black");
		 mainScrollPanel = new ScrollPanel();
		mainScrollPanel.addStyleName(LIGHT_SCROLL_PANEL);
//		scrollPanel.setWidth("100px");
		
		mInfoMessage=  new HTML("info");
		mInfoMessage.addStyleName(MAIN_SCREEN_MSG);

		mInfoMessagePanel = new Panel();
		mainPanel= new Panel();


		
	}

	private ValueChangeHandler<Boolean> getPoleValueChangeHandler(final Quarter quarter, final MCheckBox checkBox, final LightArea lightPole) {
		changeInfoMsg(quarter.mQuarterNum, MESSAGES.changingPoleStatus());
		return new ValueChangeHandler<Boolean>() {

			@Override
			public void onValueChange(final ValueChangeEvent<Boolean> newBoolValueToSet) {
				StaticInfo.GET_MalabiDBServiceAsync().changeAreaServer(StaticInfo.USER_ID, quarter, lightPole, newBoolValueToSet.getValue(), new AsyncCallback<Integer>() {
					@Override public void onFailure(Throwable arg0) {
						checkBox.setValue(!newBoolValueToSet.getValue(), false);
						changeInfoMsg(quarter.mQuarterNum, StaticInfo.GET_MalaMessages().actionFail());
					}
					
	
					@Override public void onSuccess(Integer resultStatus) {	
						setMessageInfoByState(quarter.mQuarterNum, resultStatus);
						if (resultStatus != 0) {
							checkBox.setValue(!newBoolValueToSet.getValue(), false);
						}
					}
				});	
			}	
		};

	}

	private ValueChangeHandler<Boolean> getQuarterValueChangeHandler(final Quarter quarter, final MCheckBox master, final List<MCheckBox> listArea) {
		return new ValueChangeHandler<Boolean>() {

			@Override
			public void onValueChange(final ValueChangeEvent<Boolean> newBoolValueToSet) {
				StaticInfo.GET_MalabiDBServiceAsync().changeQuarterServer(StaticInfo.USER_ID, quarter, master.getValue(), new AsyncCallback<Integer>() {
					@Override public void onFailure(Throwable arg0) {
						setMainQuarterValue(master, mSubAreas.get(quarter.mQuarterNum));
						changeInfoMsg(quarter.mQuarterNum, StaticInfo.GET_MalaMessages().actionFail());
					}


					@Override public void onSuccess(Integer resultStatus) {
						setMessageInfoByState(quarter.mQuarterNum, resultStatus);
						if (resultStatus == 0) {
							for (MCheckBox checkBox : listArea) {
								checkBox.setValue(master.getValue(), false);
							}
						}
						else {
							setMainQuarterValue(master, mSubAreas.get(quarter.mQuarterNum));
						}

					}
				});				
			}
		};

				
	}
	
	/**
	 * 0 = good change
	 * -1 = problem on changing
	 * -2 = no communication with adam
	 * 1 = user is logged off
	 * 2 = user is not active.
	 * 
	 * @param resultStatus
	 */
	private void setMessageInfoByState(int quarterNum, int resultStatus) {
		if (resultStatus == 0)
			changeInfoMsg(quarterNum,"");
		else if (resultStatus == -1) {
			//checkBox.setValue(!newBoolValueToSet.getValue(), false);
			changeInfoMsg(quarterNum,StaticInfo.GET_MalaMessages().actionFail());
		}
		else if (resultStatus == 1) {
			changeInfoMsg(quarterNum,MESSAGES.userOffline());
		}
		else if (resultStatus == -2) {
			changeInfoMsg(quarterNum,MESSAGES.adamCommIssue());
		}
		else if (resultStatus == 2) {
			changeInfoMsg(quarterNum,MESSAGES.userNotActiveAction());
		}
		else if (resultStatus == 3) {
			changeInfoMsg(quarterNum,MESSAGES.userNotValidForControler());
		}
		
	}


	private Label createLabel(String labelName, String styleName, FlowPanel parentPanel)
	{
		Label label = new Label(labelName);
		label.addStyleName("malabi-light-label");
		label.addStyleName(styleName);
		parentPanel.add(label);
		return label;
	}
	
	private MCheckBox createAllQuarterCheckBox(Quarter quarter, String name, FlowPanel parentPanel, ArrayList<MCheckBox> areaList)
	{
		MCheckBox checkBox = new MCheckBox();
		//checkBox.setValue(false);
		checkBox.addStyleName(QUARTER_PANEL_ALL_CHECKBOX);
		mLightActivity.addHandlerRegistration(checkBox.addValueChangeHandler(getQuarterValueChangeHandler(quarter, checkBox, areaList)));
		parentPanel.add(checkBox);
		createLabel(name, QUARTER_PANEL_ALL_LABEL, parentPanel);

		return checkBox;
	}
	
	private static String getNameByIndex(int area, int quarter) {
		return new StringBuilder().append(INDEX).append("_").append(quarter).append("_").append(area).toString();
	}
	
	private MCheckBox createCheckBoxAndLabelForAreaWithIndex(LightArea lightPole, FlowPanel parentPanel, int areaNum,Quarter quarter)
	{
		FlowPanel areaWithIndexPanel = new FlowPanel();
		areaWithIndexPanel.addStyleName(QUARTER_INDEX_AREA_PANEL);
		areaWithIndexPanel.addStyleName(getNameByIndex(areaNum, quarter.mQuarterNum));
		//color: rgb(0, 32, 96);
		final MCheckBox checkBox = new MCheckBox();
		checkBox.setValue(lightPole.mState);
		checkBox.addStyleName(QUARTER_PANEL_AREA_CHECKBOX);
		
		mLightActivity.addHandlerRegistration(checkBox.addValueChangeHandler(getPoleValueChangeHandler(quarter, checkBox, lightPole)));
		//createLabel(lightPole.mName, QUARTER_PANEL_AREA_LABEL, areaWithIndexPanel);
		areaWithIndexPanel.add(checkBox);
		parentPanel.add(areaWithIndexPanel);

		return checkBox;
	}
	
	private void createExpandImageButton(final FlowPanel associatedPanel, final FlowPanel parentPanel,
			final boolean isControlQuarterPanel, final MCheckBox mCheckBox, final List<MCheckBox> thisAreaList) {
		final Button expandImageButton = new Button(PLUS);
		//expandImageButton.getElement().setInnerHTML("<i class=\"fas fa-"+PLUS+"\"></i>");
		expandImageButton.addStyleName(QUARTER_EXPAND_ICON);
		TapHandler tapHandler = new TapHandler() {
			
			@Override
			public void onTap(TapEvent event) {
				boolean isClosed = expandImageButton.getText().equals(PLUS);
				String newValue = isClosed ? MINUS : PLUS;
				expandImageButton.setText(newValue);
				if (isClosed) {
					associatedPanel.removeStyleName(HIDDEN_TAG);
					if (isControlQuarterPanel) {
						mCheckBox.disableButton();
					}
				}
				else {
					associatedPanel.addStyleName(HIDDEN_TAG);
					if (isControlQuarterPanel)
					{
						if (isAllDisable(thisAreaList)) {
							mCheckBox.disableButton();
						}
						else {
							mCheckBox.stopDisable();
							setMainQuarterValue(mCheckBox, thisAreaList);
						}
					}
				}
				mainScrollPanel.refresh();

			}
		};
		mLightActivity.addHandlerRegistration(expandImageButton.addTapHandler(tapHandler));
		parentPanel.add(expandImageButton);
	}
	
	/**
	 * Changing the Main CheckBox of the quarter to true/ false/ middle by the value of all the areas values.
	 * @param allQuarterCheckBox
	 * @param thisAreaList
	 */
	private void setMainQuarterValue(MCheckBox allQuarterCheckBox,List<MCheckBox> areaList) {
		if (isAllSame(areaList))
		{
			allQuarterCheckBox.setValue(areaList.get(0).getValue(), false);
		}
		else
		{
			allQuarterCheckBox.middleState();
		}
	}

	private boolean isAllDisable(List<MCheckBox> areaList) {
		return areaList.get(0).isDisableButton();
	}
	
	private boolean isAllSame(List<MCheckBox> areaList) {
		boolean value = areaList.get(0).getValue();
		for (MCheckBox checkBox : areaList) {
			if (value != checkBox.getValue())
				return false;
		}
		return true;
	}
	
	/**
	 * Adding Quarter panels to screen
	 */
	private void initQuarterItemsOnScreen() {
		String subArearsString = MESSAGES.subAreas();
		String allQuarterString =  MESSAGES.allQuarter();
		String quarterString = MESSAGES.quarter();
		
		ArrayList<FlowPanel> panelList =  new ArrayList<FlowPanel>();
		
		 FlowPanel allQuarterFlowPanel;
		 FlowPanel areaQuarterFlowPanel;
		 
		for (Quarter quarter : StaticInfo.MALABI_INFO.mQuarters)
		{
			final FlowPanel quarterFlexPanel = new FlowPanelEx(QUARTER_PANEL);
			mQuarterFlexPanelList.add(quarterFlexPanel);
			quarterFlexPanel.addStyleName(HIDDEN_TAG);
			panelList.add(quarterFlexPanel);
			
			//** TITLE PANEL - Start
			final FlowPanel quarterTitlePanel= new FlowPanelEx(QUARTER_TITLE_PANEL);
			
			createExpandImageButton(quarterFlexPanel, quarterTitlePanel, false, null, null);
			
//			quarterTitlePanel.add(new FlexSpacer());	
			createLabel(quarterString+" "+ quarter.mQuarterNum ,QUARTER_TITLE, quarterTitlePanel);

			
//			quarterTitlePanel.add(new FlexSpacer());
			/**** INFO*/
			final HTML quarterInfoMessage=  new HTML("info");
			quarterInfoMessage.addStyleName(QUARTER_PANEL_SUBAREA_INFO_MSG);
//			mInfoMessagePanel.addStyleName(HIDDEN);
//			mInfoMessagePanelList.add(mInfoMessagePanel);
			mInfoMessageList.put(quarter.mQuarterNum,quarterInfoMessage);
			/*** INFO*/
			
			quarterTitlePanel.add(quarterInfoMessage);
			//quarterTitlePanel.setHeight("450px");
			mainPanel.add(quarterTitlePanel);

			//** TITLE PANEL - End
			
			final ArrayList<MCheckBox> areaList = new ArrayList<MCheckBox>();
			// ***  All quarter 1 line panel - title + checkBox - START 
			allQuarterFlowPanel = new FlowPanelEx(QUARTER_ALL_PANEL);
			final MCheckBox allQuarterCheckBox = createAllQuarterCheckBox(quarter, allQuarterString,allQuarterFlowPanel, areaList);
			mQuarterCheckBoxList.add(allQuarterCheckBox);
			quarterFlexPanel.add(allQuarterFlowPanel);	
			// ***  All quarter 1 line panel - END 
			
			// SUBS AREA Panel + title 1 line - title + expand
			final FlowPanel subAreaTitlePanel= new FlowPanelEx(QUARTER_PANEL_SUBAREA_PANEL);
			areaQuarterFlowPanel = new FlowPanelEx(QUARTER_AREA_PANEL);
			areaQuarterFlowPanel.addStyleName(HIDDEN_TAG);
			createExpandImageButton(areaQuarterFlowPanel, subAreaTitlePanel, true, allQuarterCheckBox, areaList);

			subAreaTitlePanel.add(new FlexSpacer());	
			createLabel(subArearsString, QUARTER_PANEL_SUBAREA_LABEL, subAreaTitlePanel);


			subAreaTitlePanel.add(new FlexSpacer());
			quarterFlexPanel.add(subAreaTitlePanel);
			// SUBS AREA End
			
			// MAP PANEL
			//areaQuarterFlowPanel = new FlowPanelEx(QUARTER_AREA_PANEL);
			areaQuarterFlowPanel.addStyleName(INDEX+"_"+quarter.mQuarterNum);
			int area =0;
			for (LightArea lightPole : quarter.mLightAreas)
			{
				areaList.add(createCheckBoxAndLabelForAreaWithIndex(lightPole, areaQuarterFlowPanel, area, quarter));
				area++;
			}
			

			quarterFlexPanel.add(areaQuarterFlowPanel);
			// MAP PANEL
			mainPanel.add(quarterFlexPanel);
			mSubAreas.put(quarter.mQuarterNum, areaList);
			quarterInfoMessage.setText(MESSAGES.adamLoadingQuarter());
			
			StaticInfo.GET_MalabiDBServiceAsync().getAreasInQuarterState(quarter.mNumOfAreasInQuarter, quarter.mQuarterNum, new AsyncCallback<LightAreasStatus>() {
				
				@Override
				public void onSuccess(LightAreasStatus lightPolesStatus) {
					if (lightPolesStatus.mReturnState == 0) {
						quarterInfoMessage.setText("");
						quarterInfoMessage.addStyleName(HIDDEN);
						final ArrayList<MCheckBox> checkBoxs = mSubAreas.get(lightPolesStatus.mQuarterIndex);
						for (int i = 0; i < lightPolesStatus.mAreasState.size(); i++)
						{
							checkBoxs.get(i).setValue(lightPolesStatus.mAreasState.get(i), false);
						}
						setMainQuarterValue(allQuarterCheckBox, checkBoxs );
						return;
					}
					if (lightPolesStatus.mReturnState == -1) {
						quarterInfoMessage.setText(MESSAGES.adamCommIssue());
						quarterInfoMessage.removeStyleName(HIDDEN);
						setCheckBoxToDisable(areaList);
						allQuarterCheckBox.disableButton();
						return;
					}
					if (lightPolesStatus.mReturnState == -2) {
						quarterInfoMessage.setText(MESSAGES.adamError());
						quarterInfoMessage.removeStyleName(HIDDEN);
						setCheckBoxToDisable(areaList);
						allQuarterCheckBox.disableButton();
						return;
					}
				}
				
				@Override
				public void onFailure(Throwable arg0) {
					quarterInfoMessage.setText(MESSAGES.actionFail());
					
				}
			});
			
			//break;
//			HTML thtmlHtml = new HTML(getHtml());
//			main.add(thtmlHtml);
//			createLabel(22 +i*7, 32, MESSAGES.quarter() +String.valueOf(mLightControlList.get(i).mID), main);
		}
		mainScrollPanel.add(mainPanel);
		main.add(mainScrollPanel);

//		scrollPanel.setShowVerticalScrollBar(true);
//		scrollPanel.setShowHorizontalScrollBar(true);
//		scrollPanel.setScrollingEnabledX(true);
//		scrollPanel.setScrollingEnabledY(true);
		mainScrollPanel.refresh();
	}
	
	@Override
	public void UpdateLightControlList() {
	
		if (!mQuarterCheckBoxList.isEmpty())
			return;
		
		mInfoMessagePanel.addStyleName(MAIN_SCREEN_INFO_PANEL);

		mInfoMessagePanel.add(mInfoMessage);
		mInfoMessagePanel.addStyleName(HIDDEN);
		main.add(mInfoMessagePanel);
		initQuarterItemsOnScreen();
	}
	
	public void setMainInfoMessage(String msg) {
		mInfoMessagePanel.setStyleName(HIDDEN, msg.isEmpty());
		mInfoMessage.setText(msg);
	}
	
	private void setCheckBoxToDisable(ArrayList<MCheckBox> checkBoxs) {
		if (checkBoxs == null)
			return;
		for (int i = 0; i < checkBoxs.size(); i++) {
			if (checkBoxs.get(i) != null)
				checkBoxs.get(i).disableButton();
		}
	}
	
	/**
	 * Changing + hide\ display info box for the user.
	 */
	private void changeInfoMsg(int quarterNum, String msg) {
		mInfoMessageList.get(quarterNum).setText(msg);
	}
	
	@Override
	public Widget asWidget() {
		return main;
	}
	
	@Override
	public void clearViewObjects() {
	    mQuarterCheckBoxList.clear();
	    mInfoMessageList.clear();
	    for (ArrayList<MCheckBox> list :mSubAreas.values()) {
	    	list.clear();
	    }
	    for (FlowPanel flowPanel :mQuarterFlexPanelList) {
	    	flowPanel.clear();
	    }
	    mSubAreas.clear();
	    mInfoMessagePanel.clear();
	    mainPanel.clear();
	    mainScrollPanel.clear();
	}

	@Override
	public void setControler(LightActivity lightActivity) {
		if (mLightActivity == null)
			mLightActivity = lightActivity;
	}
	
	

}
