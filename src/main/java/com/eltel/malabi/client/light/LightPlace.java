package com.eltel.malabi.client.light;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
public class LightPlace extends Place {
	public static class LightPlaceTokenizer implements PlaceTokenizer<LightPlace> {

		@Override
		public LightPlace getPlace(String token) {
			return new LightPlace();
		}

		@Override
		public String getToken(LightPlace place) {
			return "";
		}

	}
}
