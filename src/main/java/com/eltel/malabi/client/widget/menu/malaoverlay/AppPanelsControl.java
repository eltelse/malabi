package com.eltel.malabi.client.widget.menu.malaoverlay;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.dom.client.StyleInjector;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiFactory;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.mgwt.dom.client.event.animation.TransitionEndEvent;
import com.googlecode.mgwt.dom.client.event.animation.TransitionEndHandler;
import com.googlecode.mgwt.ui.client.util.CssUtil;
import com.googlecode.mgwt.ui.client.widget.animation.AnimationWidget;

/**
 * Considered experimental and might change, use at your own risk
 */
public class AppPanelsControl extends Composite {

  private static final int TRANSITION_TIME_IN_MS = 200;

  private static final MalaOverlayMenuAppearance APPEARANCE = GWT.create(DefaultMalaOverlayMenuAppearance.class);

  private static int menuId = 0;

  @UiField
  protected FlowPanel container;
  @UiField
  protected FlowPanel top;
  @UiField
  protected FlowPanel middle;
  @UiField
  protected FlowPanel bottom;
  @UiField
  protected AnimationWidget animationWidget;

  private String id;

  protected final MalaOverlayMenuAppearance appearance;

  private HandlerRegistration showMainHandler;

  private HandlerRegistration hideMainHandler;

  public AppPanelsControl() {
    this(APPEARANCE);
  }

  public AppPanelsControl(MalaOverlayMenuAppearance appearance) {
    this.appearance = appearance;
    initWidget(appearance.uiBinder().createAndBindUi(this));

    id = "__mgwt_malaoverlaymenu__" + menuId++;
    getElement().setId(id);
    String bottomClass = appearance.css().bottom();
    String middleClass = appearance.css().middle();
    String topClass = appearance.css().top();
   // String infoClass = appearance.css().info();

    StringBuilder css = new StringBuilder();

    // nasty hack since we can not use media queries directly
    css.append("@media (orientation:portrait) {\n");

    //top
    css.append("#" + id + " ." + topClass + "{\n");
//   // css.append("height: 701px;");
    css.append("}");
    
    //middle
    css.append("#" + id + " ." + middleClass + "{\n");
   // css.append("height: 701px;");
    css.append("}");
    
    //button
    css.append("#" + id + " ." + bottomClass + "{\n");
  //  css.append("top: 701px;");
   // css.append("height: 769px;");
    css.append("}");

//    css.append("#" + id + " ." + infoClass + "{\n");
//    css.append("top: 828px;");
//    css.append("}");    css.append("}");

    StyleInjector.inject(css.toString());
  }

  public void setMiddle(Widget master) {
    animationWidget.setFirstWidget(master);
    animationWidget.animate(null, true, null);
  }

  public void setBottom(Widget bottomWidget) {
    bottom.clear();
    if (bottomWidget != null) {
      bottom.add(bottomWidget);
    }
  }
  
  public void setTop(Widget topWidget) {
	    top.clear();
	    if (topWidget != null) {
	      top.add(topWidget);
	    }
	  }

  @UiFactory
  protected static MalaOverlayMenuAppearance getAppearance() {
	  return APPEARANCE;
  }

  public void showMiddle(boolean show) {
    if (!CssUtil.hasTransistionEndEvent()) {
    	top.setVisible(show);
      middle.setVisible(show);
      if (show) {
        bottom.getElement().getStyle().clearLeft();
      } else {
        bottom.getElement().getStyle().setLeft(0, Unit.PX);
      }
      return;
    }

    if (show) {
    	CssUtil.setTransitionDuration(top.getElement(), TRANSITION_TIME_IN_MS);
      CssUtil.setTransitionDuration(middle.getElement(), TRANSITION_TIME_IN_MS);
      CssUtil.setTransitionDuration(bottom.getElement(), TRANSITION_TIME_IN_MS);
      top.getElement().getStyle().setOpacity(1);
      top.getElement().getStyle().setDisplay(Display.BLOCK);
      middle.getElement().getStyle().setOpacity(1);
      middle.getElement().getStyle().setDisplay(Display.BLOCK);      bottom.getElement().getStyle().clearLeft();
      showMainHandler = middle.addDomHandler(new TransitionEndHandler() {

        @Override
        public void onTransitionEnd(TransitionEndEvent event) {
          if (showMainHandler != null) {
            showMainHandler.removeHandler();
            showMainHandler = null;
            top.getElement().getStyle().clearDisplay();
            middle.getElement().getStyle().clearDisplay();
            
          }
        }
      }, TransitionEndEvent.getType());
    } else {
      CssUtil.setTransitionDuration(top.getElement(), TRANSITION_TIME_IN_MS);
      CssUtil.setTransitionDuration(middle.getElement(), TRANSITION_TIME_IN_MS);
      CssUtil.setTransitionDuration(bottom.getElement(), TRANSITION_TIME_IN_MS);
      top.getElement().getStyle().setOpacity(0);
      middle.getElement().getStyle().setOpacity(0);

      
      bottom.getElement().getStyle().setLeft(0, Unit.PX);
      hideMainHandler = middle.addDomHandler(new TransitionEndHandler() {

        @Override
        public void onTransitionEnd(TransitionEndEvent event) {
          if (hideMainHandler != null) {
            hideMainHandler.removeHandler();
            hideMainHandler = null;
            top.getElement().getStyle().setDisplay(Display.NONE);
            middle.getElement().getStyle().setDisplay(Display.NONE);
          }
        }
      }, TransitionEndEvent.getType());
    }
  }
}
