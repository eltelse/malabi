package com.eltel.malabi.client.widget.menu.malaoverlay;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.mgwt.ui.client.util.MGWTCssResource;

/**
 * Considered experimental and might change, use at your own risk
 */
public interface MalaOverlayMenuAppearance {

  public interface MenuCss extends MGWTCssResource {
    @ClassName("mgwt-MalaOverlayMenu")
    String container();
    @ClassName("mgwt-MalaOverlayMenu-top")
    String top();
    @ClassName("mgwt-MalaOverlayMenu-middle")
    String middle();
    @ClassName("mgwt-MalaOverlayMenu-animation")
    String animationWidget();
    @ClassName("mgwt-MalaOverlayMenu-bottom")
    String bottom();
  }

  public UiBinder<Widget, AppPanelsControl> uiBinder();

  public MenuCss css();
}
