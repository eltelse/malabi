package com.eltel.malabi.client.widget.menu.malaoverlay;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Widget;/**
 * Considered experimental and might change, use at your own risk
 */
public class DefaultMalaOverlayMenuAppearance implements MalaOverlayMenuAppearance {

  private static SwipeMenuUiBinder uiBinder = GWT.create(SwipeMenuUiBinder.class);

  @UiTemplate("DefaultMalaOverlayMenu.ui.xml")
  interface SwipeMenuUiBinder extends UiBinder<Widget, AppPanelsControl> {
  }

  public interface Resource extends ClientBundle {

    public static Resource INSTANCE = GWT.create(Resource.class);

    @Source("mala-overlay-menu.css")
    public MenuCss css();
  }

  static {
    Resource.INSTANCE.css().ensureInjected();
  }

  @Override
  public UiBinder<Widget, AppPanelsControl> uiBinder() {
    return uiBinder;
  }

  @Override
  public MenuCss css() {
    return Resource.INSTANCE.css();
  }
}
