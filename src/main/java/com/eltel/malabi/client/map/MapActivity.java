package com.eltel.malabi.client.map;

import com.eltel.malabi.client.ClientFactory;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.web.bindery.event.shared.EventBus;
import com.googlecode.mgwt.mvp.client.MGWTAbstractActivity;

public class MapActivity extends MGWTAbstractActivity {

  private final ClientFactory clientFactory;

  public MapActivity(ClientFactory clientFactory) {
    this.clientFactory = clientFactory;

  }

  @Override
  public void start(AcceptsOneWidget panel, final EventBus eventBus) {
    MapView view = clientFactory.getMapView();

    panel.setWidget(view);
  }

}
