package com.eltel.malabi.client.map;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;public class MapPlace extends Place {
	public static class MapPlaceTokenizer implements PlaceTokenizer<MapPlace> {

		@Override
		public MapPlace getPlace(String token) {
			return new MapPlace();
		}

		@Override
		public String getToken(MapPlace place) {
			return "";
		}

	}
}
