package com.eltel.malabi.client.map;

import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.mgwt.ui.client.widget.panel.flex.RootFlexPanel;

public class MapViewGwtImpl implements MapView {

	private RootFlexPanel main;
	private HTML googleMap;
	

	public MapViewGwtImpl() {
		main = new RootFlexPanel();
		main.addStyleName("malabi-map-panel");

		int height =Window.getClientHeight()-150;
		int width =Window.getClientWidth()-20;
		//int width = Integer.valueOf(main.getElement().getStyle().getWidth());
		String url = "<iframe src='https://www.google.com/maps/d/embed?mid=1j3KrtsXAevN9HcjFiTbM1nwZ7OSyQYco' width='"+width+"' height='"+height+"' frameborder='0' style='border:0;position: relative;'></iframe>";
		 googleMap = new HTML(url);
	//	htm.getElement().getStyle().setHeight(333, Unit.PX);
		//htm.getElement().getStyle().setWidth(, Unit.PX);
		googleMap.addStyleName("malabi-map-panel-google-map");
		main.add(googleMap);

		Window.addResizeHandler(new ResizeHandler() {
			Timer resizeTimer = new Timer() {  
				@Override
				public void run() {
					int height =Window.getClientHeight() -150;
					int width =Window.getClientWidth() - 20;
					String url = "<iframe src='https://www.google.com/maps/d/embed?mid=1j3KrtsXAevN9HcjFiTbM1nwZ7OSyQYco' width='"+(width)+"' height='"+(height)+"' frameborder='0' style='border:0;position: relative;'></iframe>";
					googleMap.setHTML(url);
				}
			};
			
			@Override
			public void onResize(ResizeEvent arg0) {
				// TODO Auto-generated method stub
				resizeTimer.cancel();
				resizeTimer.schedule(250);
				
			}
		});
	}
	
	@Override
	public Widget asWidget() {
		return main;
	}

}
