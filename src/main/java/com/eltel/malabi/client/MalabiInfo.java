package com.eltel.malabi.client;

import java.io.Serializable;
import java.util.ArrayList;

public class MalabiInfo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3558032753055028470L;

	/**
	 * 
	 */

	public MalabiUser mMalabiUser;
	
	public ArrayList<Quarter> mQuarters;
	
	public ArrayList<SoundPole> mSoundPoles;
	
	public ArrayList <SoundInfo> mSounds;
	
//	public String mErrorMsg;

	public MalabiInfo()
	{
	}

	public MalabiInfo(MalabiUser malabiUser, ArrayList<Quarter> quarters, ArrayList<SoundPole> soundPoles) {
//			, String errorMsg ) {
		super();
		this.mMalabiUser = malabiUser;
//		this.mSounds = sounds;
		this.mSounds = new ArrayList<SoundInfo>();
		this.mQuarters = quarters;
		this.mSoundPoles= soundPoles;
//		this.mErrorMsg= errorMsg;
	}
	

	
	public void InitAll()
	{
		mSoundPoles =null;
		mQuarters=null;
		mSounds=null;
		mMalabiUser = null;
//		mErrorMsg= "";
	}
	
}
