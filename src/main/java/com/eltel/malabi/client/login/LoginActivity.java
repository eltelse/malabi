package com.eltel.malabi.client.login;

import com.eltel.malabi.client.ClientFactory;
import com.eltel.malabi.client.StaticInfo;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.web.bindery.event.shared.EventBus;
import com.googlecode.mgwt.mvp.client.MGWTAbstractActivity;


public class LoginActivity extends MGWTAbstractActivity {

	private final ClientFactory clientFactory;
	private final  LoginView view;
	private boolean mIsInit =false;


	public LoginActivity(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
		view=  clientFactory.getHomeLoginView();
	}

	private int getUserId() {
		return StaticInfo.USER_ID;
	}

	private void addWebClosingHandler() {
		Window.addWindowClosingHandler(new Window.ClosingHandler() {
			@Override
			public void onWindowClosing(final Window.ClosingEvent closingEvent) {
				StaticInfo.GET_MalabiDBServiceAsync().logoffServer(getUserId(), new AsyncCallback<Boolean>() {
					@Override	public void onFailure(Throwable arg0) {}
					@Override	public void onSuccess(Boolean arg0) {}
				}
						);}
		});

	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		if (!mIsInit) {
			addWebClosingHandler();
			mIsInit = true;
		}
		view.setPlaceController(clientFactory.getPlaceController());
		panel.setWidget(view);
	}
}
