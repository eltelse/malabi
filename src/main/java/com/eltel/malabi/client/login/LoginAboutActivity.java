package com.eltel.malabi.client.login;

import com.eltel.malabi.client.ClientFactory;
import com.eltel.malabi.client.activities.AboutActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;


public class LoginAboutActivity extends AboutActivity {

	public LoginAboutActivity(ClientFactory clientFactory) {
		super(clientFactory, "nav");
		isLogin= true;

	}

	@Override
	public void start(AcceptsOneWidget panel, final EventBus eventBus) {
		super.start(panel, eventBus);
		LoginAboutView loginAboutView = clientFactory.getLoginAboutView();
		panel.setWidget(loginAboutView);

	}

}
