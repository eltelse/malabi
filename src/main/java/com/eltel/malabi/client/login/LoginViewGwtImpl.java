package com.eltel.malabi.client.login;

import com.eltel.malabi.client.MalabiInfo;
import com.eltel.malabi.client.StaticInfo;
import com.eltel.malabi.client.mainScreen.MainScreenPlace;
import com.eltel.malabi.client.messages.MalaMessages;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.ui.client.widget.button.Button;
import com.googlecode.mgwt.ui.client.widget.input.MPasswordTextBox;
import com.googlecode.mgwt.ui.client.widget.input.MTextBox;
import com.googlecode.mgwt.ui.client.widget.panel.Panel;
import com.googlecode.mgwt.ui.client.widget.panel.flex.RootFlexPanel;


public class LoginViewGwtImpl implements LoginView {

	private static String MALA = "MALA";
	private static String WIRELESS_CONTROL = "Wireless Control";
	private static String HIDDEN = "hidden";
	
	private static String LOGIN_LABEL = "malabi-login-label";
	private static String LOGIN_LABEL_APPENDIX = "-label";
	
	private static String LOGIN_LINE= "malabi-login-line";
	private static String LOGIN_USER_LINE = "malabi-login-user-line";
	private static String LOGIN_PASWORD_LINE = "malabi-login-password-line";
	
	private static String LOGIN_USER_ICON= "malabi-login-user-icon";
	private static String LOGIN_PASSWORD_ICON = "malabi-login-password-icon";
	private static String LOGIN_ICON = "malabi-login-icon";
	
	private static String LOGIN_MIDDLE_SCREEN= "malabi-login-middle-screen";
	private static String LOGIN_TEXTBOX = "malabi-login-textbox";
	private static String LOGIN_USER_TEXTBOX = "malabi-login-user-textbox";
	private static String LOGIN_PASSWORD_TEXTBOX = "malabi-login-password-textbox";

	private static String LOGIN_INFO_PANEL = "malabi-login-info-panel";
	private static String LOGIN_INFO_MSG = "malabi-login-info-msg";
	
	private static String LOGIN_BUTTON = "malabi-login-button";
	
	private static String ICON_A= "<i class=\"material-icons\">";
	private static String ICON_B= "</i>";
	
	private PlaceController mPlaceController;

	private RootFlexPanel main = new RootFlexPanel();
	private Button mLoginButton;
	private MalaMessages messages = StaticInfo.GET_MalaMessages();
	
	private MTextBox mUserTextBox;
	private MPasswordTextBox mPasswordTextBox;
	private HTML mInfoMessage;
	private Panel mInfoMessagePanel;
	
	private boolean mIsLoginLocked = false;
	
	public LoginViewGwtImpl() {
		//main = new RootFlexPanel();
		addHTMLLabel(MALA,1);
		addHTMLLabel(WIRELESS_CONTROL,2);
		initUI();
	}

	private void initUI() {


		mInfoMessage=  new HTML("info");
		mInfoMessage.addStyleName(LOGIN_INFO_MSG);

		mInfoMessagePanel = new Panel();
		mInfoMessagePanel.addStyleName(LOGIN_INFO_PANEL);
		
		mInfoMessagePanel.add(mInfoMessage);
		mInfoMessagePanel.addStyleName(HIDDEN);
		main.add(mInfoMessagePanel);
		addHTMLLine( LOGIN_USER_LINE);
		addHTMLLine( LOGIN_PASWORD_LINE);

		addHTMLIcon("face", LOGIN_USER_ICON); //👨‍💼
		addHTMLIcon("vpn_key", LOGIN_PASSWORD_ICON); //&#128273
		
		main.addStyleName(LOGIN_MIDDLE_SCREEN);

		mUserTextBox = new MTextBox();
		mUserTextBox.addStyleName(LOGIN_TEXTBOX);
		mUserTextBox.addStyleName(LOGIN_USER_TEXTBOX);
		mUserTextBox.setPlaceHolder(messages.userName());


		mPasswordTextBox = new MPasswordTextBox();
		mPasswordTextBox.addStyleName(LOGIN_TEXTBOX);
		mPasswordTextBox.addStyleName(LOGIN_PASSWORD_TEXTBOX);
		mPasswordTextBox.setPlaceHolder(messages.password());

		mLoginButton = new Button(messages.enter());
		mLoginButton.addStyleName(LOGIN_BUTTON);

		main.add(mUserTextBox);
		main.add(mPasswordTextBox);
		main.add(mLoginButton);
		
		
		mLoginButton.addTapHandler(new TapHandler() {

			 @Override
			 public void onTap(TapEvent event) {
				 if (getLockLoginState())
					 return;
				 setLockLoginState(true);
				 String userName = mUserTextBox.getText();
				 setInfo(messages.pleasewait());
				 StaticInfo.GET_MalabiDBServiceAsync().loginServer(userName, mPasswordTextBox.getText(), new AsyncCallback<MalabiInfo>() {
					 public void onSuccess(MalabiInfo result) {
						 
						 if (result.mMalabiUser.mIsValidUser) {
							 if (!result.mMalabiUser.mIsActive){
								 setInfo(messages.userNotActive());
							 }
							 else if (!result.mMalabiUser.mIsValidDate){
								 setInfo(messages.userNotValidDate());
							 }
							 else if (!result.mMalabiUser.mIsFirstUser){
								 setInfo(messages.doubleUser());
							 }
							 else {
								 StaticInfo.MALABI_INFO = result;
								 StaticInfo.USER_ID = result.mMalabiUser.mID;
								 cleanFields();
								 mPlaceController.goTo(new MainScreenPlace());
								 setInfo("");
							 }
						 }
						 else
						 {
							 setInfo(messages.wrongpass());
						 }
						 setLockLoginState(false);

					 }
					 public void onFailure(Throwable caught) {
						 setInfo(messages.malaerror());
						 setLockLoginState(false);
					 };

				 });
			 }
		});
	}
	
	private boolean getLockLoginState() {
		return mIsLoginLocked;
	}
	
	private void setLockLoginState(boolean state) {
		mIsLoginLocked = state;
	}
	
	private void addHTMLLabel (String label, int index ) {
		HTML newLabel1 = new HTML("<h1>"+label+"</h1>");
		addHTMLAndSewtStyle(newLabel1, LOGIN_LABEL, LOGIN_LABEL+index+LOGIN_LABEL_APPENDIX);
	}
	
	
	private void addHTMLLine (String label2) {
		HTML newLabel1 = new HTML();
		addHTMLAndSewtStyle(newLabel1, LOGIN_LINE, label2);
	}
	
	private void addHTMLAndSewtStyle (HTML newLabel1,String style1, String style2) {
		newLabel1.addStyleName(style1);
		newLabel1.addStyleName(style2);
		main.add(newLabel1);
	}
	
	private void addHTMLIcon (String icon, String style1) {
	  StringBuilder stringBuilderIcon = new StringBuilder().append(ICON_A).append(icon).append(ICON_B);
		HTML newLabel1 = new HTML(stringBuilderIcon.toString());
		addHTMLAndSewtStyle(newLabel1, style1, LOGIN_ICON);
	}

	@Override
	public Widget asWidget() {
		return main;
	}

	private void cleanFields() {
		mPasswordTextBox.setText("");
		mUserTextBox.setText("");
	}
	
	@Override
	public void setPlaceController(PlaceController placeController) {
		this.mPlaceController = placeController;		
	}
	
	 private void setInfo(final String msg) {
		 mInfoMessagePanel.setStyleName(HIDDEN, msg.isEmpty());
		 mInfoMessage.setText(msg);
	 }
}
