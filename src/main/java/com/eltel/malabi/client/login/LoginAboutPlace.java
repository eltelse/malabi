package com.eltel.malabi.client.login;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;


public class LoginAboutPlace extends Place {
	public static class LoginAboutPlaceTokenizer implements PlaceTokenizer<LoginAboutPlace> {

		@Override
		public LoginAboutPlace getPlace(String token) {
			return new LoginAboutPlace();
		}

		@Override
		public String getToken(LoginAboutPlace place) {
			return "";
		}

	}
}
