package com.eltel.malabi.client.login;

import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.IsWidget;
public interface LoginView extends IsWidget {

	public void setPlaceController(PlaceController placeController);
}

