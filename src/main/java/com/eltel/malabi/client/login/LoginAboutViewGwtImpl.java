/*

 *
 use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *

 *
 distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the under

 */package com.eltel.malabi.client.login;

import com.eltel.malabi.client.activities.AboutViewGwtImpl;


public class LoginAboutViewGwtImpl extends AboutViewGwtImpl implements LoginAboutView { 
	private static String LOGIN_BUTTOM_PANEL = "malabi-login-buttom-panel";

	public LoginAboutViewGwtImpl() {
	  buttomTitle.setText("");
	  headerPanel.addStyleName(LOGIN_BUTTOM_PANEL);
	}
}
