package com.eltel.malabi.client;

import java.io.Serializable;

public class SoundInfo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1387878256850922603L;

	public Integer mID;
	
	public String mFileName;
	
	public Integer mDuranceVal;
	
	public String mDisplayName;
	
	public String mDurance;

	public SoundInfo()
	{
		this.mID = -1;
		this.mFileName = "error.mp3";
		this.mDurance = "00:00:15";
		this.mDuranceVal = 15;
		this.mDisplayName = "No File";
	}
	public SoundInfo(Integer id, String fileName, String durance, String displayName, int duranceVal) {
		super();
		this.mID = id;
		this.mFileName = fileName;
		this.mDurance = durance;
		this.mDisplayName = displayName;
		this.mDuranceVal = duranceVal;
	}
	
	@Override
	public String toString() {
		return "SoundInfo- ID: "+mID+"; mFileName: "+ mFileName+"; mDurance: "+mDurance+"; mDuranceVal: "+mDuranceVal +" mDisplayName: "+mDisplayName+";";
	}
	
	
}
