package com.eltel.malabi.client.messages;

public interface MalaMessages  extends com.google.gwt.i18n.client.Messages{
	 
	@DefaultMessage(" 1 ���� �����")
	@Key("shloam")
	String shloam();
	
	@DefaultMessage("�� �����")
	@Key("userName")
	String userName();
	
	@DefaultMessage("�����")
	@Key("password")
	String password();
	
	@DefaultMessage("�����")
	@Key("enter")
	String enter();
	
	@DefaultMessage("�����")
	@Key("sound")
	String sound();
	
	@DefaultMessage("�����")
	@Key("light")
	String light();
	
	@DefaultMessage("���")
	@Key("map")
	String map();
	
	@DefaultMessage("���")
	@Key("choose")
	String choose();
	
	@DefaultMessage("���")
	@Key("cancel")
	String cancel();
	
	@DefaultMessage("���� �����")
	@Key("soundFile")
	String soundFile();
	
	@DefaultMessage("��� ���� ������")
	@Key("chooseAudioFile")
	String chooseAudioFile();
	
	@DefaultMessage("��� ������ ������")
	@Key("chooseSoundPoles")
	String chooseSoundPoles();
	
	@DefaultMessage("����")
	@Key("poles")
	String poles();
	
	@DefaultMessage("����")
	@Key("quarter")
	String quarter();	
	
	@DefaultMessage("��� �����")
	@Key("allQuarter")
	String allQuarter();
	
	
	@DefaultMessage("����")
	@Key("area")
	String area();
	
	@DefaultMessage("��� ������")
	@Key("subAreas")
	String subAreas();
	
	@DefaultMessage("����")
	@Key("help")
	String help();
	
	@DefaultMessage("�����")
	@Key("about")
	String about();
	
	@DefaultMessage("�����")
	@Key("logoff")
	String logoff();
	
	@DefaultMessage("�� ����� �� ����� ������")
	@Key("wrongpass")
	String wrongpass();
	
	@DefaultMessage("����� ���� ����")
	@Key("userNotActive")
	String userNotActive();
	
	@DefaultMessage("����� ������ ���� �����")
	@Key("userNotValidDate")
	String userNotValidDate();
	
	@DefaultMessage("��� ����")
	@Key("pleasewait")
	String pleasewait();
	
	@DefaultMessage("����� ����� �������")
	@Key("malaerror")
	String malaerror();
	
	@DefaultMessage("�� ���� �� ����")
	@Key("noSelectedSoundPoles")
	String noSelectedSoundPoles();

	@DefaultMessage("�� ����� �� ����� ������")
	@Key("noSelectedAudio")
	String noSelectedAudio();
	
	@DefaultMessage("����� {0} ������ ����  {1}")
	@Key("playingAudio")
	String playingAudio(String audioName, String lenght);
	
	@DefaultMessage("����� {0} ������ ")
	@Key("playingAudioFile")
	String playingAudioFile(String audioName);
	
	@DefaultMessage("{0} �����")
	@Key("timerCounter")
	String timerCounter(int lenght);
	
	@DefaultMessage("������� {0} ����� ������ ������")
	@Key("stopingAudio")
	String stopingAudios(String poles);	
	
	@DefaultMessage("����� {0} ����� ������ ������")
	@Key("stopingAudio")
	String stopingAudio(int pole);	
	
	@DefaultMessage("������ �����")
	@Key("actionFail")
	String actionFail();

	@DefaultMessage("���� {0} �� �������")
	@Key("nonActiveQuarters")
	String nonActiveQuarters(String indexs);

	@DefaultMessage("������ �� �����. ��� ��� ����.")
	@Key("failToDoAction")
	String failToDoAction();
	
	@DefaultMessage("����� ������ �� ����� ������. ��� ��� ����.")
	@Key("failToDoSoundAction")
	String failToDoSoundAction();
	
	@DefaultMessage("����� ������ �� ����� ������. ��� ��� ����.")
	@Key("failToDoStopAction")
	String failToDoStopAction();
	
	@DefaultMessage("����� ��� ������� 054-2557188.")
	@Key("helpMsg")
	String helpMsg();
		
	@DefaultMessage("���� �� ��� ������ �����")
	@Key("adamLoadingQuarter")
	String adamLoadingQuarter();
	
	@DefaultMessage("��� ������ ���� �����")
	@Key("adamCommIssue")
	String adamCommIssue();
	
	@DefaultMessage("���� ���� �����, �� ������")
	@Key("adamError")
	String adamError();
	
	@DefaultMessage("����� ����� ����� ������� �����")
	@Key("userOffline")
	String userOffline();
	
	@DefaultMessage("����� ����� ����� ������� ���� ����")
	@Key("userNotActiveAction")
	String userNotActiveAction();
	
	@DefaultMessage("����� ����� ����� ������ ������ ������ ���� �����")
	@Key("userNotValidForControler")
	String userNotValidForControler();

	@DefaultMessage("���� �� ��� ������.. ��� ����..")
	@Key("changingPoleStatus")
	String changingPoleStatus();

	@DefaultMessage("����� ������ ����� ���.")
	@Key("quarterIsInUse")
	String quarterIsInUse();

	@DefaultMessage("������ {0} ������  ������ ����� ���")
	@Key("usedPoles")
	String usedPoles(String quarters);

	@DefaultMessage("������ ����� ���")
	@Key("soundPoleInUse")
	String soundPoleInUse();

	@DefaultMessage("���� �����")
	@Key("playInAction")
	String playInAction();

	@DefaultMessage("���� �����")
	@Key("stopInAction")
	String stopInAction();

	@DefaultMessage("���� ����� �� ����� ������� ������")
	@Key("refreshSoundInfo")
	String refreshSoundInfo();
	
	@DefaultMessage("��� ���� ����� ����� ����� ������� ������")
	@Key("pleaeWaitToRefreshSoundInfo")
	String pleaeWaitToRefreshSoundInfo();
	
	@DefaultMessage("���� ����� �� ����� �������")
	@Key("refreshSoundPolesList")
	String refreshSoundPolesList();
	
	@DefaultMessage("���� �� ���� �������")
	@Key("poleIsNotOnline")
	String poleIsNotOnline();
	
	@DefaultMessage("����� ������ �� �����")
	@Key("poleCommIssue")
	String poleCommIssue();
	
	@DefaultMessage("���� ����� ������ �� �����")
	@Key("checkingPoleOnlineState")
	String checkingPoleOnlineState();

	@DefaultMessage("���� ����� �� ����� �������")
	@Key("refreshQuarterList")
	String refreshQuarterList();
	
	@DefaultMessage("�� ���� ������ ������ ��� ����� �� ���� �����")
	@Key("doubleUser")
	String doubleUser();
}
