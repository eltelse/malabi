package com.eltel.malabi.client;

import com.google.gwt.user.client.ui.FlowPanel;

public class FlowPanelEx extends FlowPanel {

	public FlowPanelEx() {
		super();
	}
	
	public FlowPanelEx(String styleName) {
		super();
		this.addStyleName(styleName);
	}
}
