package com.eltel.malabi.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("malabidb")
public interface MalabiDBService extends RemoteService {
	MalabiInfo loginServer(String userName, String password) throws IllegalArgumentException;
	Boolean logoffServer(int userID) throws IllegalArgumentException;
	Integer changeAreaServer(int userID, Quarter quarter, LightArea lightPole, Boolean state) throws IllegalArgumentException;
	Integer changeQuarterServer(int userID,Quarter quarter, Boolean state) throws IllegalArgumentException;
	Integer playAudioServer(int userID, SoundInfo soundInfo, ArrayList<SoundPole> polesListToPlay) throws IllegalArgumentException;
	Integer stopAudioServer(int userID, ArrayList<SoundPole> polesListToStop) throws IllegalArgumentException;
	LightAreasStatus getAreasInQuarterState(int numOfPoles, int quarterNum)throws IllegalArgumentException;
	Boolean isUserLoggedOffServer(int userID) throws IllegalArgumentException;
	ArrayList<SoundInfo> getSoundInfoList() throws IllegalArgumentException;
	Boolean getPoleState(int poleNum) throws IllegalArgumentException;
}
