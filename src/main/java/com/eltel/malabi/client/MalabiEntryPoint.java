package com.eltel.malabi.client;

import com.eltel.malabi.client.css.AppBundle;
import com.eltel.malabi.client.mappers.PhoneActivityMapper;
import com.eltel.malabi.client.mappers.PhoneAnimationMapper;
import com.eltel.malabi.client.mappers.TabletBottumAnimationMapper;
import com.eltel.malabi.client.mappers.TabletButtomActivityMapper;
import com.eltel.malabi.client.mappers.TabletMiddleActivityMapper;
import com.eltel.malabi.client.mappers.TabletMiddleAnimationMapper;
import com.eltel.malabi.client.mappers.TabletTopActivityMapper;
import com.eltel.malabi.client.mappers.TabletTopAnimationMapper;
import com.eltel.malabi.client.places.HomePlace;
import com.eltel.malabi.client.widget.menu.malaoverlay.AppPanelsControl;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.StyleInjector;
import com.google.gwt.user.client.ui.RootPanel;
import com.googlecode.mgwt.mvp.client.AnimatingActivityManager;
import com.googlecode.mgwt.mvp.client.AnimationMapper;
import com.googlecode.mgwt.ui.client.MGWT;
import com.googlecode.mgwt.ui.client.MGWTSettings;
import com.googlecode.mgwt.ui.client.MGWTSettings.ViewPort;
import com.googlecode.mgwt.ui.client.util.SuperDevModeUtil;
import com.googlecode.mgwt.ui.client.widget.animation.AnimationWidget;


public class MalabiEntryPoint implements EntryPoint {

  private void start() {

    SuperDevModeUtil.showDevMode();

    ViewPort viewPort = new MGWTSettings.ViewPort();
    viewPort.setUserScaleAble(false).setMinimumScale(1.0).setMinimumScale(1.0).setMaximumScale(1.0);

    MGWTSettings settings = new MGWTSettings();
    settings.setViewPort(viewPort);
    settings.setIconUrl("c:\\logo.png");
    settings.setFullscreen(true);
    settings.setFixIOS71BodyBug(true);
    settings.setPreventScrolling(true);

    MGWT.applySettings(settings);

    final ClientFactory clientFactory = new ClientFactoryImpl();

    if (MGWT.getFormFactor().isTablet() || MGWT.getFormFactor().isDesktop()) {
      // very nasty workaround because GWT does not corretly support
      // @media
      StyleInjector.inject(AppBundle.INSTANCE.css().getText());

      createTabletDisplay(clientFactory);
    } else {
      createPhoneDisplay(clientFactory);
    }

    clientFactory.getPlaceController().goTo(new HomePlace());

  }

  private void createPhoneDisplay(ClientFactory clientFactory) {
    AnimationWidget display = new AnimationWidget();

    PhoneActivityMapper appActivityMapper = new PhoneActivityMapper(clientFactory);

    PhoneAnimationMapper appAnimationMapper = new PhoneAnimationMapper();

    AnimatingActivityManager activityManager = new AnimatingActivityManager(appActivityMapper, appAnimationMapper, clientFactory.getEventBus());

    activityManager.setDisplay(display);

    RootPanel.get().add(display);

  }

  private void createTabletDisplay(ClientFactory clientFactory) {

	  AnimationWidget topPanel = new AnimationWidget();
	AnimationWidget middlePanel = new AnimationWidget();
    AppPanelsControl mainPanelsHolder = new AppPanelsControl(); 
    
    
    
    //   TOP   ///
    ActivityMapper topActivityMapper = new TabletTopActivityMapper(clientFactory);

    AnimationMapper topAnimationMapper = new TabletTopAnimationMapper();

    AnimatingActivityManager topActivityManager = new AnimatingActivityManager(topActivityMapper, topAnimationMapper, clientFactory.getEventBus());

    topActivityManager.setDisplay(topPanel);
    

    //  MIDDLE   ///
    ActivityMapper middleActivityMapper = new TabletMiddleActivityMapper(clientFactory);

    AnimationMapper middleAnimationMapper = new TabletMiddleAnimationMapper();

    AnimatingActivityManager middleActivityManager = new AnimatingActivityManager(middleActivityMapper, middleAnimationMapper, clientFactory.getEventBus());

    middleActivityManager.setDisplay(middlePanel);
    

    //  BOTTOM   ///
    AnimationWidget bottomPanel = new AnimationWidget();

    TabletButtomActivityMapper bottomActivityMapper = new TabletButtomActivityMapper(clientFactory);

    AnimationMapper bottomAnimationMapper = new TabletBottumAnimationMapper();

    AnimatingActivityManager bottomActivityManager = new AnimatingActivityManager(bottomActivityMapper, bottomAnimationMapper, clientFactory.getEventBus());

    bottomActivityManager.setDisplay(bottomPanel);
    
    mainPanelsHolder.setTop(topPanel);  
    mainPanelsHolder.setMiddle(middlePanel);
    mainPanelsHolder.setBottom(bottomPanel);
   RootPanel.get().add(mainPanelsHolder);

  }

  @Override
  public void onModuleLoad() {
    start();
  }

}
