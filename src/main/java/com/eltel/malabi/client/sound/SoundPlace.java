package com.eltel.malabi.client.sound;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;public class SoundPlace extends Place {
	public static class SoundPlaceTokenizer implements PlaceTokenizer<SoundPlace> {

		@Override
		public SoundPlace getPlace(String token) {
			return new SoundPlace();
		}

		@Override
		public String getToken(SoundPlace place) {
			return "";
		}

	}
}
