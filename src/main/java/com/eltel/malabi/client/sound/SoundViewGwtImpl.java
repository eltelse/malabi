package com.eltel.malabi.client.sound;

import java.util.ArrayList;

import com.eltel.malabi.client.SoundInfo;
import com.eltel.malabi.client.SoundPole;
import com.eltel.malabi.client.StaticInfo;
import com.eltel.malabi.client.messages.MalaMessages;
import com.eltel.malabi.client.sound.ChooseTrack.ConfirmCallback;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.ui.client.widget.button.Button;
import com.googlecode.mgwt.ui.client.widget.input.checkbox.MCheckBox;
import com.googlecode.mgwt.ui.client.widget.panel.Panel;
import com.googlecode.mgwt.ui.client.widget.panel.flex.RootFlexPanel;

public class SoundViewGwtImpl implements SoundView {

	private RootFlexPanel main;
	private Button mPlayButton;
	private Button mChooseButton;
	private Button mStopButton;
	private String mAudioName;
	private Label mAudioNameWithTime;
	private SoundInfo mSoundInfo;
	private final ArrayList<String> mPolesNamesList = new ArrayList<String>();
	private final ArrayList<MCheckBox> mPolesButtonList = new ArrayList<MCheckBox>();
//	private final ArrayList<SoundPole> mPolesList = new ArrayList<SoundPole>();
	private ConfirmCallback mChooseTrackCallback;
	private boolean mIsSelectedAudio = false;


	private final HTML mInfoMessage;
	private  final Panel mInfoMessagePanel;
	private ArrayList<TimerEx> mInfoMessageTimerList = new ArrayList<SoundViewGwtImpl.TimerEx>();
	private ArrayList<Label> mTimeLabelList = new ArrayList<Label>();

	private static String SOUND_INFO_PANEL = "malabi-sound-info-panel";
	private static String SOUND_INFO_MSG = "malabi-sound-info-msg";
	private static String HIDDEN = "hidden";

	private static String SOUND_BUTTON = "malabi-sound-button";
	private static String SOUND_LABEL = "malabi-sound-label";
	private static String SOUND_TIME_LABEL = "malabi-sound-time-label";
	private static String SOUND_CHECKBOX = "malabi-sound-checkbox";

	private static String ICON_A= "<i class=\"material-icons\">";
	private static String ICON_B= "</i>";

	private MalaMessages messages = StaticInfo.GET_MalaMessages();
	private ValueChangeHandler<Boolean> mChangeHandler;
	private SoundActivity mSoundActivity;
	
	private boolean mIsSoundListLoad = true;
	
	private class TimerEx extends Timer{

		public String mAudioName;
		public int mLenth;
		public Label mLabel;

		public TimerEx(Label label) {
			mLabel = label;
		}

		public void initInfo(String audioName, int lenth) {
			cancel();
			mAudioName = audioName;
			mLenth = lenth;
		}
		
		private String getFormatTime(int lenth) {
			int p2 = lenth / 60;
			return p2 / 60 + ":" + p2 % 60 + ":" + lenth % 60;
		}

		@Override
		public void run() {
			mLenth = mLenth - 1;
			if (mLenth <0) {
				cancel();
			}
			else {
				mLabel.setText(messages.playingAudio(mAudioName,getFormatTime(mLenth)));
			}
		}
		
		@Override
		public void cancel() {
			mLabel.setText("");
			super.cancel();
		}

	}

	public SoundViewGwtImpl() {
		main = new RootFlexPanel();
		main.addStyleName("malabi-sound");
		mChangeHandler = getChangeHandler();

		mInfoMessage=  new HTML("info");
		mInfoMessage.addStyleName(SOUND_INFO_MSG);

		mInfoMessagePanel = new Panel();
	}
	
	private void initUI() {
		mChooseButton = createButtonRound("sound/sound_choose_icon", main, "music","music_note");
		
		mPlayButton = createButtonRound("sound/sound_play", main, "play","play_arrow");
		mStopButton = createButtonRound("sound/sound_play", main,"stop","stop");
		mAudioNameWithTime = createLabel(messages.chooseAudioFile(),main, "title");
		
		createLabel(messages.chooseSoundPoles(),main, "choose");
		mInfoMessagePanel.addStyleName(SOUND_INFO_PANEL);

		mInfoMessagePanel.add(mInfoMessage);
		mInfoMessagePanel.addStyleName(HIDDEN);
		main.add(mInfoMessagePanel);

		mSoundActivity.addHandlerRegistration(mChooseButton.addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) { //Open choose audio pop-up
				if (isSoundListFinishLoad()) {
					setInfoMessage("");
					openPopUp();
				}
				else {
					setInfoMessage(messages.pleaeWaitToRefreshSoundInfo());
				}
				
			}

		}));
		mSoundActivity.addHandlerRegistration(mPlayButton.addTapHandler(new TapHandler() {
			@Override
			public void onTap(TapEvent event) { //Play Button
				setInfoMessage(messages.playInAction());
				final ArrayList<SoundPole> playPolesList= new ArrayList<SoundPole>();
				final ArrayList<Integer> playPolesButtonList = new ArrayList<Integer>();
				final String playAudioName = getAudioValue();
				for (int i = 0 ; i <mPolesButtonList.size(); i++ )
				{
					if (mPolesButtonList.get(i).getValue()) {
						playPolesList.add(StaticInfo.MALABI_INFO.mSoundPoles.get(i));
						playPolesButtonList.add(i);
					}
				}

				if (!isSelectedAudio() || getSelectedSoundInfo() == null) {
					setInfoMessage(messages.noSelectedAudio());
					return;
				}

				if (playPolesList.isEmpty()) {
					setInfoMessage(messages.noSelectedSoundPoles());
					return;
				}

				StaticInfo.GET_MalabiDBServiceAsync().playAudioServer(StaticInfo.USER_ID, getSelectedSoundInfo(), playPolesList , new AsyncCallback<Integer>() {

					@Override
					public void onSuccess(Integer res) {
						if (res == 0 ) {
							for (Integer index : playPolesButtonList)
							{
//								 (mPolesButtonList.get(i).getValue()) {
									
									mInfoMessageTimerList.get(index).initInfo(playAudioName, getSelectedSoundInfo().mDuranceVal);
									mInfoMessageTimerList.get(index).scheduleRepeating(1000);
//								}
							}
							setInfoMessage("");//messages.playingAudioFile(getAudioValue()));

							cleanAudioChooise();
						}
						else {
//							setInfoMessage(messages.failToDoSoundAction());
							setMessageInfoByState(res);
						}
						playPolesButtonList.clear();
						playPolesList.clear();
					}

					@Override
					public void onFailure(Throwable res) {
						setInfoMessage(messages.failToDoAction());
						cleanAudioChooise();
						playPolesButtonList.clear();
						playPolesList.clear();
					}

				});
			}
		}));
		mSoundActivity.addHandlerRegistration(mStopButton.addTapHandler(new TapHandler() {
			@Override
			public void onTap(TapEvent event) {   //Stop Button
				setInfoMessage(messages.stopInAction());
				final ArrayList<SoundPole> stopPolesList= new ArrayList<SoundPole>();
				final ArrayList<Integer> stopPolesButtonList = new ArrayList<Integer>();
				for (int i = 0 ; i <mPolesButtonList.size(); i++ )
				{
					if (mPolesButtonList.get(i).getValue()) {
						stopPolesList.add(StaticInfo.MALABI_INFO.mSoundPoles.get(i));
						stopPolesButtonList.add(i);
					}
				}

				if (stopPolesList.isEmpty()) {
					setInfoMessage(messages.noSelectedSoundPoles());
					return;
				}

				StaticInfo.GET_MalabiDBServiceAsync().stopAudioServer(StaticInfo.USER_ID, stopPolesList, new AsyncCallback<Integer>() {

					@Override
					public void onSuccess(Integer res) {
						if (res ==0) {
							for (Integer index :stopPolesButtonList)
							{
									mInfoMessageTimerList.get(index).cancel();

							}
							if (stopPolesList.size() == 1) {
								setInfoMessage(messages.stopingAudio(stopPolesList.get(0).mQuarterId));
							}
							else {
								setInfoMessage(messages.stopingAudios(Get_Poles_Num(stopPolesList)));
							}
						}
						else {
							//setInfoMessage(messages.failToDoStopAction());
							setMessageInfoByState(res);
						}
						
						stopPolesButtonList.clear();
						stopPolesList.clear();

						//						cleanAudioChooise();
					}

					@Override
					public void onFailure(Throwable res) {
						setInfoMessage(messages.failToDoAction());
						stopPolesButtonList.clear();
						stopPolesList.clear();
						//						cleanAudioChooise();
					}

				});
			}
		}));
	}
	
	/**
	 * 0 = good change
	 * -1 = problem on changing
	 * -2 = no communication with adam
	 * 1 = user is logged off
	 * 2 = user is not active.
	 * @param resultStatus
	 */
	private void setMessageInfoByState(int resultStatus) {
		if (resultStatus == 0)
			setInfoMessage("");
		else if (resultStatus == -1) {
			//checkBox.setValue(!newBoolValueToSet.getValue(), false);
			setInfoMessage(messages.actionFail());
		}
		else if (resultStatus == 1) {
			setInfoMessage(messages.userOffline());
		}
		else if (resultStatus == -2) {
			setInfoMessage(messages.actionFail());
		}
		else if (resultStatus == 2) {
			setInfoMessage(messages.userNotActiveAction());
		}
		else if (resultStatus == 3) {
			setInfoMessage(messages.userNotValidForControler());
		}
		
	}

	private String Get_Poles_Num (ArrayList<SoundPole> polesList) {
		StringBuilder res = new StringBuilder();
		StringBuilder listId = new StringBuilder();
		for (SoundPole pole : polesList) {
			listId.append(pole.mQuarterId).append(", ");
		}
		if (!listId.toString().isEmpty())
			res.append(listId.substring(0,listId.length() -2));
		return res.toString();
	}

	private void openPopUp ()
	{
		if (mChooseTrackCallback == null) 
			mChooseTrackCallback = CreateChooseTrackHandler();
		
		ChooseTrack chooseTrackDialog= new ChooseTrack(mChooseTrackCallback, mSoundActivity);
		chooseTrackDialog.show();
	}

	private Button createButtonRound(String name,  RootFlexPanel flowPanel, String styleName, String iconName)
	{
		Button button = new Button();

		StringBuilder stringBuilderIcon = new StringBuilder().append(ICON_A).append(iconName).append(ICON_B);
		button.getElement().setInnerHTML(stringBuilderIcon.toString());
		button.addStyleName(SOUND_BUTTON);
		button.addStyleName(SOUND_BUTTON+"-"+styleName);
		flowPanel.add(button);
		return button;
	}

	private Label createLabel(String labelName, RootFlexPanel flowPanel, String styleName)
	{
		Label label = new Label(labelName);
		label.addStyleName(SOUND_LABEL);
		label.addStyleName(SOUND_LABEL+"-"+styleName);
		flowPanel.add(label);
		return label;

	}

	private Label createLabel(int top, String labelName, RootFlexPanel flowPanel, boolean isInfo)
	{
		final Label label = new Label(labelName);
		if (!isInfo)
			label.addStyleName(SOUND_LABEL);
		else {
			label.addStyleName(SOUND_TIME_LABEL);
			TimerEx timerEx = new TimerEx(label);
			mInfoMessageTimerList.add(timerEx);
		}
		label.getElement().getStyle().setTop(top, Unit.PCT);
		flowPanel.add(label);

		return label;
	}

	private void createCheckBox(int top, String name, RootFlexPanel flowPanel, MCheckBox checkBox)
	{
		StringBuilder stringBuilderIcon = new StringBuilder().append(ICON_A).append("done").append(ICON_B);
		checkBox.getElement().setInnerHTML(stringBuilderIcon.toString());//"✔");
		//MCheckBox checkBox = new MCheckBox();
		checkBox.setValue(false, false);
		changeCheckUI(checkBox.getElement(), false);
		checkBox.addStyleName(SOUND_CHECKBOX);
		checkBox.getElement().getStyle().setTop(top, Unit.PCT);
		mSoundActivity.addHandlerRegistration(checkBox.addValueChangeHandler(mChangeHandler));
		flowPanel.add(checkBox);

		createLabel(top, name, main, false);
		mTimeLabelList.add(createLabel(top+4, "", main, true));
	}

	private ValueChangeHandler<Boolean> getChangeHandler()
	{
		return  new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> arg0) {
				Object source =arg0.getSource();
				for (MCheckBox checkBox : mPolesButtonList)
				{
					if (checkBox.equals(source))
					{
						changeCheckUI(checkBox.getElement(), !arg0.getValue());
						return;
					}
				}
			}
		};
	}

	private void changeCheckUI(Element element, boolean state) {
	}

	@Override
	public Widget asWidget() {
		return main;
	}

	public void setTitle(String text, String audioName) {
		mAudioNameWithTime.setText(text);
		mAudioName = audioName;
	}
	
	private String getAudioValue() {
		return mAudioName;
	}


	private void setSelectedSoundInfo(SoundInfo soundInfo) {
		mSoundInfo = soundInfo;
	}

	private SoundInfo getSelectedSoundInfo() {
		return mSoundInfo;
	}

	@Override
	public void updateSoundViewScreen() {
		initUI();

		cleanAudioChooise();
		loadUpdatedSoundList();
		initPolesItemsOnScreen();
	}
	
	/**
	 * Adding poles items to screen
	 */
	private void initPolesItemsOnScreen() {
		for (int i = 0 ; i < StaticInfo.MALABI_INFO.mSoundPoles.size() ; i++)
		{
				mPolesNamesList.add(StaticInfo.MALABI_INFO.mSoundPoles.get(i).mName+" " +StaticInfo.MALABI_INFO.mSoundPoles.get(i).mLocation);
				MCheckBox poleCheckBox = new MCheckBox();
				createCheckBox(22 +i*9, mPolesNamesList.get(i) ,main, poleCheckBox);
				mPolesButtonList.add(poleCheckBox);
				mTimeLabelList.get(i).setText(messages.checkingPoleOnlineState());
				final int poleNum = i ;
				StaticInfo.GET_MalabiDBServiceAsync().getPoleState(StaticInfo.MALABI_INFO.mSoundPoles.get(i).mQuarterId, new AsyncCallback<Boolean>() {
					
					@Override
					public void onSuccess(Boolean poleOnline) {
						if (poleOnline) {
							mTimeLabelList.get(poleNum).setText("");
						}
						else {
							
							mTimeLabelList.get(poleNum).setText(messages.poleIsNotOnline());
							mPolesButtonList.get(poleNum).disableButton();
						}
					}
					
					@Override
					public void onFailure(Throwable arg0) {
						mTimeLabelList.get(poleNum).setText(messages.poleCommIssue());
					}
				});
		}
	}
	
	/**
	 * Getting update song list from the server.
	 */
	private void loadUpdatedSoundList() {
		setIsSoundListfinishLoad(false);
		setInfoMessage(messages.refreshSoundInfo());
		StaticInfo.GET_MalabiDBServiceAsync().getSoundInfoList(new AsyncCallback<ArrayList<SoundInfo>>() {

			@Override
			public void onSuccess(ArrayList<SoundInfo> updateSoungList) {
				StaticInfo.MALABI_INFO.mSounds.clear();
				StaticInfo.MALABI_INFO.mSounds = updateSoungList;
				setInfoMessage("");
				setIsSoundListfinishLoad(true);
				
			}

			@Override
			public void onFailure(Throwable res) {
				setInfoMessage(messages.failToDoAction());
				setIsSoundListfinishLoad(true);
			}

		});

	}

	
	public void setIsSoundListfinishLoad(boolean isSoundListLoad) {
		this.mIsSoundListLoad = isSoundListLoad;
	}
	
	public boolean isSoundListFinishLoad() {
		return mIsSoundListLoad;
	}

	@Override
	public void clearViewObjects() {
		mPolesNamesList.clear();
		mPolesButtonList.clear();
		mTimeLabelList.clear();
//		mPolesList.clear();
		mInfoMessagePanel.clear();
		for (TimerEx val : mInfoMessageTimerList)
			val.cancel();
			
		mInfoMessageTimerList.clear();
		main.clear();
	}

	private ConfirmCallback CreateChooseTrackHandler()
	{
		return new ConfirmCallback() {
			@Override
			public void onChoose(String selected, SoundInfo soundInfo) {	
				setTitle(selected, soundInfo.mDisplayName);
				setSelectedSoundInfo(soundInfo);
				setInfoMessage("");
				mIsSelectedAudio = true;

			}
			@Override
			public void onCancel() {
				cleanAudioChooise();
			}


		};
	}

	public void setInfoMessage(String msg) {
		mInfoMessagePanel.setStyleName(HIDDEN, msg.isEmpty());
		mInfoMessage.setText(msg);
	}

	private void cleanAudioChooise() {
		setTitle(messages.chooseAudioFile(),"");
		setSelectedSoundInfo(null);
		mIsSelectedAudio = false;
	}

	private boolean isSelectedAudio() {
		return mIsSelectedAudio;
	}

	@Override
	public void setControler(SoundActivity soundActivity) {
		if (mSoundActivity == null)
			mSoundActivity = soundActivity;
		
	}

}
