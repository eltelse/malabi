package com.eltel.malabi.client.sound.popup;

import com.google.gwt.dom.client.Element;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.mgwt.ui.client.util.MGWTCssResource;
import com.googlecode.mgwt.ui.client.widget.button.ButtonBaseAppearance;

public interface SoundDialogPanelAppearance extends ButtonBaseAppearance  {
  interface SoundDialogButtonCss extends ButtonBaseAppearance.ButtonBaseCss {
    @ClassName("mgwt-SoundDialogButton-cancel")
    public String okbutton();

    @ClassName("mgwt-SoundDialogButton-ok")
    public String cancelbutton();

    @Override
    @ClassName("mgwt-SoundDialogButton-active")
    public String active();

    @Override
    @ClassName("mgwt-SoundDialogButton")
    public String button();
  }

  interface SoundDialogCss extends MGWTCssResource {
    @ClassName("mgwt-SoundDialogPanel")
    String soundDialogPanel();

    @ClassName("mgwt-SoundDialogPanel-container")
    String container();

    @ClassName("mgwt-SoundDialogPanel-title")
    String title();

    @ClassName("mgwt-SoundDialogPanel-content")
    String content();

    @ClassName("mgwt-SoundDialogPanel-footer")
    String footer();
    
    @ClassName("mgwt-SoundDialogOverlay")
	String soundDialogOverlay();

    @ClassName("mgwt-SoundDialogOverlay-shadow")
    String animationContainerShadow();
  }

  SoundDialogCss overlayCss();
  
  SoundDialogCss soundDialogCss();

  @Override
  SoundDialogButtonCss css();

  UiBinder<Widget, SoundDialogPanel> soundDialogBinder();

  @Override
  UiBinder<Element, SoundDialogButton> uiBinder();
  
}
