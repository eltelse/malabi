package com.eltel.malabi.client.sound;

import com.eltel.malabi.client.ClientFactory;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.web.bindery.event.shared.EventBus;
import com.googlecode.mgwt.mvp.client.MGWTAbstractActivity;

public class SoundActivity extends MGWTAbstractActivity {

  private final SoundView view;
  public SoundActivity(ClientFactory clientFactory) {
    view = clientFactory.getSoundView();
  }

  @Override
  public void start(AcceptsOneWidget panel, final EventBus eventBus) {
	  view.setControler(this);
	  view.updateSoundViewScreen();
	  panel.setWidget(view);
  }
  
  @Override
	public void addHandlerRegistration(HandlerRegistration handlerRegistration) {
		super.addHandlerRegistration(handlerRegistration);
	}
  
  @Override
  public void addHandlerRegistration(com.google.web.bindery.event.shared.HandlerRegistration handlerRegistration) {
		super.addHandlerRegistration(handlerRegistration);
	}
  
  @Override
	public void onStop() {
		super.onStop();
		view.clearViewObjects();
	}
  
}