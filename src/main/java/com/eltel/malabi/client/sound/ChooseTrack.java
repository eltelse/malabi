package com.eltel.malabi.client.sound;

import java.util.ArrayList;

import com.eltel.malabi.client.SoundInfo;
import com.eltel.malabi.client.StaticInfo;
import com.eltel.malabi.client.messages.MalaMessages;
import com.eltel.malabi.client.sound.popup.SoundDialogPanel;
import com.eltel.malabi.client.sound.popup.SoundDialogPanelAppearance;
import com.eltel.malabi.client.sound.popup.SoundPopinDialogOverlay;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.ui.client.widget.dialog.Dialog;
import com.googlecode.mgwt.ui.client.widget.dialog.HasTitleText;
import com.googlecode.mgwt.ui.client.widget.input.radio.MRadioButton;
import com.googlecode.mgwt.ui.client.widget.panel.Panel;
import com.googlecode.mgwt.ui.client.widget.panel.scroll.ScrollPanel;


public class ChooseTrack implements HasTitleText, Dialog {
	/**
	 * The callback used when buttons are taped

	 */
	public interface ConfirmCallback {
		/**
		 * Called if ok button is taped
		 * @param selected 
		 */
		public void onChoose(String selected, SoundInfo soundInfo);

		/**
		 * called if cancel button is taped
		 */
		public void onCancel();
	}

	private SoundPopinDialogOverlay mPopinDialog;
	private SoundDialogPanel mSoundDialogPanel;
	private ConfirmCallback mConfirmCallBack;
	private ArrayList<MRadioButton> mButtonList = new ArrayList<MRadioButton>();
	private MalaMessages  MESSAGES = StaticInfo.GET_MalaMessages();
	private ScrollPanel mScrollPanel;
	private Panel mMainPanel;
	private SoundActivity mSoundActivity;

	/**
	 * @param callback - the callback used when a button of the dialog is taped
	 */
	public ChooseTrack( ConfirmCallback callback, SoundActivity soundActivity) {
		this.mConfirmCallBack = callback;
		this.mSoundActivity = soundActivity;
		SoundDialogPanelAppearance appearance =SoundDialogPanel.DEFAULT_APPEARANCE;
		mPopinDialog = new SoundPopinDialogOverlay(appearance);
		mSoundDialogPanel = new SoundDialogPanel(appearance);
		mSoundDialogPanel.addStyleName("malabi-sound-file-dialog");
		mSoundDialogPanel.showOkButton(true);
		mSoundDialogPanel.showCancelButton(true);
		mPopinDialog.add(mSoundDialogPanel);

		mSoundActivity.addHandlerRegistration(mSoundDialogPanel.getCancelButton().addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
				mPopinDialog.hide();
				if (ChooseTrack.this.mConfirmCallBack != null)
					ChooseTrack.this.mConfirmCallBack.onCancel();
				clearViewObjects();
			}
		}));

		mSoundActivity.addHandlerRegistration(mSoundDialogPanel.getOkButton().addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
				MRadioButton selected = null;
				int buttonIndex = 0;
				int selectedIndex = -1;
				for (MRadioButton button : mButtonList)
				{
					if (button.getValue()) {
						selected = button;
						selectedIndex = buttonIndex;
						break;
					}
					buttonIndex++;
				}
				if (selectedIndex == -1)
					return;
				final SoundInfo soundInfo = StaticInfo.MALABI_INFO.mSounds.get(selectedIndex);  			
				mPopinDialog.hide();
				if (ChooseTrack.this.mConfirmCallBack != null)
					ChooseTrack.this.mConfirmCallBack.onChoose(selected.getText(), soundInfo);
			}
		}));

		//Label  titleLabel = new Label(MESSAGES.soundFile());
		// titleLabel.addStyleName("malabi-sound-file-title");
		// dialogPanel1.getContent().add(titleLabel);

		mScrollPanel = new ScrollPanel();

		mMainPanel = new Panel();
		
		for (SoundInfo audio : StaticInfo.MALABI_INFO.mSounds)
			addRadio(audio.mDisplayName+" "+audio.mDurance);
		
		setTitleText(MESSAGES.soundFile());
		mScrollPanel.add(mMainPanel);
		//    mScrollPanel.setHeight("300px");
		mSoundDialogPanel.getContent().add(mScrollPanel);
		mScrollPanel.setHeight("100%");
		mScrollPanel.refresh();
	}
	
	public void clearViewObjects() {
		mButtonList.clear();
		mMainPanel.clear();
		mScrollPanel.clear();
		mSoundDialogPanel.getContent().clear();
		mPopinDialog.clear();
	}

	private void addRadio(String text)
	{

		MRadioButton radioButton = new MRadioButton("sound");
		radioButton.addStyleName(SOUND_FILE_RADIO);
		//  radioButton.labelElement.getStyle()
		radioButton.setText(text);
		mMainPanel.add(radioButton);
		mButtonList.add(radioButton);
	}

	private static String SOUND_FILE_RADIO = "malabi-sound-file-radio";
	private static String SOUND_FILE_TITLE = "malabi-sound-file-title";
	private static String SOUND_FILE_CONTAINER = "malabi-sound-file-container";
	private static String SOUND_FILE_CONTENT = "malabi-sound-file-content";

	@Override
	public void setTitleText(String title) {
		mSoundDialogPanel.getDialogTitle().addStyleName(SOUND_FILE_TITLE);
		mSoundDialogPanel.getDialogTitle().setText(title);
		mSoundDialogPanel.getContainer().addStyleName(SOUND_FILE_CONTAINER);
		mSoundDialogPanel.getContent().addStyleName(SOUND_FILE_CONTENT);
	}

	@Override
	public String getTitleText() {
		return mSoundDialogPanel.getDialogTitle().getText();
	}


	public void show() {
		//	for (String audio : mSoundList)
		//		addRadio(audio);
		mPopinDialog.center();
	}

	@Override
	public void hide() {
		mPopinDialog.hide();
	}
}
