package com.eltel.malabi.client.sound;

import com.google.gwt.user.client.ui.IsWidget;
public interface SoundView extends IsWidget {

	public void updateSoundViewScreen();
	
	void setControler(SoundActivity soundActivity);
	
	void clearViewObjects();
}
