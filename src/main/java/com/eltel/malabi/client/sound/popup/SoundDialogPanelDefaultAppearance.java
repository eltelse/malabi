package com.eltel.malabi.client.sound.popup;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Widget;

public class SoundDialogPanelDefaultAppearance  implements SoundDialogPanelAppearance {

	  @UiTemplate("SoundDialogButtonAbstractAppearance.ui.xml")
	  interface Binder extends UiBinder<Element, SoundDialogButton> {
	  }

	  private static final Binder UI_SOUND_BINDER = GWT.create(Binder.class);

	  @UiTemplate("SoundDialogPanelAbstractAppearance.ui.xml")
	  interface DialogBinder extends UiBinder<Widget, SoundDialogPanel> {
	  }

	  private static final DialogBinder UI_SOUND_DIALOG_BINDER = GWT.create(DialogBinder.class);

	  @Override
	  public UiBinder<Element, SoundDialogButton> uiBinder() {
	    return UI_SOUND_BINDER;
	  }

	  @Override
	  public UiBinder<Widget, SoundDialogPanel> soundDialogBinder() {
	    return UI_SOUND_DIALOG_BINDER;
	  }
	  

  static {
    Resources.INSTANCE.css().ensureInjected();
    Resources.INSTANCE.cssPanel().ensureInjected();
  }

  interface Resources extends ClientBundle {

    Resources INSTANCE = GWT.create(Resources.class);

    @Source({"sounddialog-button.css"})
    SoundDialogButtonCss css();

    @Source({"sounddialog.css"})
    SoundDialogCss cssPanel();
    
  }

  @Override
  public SoundDialogCss soundDialogCss() {
    return Resources.INSTANCE.cssPanel();
  }

  @Override
  public SoundDialogButtonCss css() {
	  return Resources.INSTANCE.css();

  }
  @Override
  public SoundDialogCss overlayCss() {
	  return Resources.INSTANCE.cssPanel();
  }
}