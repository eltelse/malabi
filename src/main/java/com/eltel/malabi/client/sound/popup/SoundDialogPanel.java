package com.eltel.malabi.client.sound.popup;

import com.eltel.malabi.client.StaticInfo;
import com.eltel.malabi.client.messages.MalaMessages;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiFactory;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;

public class SoundDialogPanel extends Composite {

  public static final SoundDialogPanelDefaultAppearance DEFAULT_APPEARANCE = GWT
		  .create(SoundDialogPanelDefaultAppearance.class);
  
  private  MalaMessages  MESSAGES = StaticInfo.GET_MalaMessages();

  @UiField
  public Label title;
  @UiField
  public FlowPanel container;
  @UiField
  public FlowPanel content;
  @UiField
  public FlowPanel buttonContainer;

  @UiField(provided = true)
  public SoundDialogButton okButton;
  @UiField(provided = true)
  public SoundDialogButton cancelButton;

  private SoundDialogPanelAppearance appearance;

  /**
   * Construct the panel
   */
  public SoundDialogPanel() {
    this(DEFAULT_APPEARANCE);
  }

  /**
   * Construct panel with a special css
   *
   * @param css the css to use
   */
  public SoundDialogPanel(SoundDialogPanelAppearance appearance) {
    this.appearance = appearance;
    okButton = new SoundDialogButton(appearance, MESSAGES.choose(), "ok");
    
    cancelButton = new SoundDialogButton(appearance, MESSAGES.cancel(), "cancel");
    
 //  getDialogTitle().setText(MESSAGES.soundFile());
    initWidget(this.appearance.soundDialogBinder().createAndBindUi(this));
    cancelButton.setCancel(true);
    okButton.setOK(true);
    buttonContainer.addStyleName("malabi-sound-file-dialog-buttons");
  }

  /**
   * get the container of the panel
   *
   * @return the container of the dialog panel
   */
  public FlowPanel getContent() {
    return content;
  }
  
  public FlowPanel getContainer() {
	    return container;
  }

  /**
   * get {@link HasTapHandlers} for the cancel button
   *
   * @return the {@link HasTapHandlers} for cancel button
   */
  public HasTapHandlers getCancelButton() {
    return cancelButton;
  }

  /**
   * get {@link HasTapHandlers} for the ok button
   *
   * @return the {@link HasTapHandlers} for ok button
   */
  public HasTapHandlers getOkButton() {
    return okButton;
  }

  /**
   * show the cancel button
   *
   * @param show true to show, otherwise hidden
   */
  public void showCancelButton(boolean show) {
    if (show) {
      int widgetCount = buttonContainer.getWidgetCount();
      if (widgetCount == 0) {
        buttonContainer.add(cancelButton);
      }
    } else {
      buttonContainer.remove(cancelButton);
    }
  }

  /**
   * show the ok button
   *
   * @param show true to show, otherwise hidden
   */
  public void showOkButton(boolean show) {
    if (show) {
      buttonContainer.insert(okButton, 0);
    } else {
      buttonContainer.remove(okButton);
    }
  }

  /**
   * Get the title of the dialog
   *
   * @return the title of the dialog
   */
  public Label getDialogTitle() {
    return title;
  }

  @UiFactory
  public SoundDialogPanelAppearance getAppearance() {
    return appearance;
  }
}
