package com.eltel.malabi.client.sound.popup;

import com.google.gwt.uibinder.client.UiFactory;
import com.googlecode.mgwt.ui.client.widget.button.ButtonBase;

public class SoundDialogButton extends ButtonBase {
  private SoundDialogPanelAppearance appearance;

  public SoundDialogButton(SoundDialogPanelAppearance appearance, String text, String styleName) {
    super(appearance);
    this.appearance = appearance;
    setElement(appearance.uiBinder().createAndBindUi(this));
    setText(text);
    addStyleName("malabi-sound-file-button");
    addStyleName("malabi-sound-file-button-"+styleName);
  }

  public void setCancel(boolean cancel) {
    removeStyleNames();
    if (cancel) {
      addStyleName(appearance.css().cancelbutton());
    }
  }

  public void setOK(boolean ok) {
    removeStyleNames();
    if (ok) {
      addStyleName(appearance.css().okbutton());
    }
  }

  @UiFactory
  public SoundDialogPanelAppearance getAppearance() {
	  return appearance;
  }

  private void removeStyleNames() {
    removeStyleName(appearance.css().cancelbutton());
    removeStyleName(appearance.css().okbutton());
  }
}
