package com.eltel.malabi.client.help;

import com.googlecode.mgwt.ui.client.util.MGWTCssResource;

public interface HelpDialogPanelAppearance {
   

  interface HelpDialogCss extends MGWTCssResource {
    @ClassName("mgwt-HelpDialogPanel")
    String HelpDialogPanel();

    @ClassName("mgwt-HelpDialogPanel-container")
    String container();

    @ClassName("mgwt-HelpDialogOverlay")
	String HelpDialogOverlay();

    @ClassName("mgwt-HelpDialogOverlay-shadow")
    String animationContainerShadow();
  }

  HelpDialogCss HelpDialogCss();
  
  HelpDialogCss overlayCss();

//  UiBinder<Widget, HelpDialogPanel> HelpDialogBinder();
}
