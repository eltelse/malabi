package com.eltel.malabi.client.help;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.resources.client.ClientBundle;

public class HelpDialogPanelDefaultAppearance  implements HelpDialogPanelAppearance
{

	  static {
	    Resources.INSTANCE.cssPanel().ensureInjected();
	  }

	  interface Resources extends ClientBundle {

	  Resources INSTANCE = GWT.create(Resources.class);

	  @Source({"helpdialog.css"})
		HelpDialogCss cssPanel();

	    @Source({"helpdialog.css"})
	    HelpDialogCss css();
	  }

	  @Override
	  public HelpDialogCss overlayCss() {
	    return Resources.INSTANCE.css();
	  }
//	  {

//	@UiTemplate("HelpDialogPanelAbstractAppearance.ui.xml")
//	interface DialogBinder extends UiBinder<Widget, HelpDialogPanel> {
//	}

//	private static final DialogBinder PUP_UP_BINDER = GWT.create(DialogBinder.class);

//	@Override
//	public UiBinder<Widget, HelpDialogPanel> HelpDialogBinder() {
//		return PUP_UP_BINDER;
//	}


	@Override
	public HelpDialogCss HelpDialogCss() {
		return Resources.INSTANCE.cssPanel();
	}
}
