package com.eltel.malabi.client;

import com.eltel.malabi.client.activities.AboutView;
import com.eltel.malabi.client.light.LightView;
import com.eltel.malabi.client.login.LoginAboutView;
import com.eltel.malabi.client.login.LoginView;
import com.eltel.malabi.client.mainScreen.MainScreenView;
import com.eltel.malabi.client.map.MapView;
import com.eltel.malabi.client.sound.SoundView;
import com.eltel.malabi.client.topPanel.EmptyTopPanelView;
import com.eltel.malabi.client.topPanel.TopPanelView;
import com.google.gwt.place.shared.PlaceController;
import com.google.web.bindery.event.shared.EventBus;

public interface ClientFactory {
//	public LoginView getLoginView();
	
	public MainScreenView getMainScreenView();
	
	public EmptyTopPanelView getEmptyTopPanelView();
	
	public TopPanelView getTopPanelView();
	
	public SoundView getSoundView();
	
	public LightView getLightView();
	
	public MapView getMapView();
	
	public LoginView getHomeLoginView();
	
	public LoginAboutView getLoginAboutView();

	public EventBus getEventBus();

	public PlaceController getPlaceController();

	/**
	 * @return
	 */
	public AboutView getAboutView();
}
