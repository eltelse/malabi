package com.eltel.malabi.client.activities;

import com.eltel.malabi.client.ClientFactory;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.googlecode.mgwt.mvp.client.MGWTAbstractActivity;


public class AboutActivity extends MGWTAbstractActivity {

	protected ClientFactory clientFactory;
	protected boolean isLogin = false;

	public AboutActivity(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	public AboutActivity(ClientFactory clientFactory, String eventId) {
		this(clientFactory);
	}	

	@Override
	public void start(AcceptsOneWidget panel, final EventBus eventBus) {
		AboutView view = clientFactory.getAboutView();
		if (!isLogin)
		{
			AboutView aboutView = clientFactory.getAboutView();
			panel.setWidget(aboutView);
		}
		
		view.setPlaceController(clientFactory.getPlaceController());

	}

}
