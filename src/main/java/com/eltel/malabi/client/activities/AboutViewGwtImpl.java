package com.eltel.malabi.client.activities;

import com.eltel.malabi.client.help.HelpDialogOverlay;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.ui.client.widget.button.Button;
import com.googlecode.mgwt.ui.client.widget.header.HeaderPanel;
import com.googlecode.mgwt.ui.client.widget.header.HeaderTitle;
import com.googlecode.mgwt.ui.client.widget.panel.flex.FlexSpacer;

public class AboutViewGwtImpl  implements AboutView  {

	protected HeaderTitle buttomTitle;
	protected HeaderPanel headerPanel;
	private static String BUTTOM_PANEL = "malabi-buttom-panel";
	private static String BUTTOM_PANEL_LABEL = "malabi-buttom-panel-label";
	private static String BUTTOM_PANEL_HELP_BUTTON = "malabi-buttom-panel-help-button";
	private static String BUTTOM_PANEL_ELTEL_LOGO = "malabi-buttom-panel-eltel-logo";
	
	private static String ELTEL_LOGO_A = "<div class=\"logo\"><a href=\"http://eltel.co.il/\"target=\"_blank\">";
	private static String ELTEL_LOGO_B = "<img src=\"https://eltel.co.il/wp-content/uploads/2019/09/Eltel-new-Logo-1.png\" alt=\"\"></a></div>";
	
	private HelpDialogOverlay mHelpPopUp;
	private PlaceController mPlaceController;

	public AboutViewGwtImpl() {
				initUI();
	}
	
	
	private void initUI(){
	
		headerPanel = new HeaderPanel();
		headerPanel.add(new FlexSpacer());
		headerPanel.addStyleName(BUTTOM_PANEL);
		headerPanel.add(new HeaderTitle());
		buttomTitle = new HeaderTitle("MALA wireless control");
		buttomTitle.addStyleName(BUTTOM_PANEL_LABEL);
		headerPanel.add(buttomTitle);
		Button button = new Button("?");
		button.addStyleName(BUTTOM_PANEL_HELP_BUTTON);
		button.addTapHandler(new TapHandler() {
			
			@Override
			public void onTap(TapEvent event) {
				if (mHelpPopUp == null)
					mHelpPopUp = new HelpDialogOverlay( mPlaceController);
				mHelpPopUp.show();
			}
		});
		headerPanel.add(button);
		
		StringBuilder eltelLogo = new StringBuilder();
		eltelLogo.append(ELTEL_LOGO_A);
		eltelLogo.append(ELTEL_LOGO_B);
		HTML html = new HTML(eltelLogo.toString());
		html.addStyleName(BUTTOM_PANEL_ELTEL_LOGO);
		headerPanel.add(html);
	}
	
	@Override
	public void setPlaceController(PlaceController placeController) {
		this.mPlaceController = placeController;		
	}
	
	@Override
	public Widget asWidget() {
		return headerPanel;
	}
}

