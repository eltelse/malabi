package com.eltel.malabi.client.activities;

import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.IsWidget;

/**

 * 
 */
public interface AboutView extends IsWidget {
	
	public void setPlaceController(PlaceController placeController);
	
	//public HasTapHandlers getHelpButton();
//
//	public HasText getHeader();

}
