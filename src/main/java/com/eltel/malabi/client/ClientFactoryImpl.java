package com.eltel.malabi.client;

import com.eltel.malabi.client.activities.AboutView;
import com.eltel.malabi.client.activities.AboutViewGwtImpl;
import com.eltel.malabi.client.light.LightView;
import com.eltel.malabi.client.light.LightViewGwtImpl;
import com.eltel.malabi.client.login.LoginAboutView;
import com.eltel.malabi.client.login.LoginAboutViewGwtImpl;
import com.eltel.malabi.client.login.LoginView;
import com.eltel.malabi.client.login.LoginViewGwtImpl;
import com.eltel.malabi.client.mainScreen.MainScreenView;
import com.eltel.malabi.client.mainScreen.MainScreenViewGwtImpl;
import com.eltel.malabi.client.map.MapView;
import com.eltel.malabi.client.map.MapViewGwtImpl;
import com.eltel.malabi.client.sound.SoundView;
import com.eltel.malabi.client.sound.SoundViewGwtImpl;
import com.eltel.malabi.client.topPanel.EmptyTopPanelView;
import com.eltel.malabi.client.topPanel.EmptyTopPanelViewGwtImp;
import com.eltel.malabi.client.topPanel.TopPanelView;
import com.eltel.malabi.client.topPanel.TopPanelViewGwtImpl;
import com.google.gwt.place.shared.PlaceController;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.SimpleEventBus;

/**

 * 
 */
public class ClientFactoryImpl implements ClientFactory {

	private EventBus eventBus;
	private PlaceController placeController;
	private LoginView homeViewImpl;
	private LoginAboutView loginAboutView;
	
	private MainScreenView mainScreenImpl;
	private EmptyTopPanelView emptyTopPanelImpl;
	private TopPanelView topPanelImpl;
	private SoundView soundViewImpl;
	
	private LightView lightViewImpl;
	private MapView mapViewImpl;
	private AboutView aboutView;

	public ClientFactoryImpl() {
		eventBus = new SimpleEventBus();

		placeController = new PlaceController(eventBus);

		homeViewImpl = new LoginViewGwtImpl();
	}

	@Override
	public MainScreenView getMainScreenView() {
		if (mainScreenImpl == null) {
			mainScreenImpl = new MainScreenViewGwtImpl();
		}
		return mainScreenImpl;
	}
	
	@Override
	public TopPanelView getTopPanelView() {
		if (topPanelImpl == null) {
			topPanelImpl = new TopPanelViewGwtImpl();
		}
		return topPanelImpl;
	}
	
	@Override
	public EmptyTopPanelView getEmptyTopPanelView() {
		if (emptyTopPanelImpl == null) {
			emptyTopPanelImpl = new EmptyTopPanelViewGwtImp();
		}
		return emptyTopPanelImpl;
	}
	

	@Override
	public LoginView getHomeLoginView() {
		if (homeViewImpl == null) {
			homeViewImpl = new LoginViewGwtImpl();
		}
		return homeViewImpl;
	}
	
	@Override
	public LoginAboutView getLoginAboutView() {
		if (loginAboutView == null) {
			loginAboutView = new LoginAboutViewGwtImpl();
		}
		return loginAboutView;
	}
	
	@Override
	public SoundView getSoundView() {
		if (soundViewImpl == null) {
			soundViewImpl = new SoundViewGwtImpl();
		}
		return soundViewImpl;
	}
	
	@Override
	public LightView getLightView() {
		if (lightViewImpl == null) {
			lightViewImpl = new LightViewGwtImpl();
		}
		return lightViewImpl;
	}
	
	@Override
	public MapView getMapView() {
		if (mapViewImpl == null) {
			mapViewImpl = new MapViewGwtImpl();
		}
		return mapViewImpl;
	}

	@Override
	public EventBus getEventBus() {
		return eventBus;
	}

	@Override
	public PlaceController getPlaceController() {
		return placeController;
	}

	@Override
	public AboutView getAboutView() {
		if (aboutView == null) {
			aboutView = new AboutViewGwtImpl();
		}

		return aboutView;
	}

}
